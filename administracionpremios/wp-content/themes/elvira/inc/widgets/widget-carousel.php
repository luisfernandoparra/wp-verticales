<?php
add_action( 'widgets_init', create_function('', 'return register_widget("themedsgn_widget_carousel");') );

class themedsgn_widget_carousel extends WP_Widget {

	public function __construct() {
		$widget_details = array(
			'classname'   => 'themedsgn_widget_carousel',
			'description' => esc_html__( 'Display posts carousel.', 'elvira' ),
		);

		parent::__construct( 'themedsgn_widget_carousel', 'ThemeDsgn Carousel Widget', $widget_details );		
	}

	public function form( $instance ) {
		$title      = isset( $instance['title'] ) ? $instance['title'] : '';
		$number     = isset( $instance['number'] ) ? (int) $instance['number'] : 5;
		$order      = isset( $instance['order'] ) ? $instance['order'] : 'date';
		$cat        = isset( $instance['cat'] ) ? $instance['cat'] : '';
		$tag        = isset( $instance['tag'] ) ? $instance['tag'] : array();

		// Selected tags by user
		$selected_tags = array();   
		if( $tag ) {
			foreach( $tag as $k => $v ) {
				$selected_tags[$v] = $v;
			}
		}

		// If no tag selected
		$no_tags = false;
		if( empty( $tag[0] ) || empty( $tag ) ) {
			$no_tags = true;
		}

		// Get all availabel tags & categories
		$all_tags   = get_tags();
		$categories = get_categories( array(
			'hide_empty'   => true,
			'hierarchical' => false,
		) );
		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:', 'elvira' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e( 'Number of posts to show:', 'elvira' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" value="<?php echo esc_attr( $number ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php esc_html_e( 'Posts ordered by:', 'elvira' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>">
				<option <?php selected( $order, 'date' ); ?> value="date">Date</option>
				<option <?php selected( $order, 'comment_count' ); ?> value="comment_count">Comment Count</option>
				<option <?php selected( $order, 'rand' ); ?> value="rand">Random</option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'cat' ); ?>"><?php esc_html_e( 'Category:', 'elvira' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'cat' ); ?>" name="<?php echo $this->get_field_name( 'cat' ); ?>">
				<option <?php selected( $cat, '' ); ?> value="">All Categories</option>
				<?php 
				if( $categories ):
					foreach( $categories as $category ): ?>
					<option <?php selected( $cat, $category->term_id ); ?> value="<?php echo esc_attr( $category->term_id ); ?>"><?php echo esc_html( $category->name ); ?></option>
					<?php
					endforeach;
				endif;
				?>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'tag' ); ?>"><?php esc_html_e( 'Tag:', 'elvira' ); ?><br>
				<small class="howto">Use CMD or CTRL to select multiple tags.</small>
			</label>
			<select style="height: 200px;" multiple class="widefat" id="<?php echo $this->get_field_id( 'tag' ); ?>" name="<?php echo $this->get_field_name( 'tag' ); ?>[]">
				<option <?php echo $no_tags ? 'selected' : ''; ?> value="">Disable tags filter</option>
				<optgroup label="Available Tags">
					<?php 
					if( $all_tags ):
						foreach( $all_tags as $t ): 
							$selected = '';
							if( isset( $selected_tags[$t->term_id] ) ) {
								if( $selected_tags[$t->term_id] == $t->term_id ) {
									$selected = 'selected';
								}
							}
						?>
							<option <?php echo esc_attr( $selected ); ?> value="<?php echo esc_attr( $t->term_id ); ?>"><?php echo esc_html( $t->name ); ?></option>
						<?php
						endforeach;
					endif;
					?>
				</optgroup>
			</select>
		</p>

		<?php

	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']     = sanitize_text_field( $new_instance['title'] );
		$instance['number']    = (int) $new_instance['number'];
		$instance['order']     = $new_instance['order'];
		$instance['cat']       = $new_instance['cat'];
		$instance['tag']       = $new_instance['tag'];
		return $instance;
	}

	public function widget( $args, $instance ) {
		$title     = !empty( $instance['title'] ) ? $instance['title'] : '';
		$number    = !empty( $instance['number'] ) ? (int) $instance['number'] : 5;
		$order     = !empty( $instance['order'] ) ? $instance['order'] : 'date';
		$cat       = !empty( $instance['cat'] ) ? $instance['cat'] : '';
		$tag       = !empty( $instance['tag'] ) ? $instance['tag'] : '';

		echo $args['before_widget'];
		if( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$use_tags = true;
		if( empty( $tag[0] ) || empty( $tag ) ) { 
			$use_tags = false;
		}

		$query_args = array(
			'posts_per_page'      => $number,
			'orderby'             => $order,
			'ignore_sticky_posts' => true,
			'cat'                 => $cat,
			'tag__in'             => $use_tags ? $tag : array(),
			'meta_key'            => '_thumbnail_id',
		);
		$query = new WP_Query( $query_args );

		if( $query->have_posts() ): ?>		

			<div class="posts-carousel owl-carousel">

				<?php 
				while( $query->have_posts() ): $query->the_post(); ?>

					<div class="post-item">
						<div class="post-item-wrap">
							<a href="<?php the_permalink(); ?>" class="post-image"><?php the_post_thumbnail( 'related-thumbnail' ); ?></a>	
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php echo esc_html( get_the_date( 'M d, Y' ) ); ?></time>
						</div>
					</div>

				<?php endwhile; ?>

			</div>

		<?php
		endif;		

		echo $args['after_widget'];
		wp_reset_postdata();
	}

}