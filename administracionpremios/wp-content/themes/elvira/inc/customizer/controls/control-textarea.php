<?php
if ( ! class_exists( 'WP_Customize_Control' ) ) {
	return NULL;
}

class Themedsgn_Customize_Textarea_Control extends WP_Customize_Control {

	public $type = 'textarea';

	public function render_content() {
		?>
		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
			<?php endif; ?>
			<textarea class="large-text" cols="20" rows="10" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
		</label>
		<?php
	}

}