<?php
/**
 * Themedsgn_Customizer class.
 */
if( !class_exists( 'Themedsgn_Customizer' ) ) {

	require plugin_dir_path( __FILE__ ) . 'inc/fonts.php';
	require plugin_dir_path( __FILE__ ) . 'controls/control-textarea.php';
	require plugin_dir_path( __FILE__ ) . 'controls/control-radio-image.php';
	require plugin_dir_path( __FILE__ ) . 'controls/control-arbitrary-html.php';
	require plugin_dir_path( __FILE__ ) . 'controls/control-code-editor.php';
	require plugin_dir_path( __FILE__ ) . 'controls/control-typography.php';

	class Themedsgn_Customizer {

		private static $instance;
		public $options = array();
		public $panels  = array();

		public static function instance() {
			if( is_null( self::$instance ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		public function set_options( $options = array() ) {
			$this->options = array_merge( $options, $this->options );
		}

		public function get_options() {
			return $this->options;
		}

		public function set_panels( $panels = array() ) {
			$this->panels = array_merge( $panels, $this->panels );
		}

		public function get_panels() {
			return $this->panels;
		}
	}
}

/**
 * Customizer style
 */
function themedsgn_enqueue_customizer_style() {
	?>
<style type="text/css">
.customize-control select { min-width: auto; }
.themedsgn-radio-image-control input.radio-image { display: none; }
.themedsgn-radio-image-control label { margin: 2px; display: inline-block; }
.themedsgn-radio-image-control img { border: 3px solid #ddd; }
.themedsgn-radio-image-control input.radio-image:checked + label img { border-color: #0a0; }
.themedsgn-arbitrary-html-control hr { clear: both; margin-bottom: 10px; margin-top: 10px; display: block; }
.themedsgn-code-editor-control { border: 1px solid #000; background: #333; padding: 10px; font-size: 12px; white-space: pre; overflow-x: scroll; font-family: monospace; color: #fff; word-wrap: normal; }
</style>
	<?php
}
add_action( 'customize_controls_print_styles', 'themedsgn_enqueue_customizer_style' );

/**
 * Themedsgn customize register.
 */
function themedsgn_customize_register( $wp_customize ) {
	$themedsgn_customizer = Themedsgn_Customizer::Instance();
	$options = $themedsgn_customizer->get_options();
	$panels  = $themedsgn_customizer->get_panels();

	// panels
	if( !empty( $panels ) ) {
		foreach( $panels as $key => $value ) {
			$wp_customize->add_panel( $key, $value );
		}
	}

	foreach( $options as $key => $value ) {

		$section_args = $value;
		unset( $section_args['fields'] );

		// add section
		themedsgn_customizer_add_section( $key, $section_args, $wp_customize );

		// add setting & control
		foreach( $value['fields'] as $field ) {

			$field['section']           = $key;
			$field['sanitize_callback'] = themedsgn_customizer_sanitize_callback( $field['type'] );

			themedsgn_customizer_add_setting( $field['id'], $field, $wp_customize );
			themedsgn_customizer_add_control( $field['id'], $field, $wp_customize );
		}
	}

}
add_action( 'customize_register', 'themedsgn_customize_register' );

/**
 * Add section.
 */
function themedsgn_customizer_add_section( $id, $args = array(), $wp_customize ) {
	$default_args = array(
		'title'           => '',
		'priority'        => NULL,
		'description'     => '',
		'active_callback' => NULL,
	);
	$args = array_merge( $default_args, $args );	
	$wp_customize->add_section( $id, $args );
}

/**
 * Add setting.
 */
function themedsgn_customizer_add_setting( $id, $args = array(), $wp_customize ) {
	$default_args = array(
		'default'              => NULL,
		'option_type'          => 'theme_mod',
		'capability'           => 'edit_theme_options',
		'theme_supports'       => NULL,
		'transport'            => NULL,
		'sanitize_callback'    => 'wp_kses_post',
		'sanitize_js_callback' => NULL
	);
	$args = array_merge( $default_args, $args );	
	$wp_customize->add_setting( $id, array(
		'default'              => $args['default'],
		'option_type'          => $args['option_type'],
		'capability'           => $args['capability'],
		'theme_supports'       => $args['theme_supports'],
		'transport'            => $args['transport'],
		'sanitize_callback'    => $args['sanitize_callback'],
		'sanitize_js_callback' => $args['sanitize_js_callback'],
	) );
}

/**
 * Add control.
 */
function themedsgn_customizer_add_control( $id, $args = array(), $wp_customize ) {
	$default_args = array(
		'label'       => '',
		'description' => '',
		'section'     => '',
		'priority'    => '',
		'type'        => '',
		'choices'     => array(),
	);
	$args = array_merge( $default_args, $args );
	switch( $args['type'] ) {
		case 'text':
		case 'url':
		case 'select':
		case 'radio':
		case 'checkbox':
		case 'range':
		case 'number':
		case 'dropdown-pages': 
			$wp_customize->add_control( $id, $args );
			break;
		case 'color':
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $id, $args ) );
			break;
		case 'image':
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, $id, $args ) );
			break;
		case 'textarea':
			$wp_customize->add_control( new Themedsgn_Customize_Textarea_Control( $wp_customize, $id, $args ) );
			break;
		case 'radio-image':
			$wp_customize->add_control( new Themedsgn_Customize_Radio_Image_Control( $wp_customize, $id, $args ) );
			break;
		case 'html':
			$wp_customize->add_control( new Themedsgn_Customize_Arbitrary_Control( $wp_customize, $id, $args ) );
			break;
		case 'code':
			$wp_customize->add_control( new Themedsgn_Customize_Code_Editor_Control( $wp_customize, $id, $args ) );
			break;
		case 'typography':
			$wp_customize->add_control( new Themedsgn_Customize_Typography_Control( $wp_customize, $id, $args ) );
			break;
	}
}

/**
 * Sanitize callback helper.
 */
function themedsgn_customizer_sanitize_callback( $type = '' ) {
	switch( $type ) {
		case 'text': 
			return 'sanitize_text_field'; 
			break;
		case 'url':
		case 'image': 
			return 'esc_url_raw'; 
			break;
		case 'textarea': 
			return 'themedsgn_sanitize_textarea'; 
			break;
		case 'checkbox':
			return 'themedsgn_sanitize_checkbox';
			break;
		case 'radio':
		case 'select':
		case 'dropdown-pages':
		case 'radio-image':
		case 'typography':
			return 'sanitize_key';
			break;
		case 'range':
		case 'number':
			return 'themedsgn_sanitize_number';
		default:
			return false;
	}
}

/**
 * Sanitize number
 */
function themedsgn_sanitize_number( $value ){
	return is_numeric( $value ) ? $value : 0;
}

/**
 * Sanitize checkbox.
 */
function themedsgn_sanitize_checkbox( $value ) {
	return ( $value == 1 ) ? true : false;
}

/**
 * Sanitize textarea
 */
function themedsgn_sanitize_textarea( $value ) {
	$allowed_tags = array(
		'a'      => array(
					'href'  => array(),
					'title' => array(),
					'class' => array(), ),
		'i'      => array(
					'class' => array(), ),
		'span'   => array(),
		'p'      => array(),
		'strong' => array(),
		'br'     => array(),
		'hr'     => array(),
		'em'     => array(),
	);
	return wp_kses( $value, $allowed_tags );
}
?>