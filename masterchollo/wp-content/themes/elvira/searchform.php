<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-form">
	<input type="search" class="search-input" placeholder="<?php echo esc_attr( themedsgn_get_theme_mod( 'search_placeholder', 'Search&hellip;' ) ); ?>" name="s" value="<?php the_search_query(); ?>">
	<button class="search-submit"><i class="fa fa-search"></i></button>
</form>