(function($) {
	'use strict';
	$(document).ready( function() {

		/* 
		 * Equal Height Elements
		 */
		$('.related-posts').imagesLoaded( function() {
			$('.related-item a').matchHeight();
		});		

		/* 
		 * Posts Carousel 
		 */
		$('.posts-carousel').imagesLoaded(function(){
			$('.posts-carousel').owlCarousel({
				rewind: true,
				margin: 30,
				nav: true,
				navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
				dots: true,
				navSpeed: 300,
				dotsSpeed: 300,
				responsive: {
					300: { items: 1 },
					600: { items: 2 },
					800: { items: 3 },
					1000: { items: 4 }
				},
				onInitialized: function(){
					$('.posts-carousel .post-item .post-item-wrap').matchHeight();
				},
				onRefreshed: function(){
					$('.posts-carousel .post-item .post-item-wrap').matchHeight();
				}
			});
		});		

		/* 
		 * Masonry layout 
		 */
		$('.blogroll-grid').imagesLoaded( function() {
			var $masonry = $('.blogroll-grid');
			$masonry.masonry({
				itemSelector: '.entry',
				columnWidth: '.grid-sizer',
			});	
		});		

		/*
		 * Widgets
		 */
		$('#sidebar').imagesLoaded( function() {
			var $sidebarMasonry = $('#sidebar');
			$sidebarMasonry.masonry({
				itemSelector: '.widget',
			});
		});		

		/*
		 * Posts Slider
		 */
		$('.posts-slider').owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			autoplayHoverPause: true,
			nav: true,
			navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
			navSpeed: 500,
			dotsSpeed: 500,
			smartSpeed: 500,
		});		

		/*
		 * Menu
		 */
		$('.main-menu li').has('ul').find('a:first').append('<i class="fa fa-angle-double-right"></i>');
		$('.mobile-nav .mobile-nav-anchor').on('click', function(e) {
			e.preventDefault();
			$('.mobile-menu').toggle();
		});

		/* 
		 * FitVids 
		 */
		$('body').fitVids();

		/*
		 * Sticky Nav
		 */
		$('.sticky-nav #nav').sticky({
			wrapperClassName: 'sticky-nav-wrapper',
			className: 'sticky-nav-on',
		});		

		/*
		 * Share
		 */
		$('.entry-share a').on('click', function(e){
			e.preventDefault();
			var left = (screen.width/2)-(700/2);
			var top = (screen.height/2)-(500/2);
			return window.open( $(this).attr('href'), '_blank', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=700, height=500, top='+top+', left='+left );
		});

	});
})(jQuery);