<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
	<div id="site">

		<?php 
		/*
		 * Topbar.
		 */
		themedsgn_elvira_topbar(); ?>

		<?php 
		/*
		 * Header.
		 */
		themedsgn_elvira_header(); ?>

		<?php
		/*
		 * Top widgetized area.
		 */
		if( is_home() ) {
			if( is_active_sidebar( 'sidebar-homepage-top' ) ) {
				?>
				<div id="top-content" class="widgetized">
					<div class="container">
						<?php dynamic_sidebar( 'sidebar-homepage-top' ); ?>
					</div>
				</div>
				<?php
			}
		} else {
			if( is_active_sidebar( 'sidebar-top' ) ){
				?>
				<div id="top-content" class="widgetized">
					<div class="container">
						<?php dynamic_sidebar( 'sidebar-top' ); ?>
					</div>
				</div>
				<?php
			}
		}
		?>

		<div id="content" class="container">
