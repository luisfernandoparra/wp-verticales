<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-wrapper">

		<header class="entry-header">
			<?php
			themedsgn_elvira_category_meta(); 
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			themedsgn_elvira_entry_meta();
			?>
		</header>
		
		<?php if( has_post_thumbnail() ): ?>
		<div class="entry-image">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'large' ); ?>
			</a>
		</div>
		<?php endif; ?>
							
		<div class="entry-content clearfix">
			<?php the_content( ' ' ); ?>
		</div>
		
		<footer class="entry-footer clearfix">
			<?php themedsgn_elvira_readmore(); ?>
			<?php themedsgn_elvira_entry_share(); ?>
		</footer>

		<?php if( is_sticky() ): ?>
		<div class="sticky-indicator"></div>
		<?php endif; ?>

	</div>
</article>