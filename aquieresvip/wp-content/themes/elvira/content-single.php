<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-wrapper">

		<header class="entry-header">
			<?php
			themedsgn_elvira_category_meta(); 
			the_title( '<h1 class="entry-title">', '</h1>' );
			themedsgn_elvira_entry_meta();
			?>
		</header>
		
		<?php if( has_post_thumbnail() && themedsgn_get_theme_mod( 'featured_image', true ) ): ?>
		<div class="entry-image">
			<?php the_post_thumbnail( 'large' ); ?>
		</div>
		<?php endif; ?>
							
		<div class="entry-content clearfix">
			<?php 
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="link-pages">',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>'
			) );
			?>
		</div>
		
		<footer class="entry-footer clearfix">
			<?php themedsgn_elvira_entry_tags(); ?>
			<?php themedsgn_elvira_entry_share(); ?>
		</footer>

	</div>
</article>