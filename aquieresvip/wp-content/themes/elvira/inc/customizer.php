<?php
/**
 * Customizer.
 */
function themedsgn_elvira_customizer() {

	$prefix  = 'elvira_option_';
	$options = array();

	// Header choices
	$header_choices = array(
		'header-1'  => array(
			'title' => 'Social -- Logo -- Search',
			'image' => get_template_directory_uri() . '/assets/images/customizer/header-1.png'
		),
		'header-2'  => array(
			'title' => 'Search -- Logo -- Social',
			'image' => get_template_directory_uri() . '/assets/images/customizer/header-2.png'
		),
		'header-3'  => array(
			'title' => 'Logo -- Social',
			'image' => get_template_directory_uri() . '/assets/images/customizer/header-3.png'
		),
		'header-4'  => array(
			'title' => 'Logo -- Search',
			'image' => get_template_directory_uri() . '/assets/images/customizer/header-4.png'
		),
		'header-5'  => array(
			'title' => 'Logo (Center)',
			'image' => get_template_directory_uri() . '/assets/images/customizer/header-5.png'
		),
		'header-6'  => array(
			'title' => 'Logo (Left)',
			'image' => get_template_directory_uri() . '/assets/images/customizer/header-6.png'
		),
	);

	// Sidebar choices
	$sidebar_choices = array(
		'left-sidebar'  => array(
			'title' => 'Left Sidebar',
			'image' => get_template_directory_uri() . '/assets/images/customizer/left-sidebar.png'
		),
		'right-sidebar'  => array(
			'title' => 'Right Sidebar',
			'image' => get_template_directory_uri() . '/assets/images/customizer/right-sidebar.png'
		),
	);

	// Blogroll choices
	$blogroll_choices = array(
		'classic' => array(
			'title' => 'Classic with Content',
			'image' => get_template_directory_uri() . '/assets/images/customizer/blogroll-classic-1.png'
		),
		'classic-alt' => array(
			'title' => 'Classic with Excerpt',
			'image' => get_template_directory_uri() . '/assets/images/customizer/blogroll-classic-2.png'
		),
		'grid'      => array(
			'title' => 'Grid',
			'image' => get_template_directory_uri() . '/assets/images/customizer/blogroll-grid.png'
		),
		'list'      => array(
			'title' => 'List',
			'image' => get_template_directory_uri() . '/assets/images/customizer/blogroll-list.png'
		),
	);

	// Font size choices
	$font_size_choices = array();
	for( $i = 1; $i<=100; $i++ ) {
		$font_size_choices[ $i ] = $i;
	}

	// Logo
	$options['logo'] = array(
		'title'    => esc_html__( 'Logo & Tagline', 'elvira' ),
		'priority' => 40,
		'fields'   => array(
			array(
				'id'          => $prefix . 'logo',
				'label'       => esc_html__( 'Logo Image', 'elvira' ),
				'description' => esc_html__( 'Upload your website logo. If you want to use default Site Title, simply remove default image.', 'elvira' ),
				'type'        => 'image',
				'default'     => get_template_directory_uri() . '/assets/images/logo.png',
			),
			array(
				'id'          => $prefix . 'html_0',
				'content'     => '<hr>',
				'type'        => 'html',
			),
			array(
				'id'          => $prefix . 'tagline',
				'label'       => esc_html__( 'Display tagline below logo?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => false,
			),
			array(
				'id'          => $prefix . 'html_1',
				'content'     => '<hr><br>Set your logo width and height. To use default fullsized image, set each field with 0 [zero].',
				'type'        => 'html',
			),
			array(
				'id'          => $prefix . 'logo_width',
				'label'       => esc_html__( 'Logo Width (px)', 'elvira' ),
				'type'        => 'number',
				'default'     => 0,
			),
			array(
				'id'          => $prefix . 'logo_height',
				'label'       => esc_html__( 'Logo Height (px)', 'elvira' ),
				'type'        => 'number',
				'default'     => 0,
			),
		),
	);

	// Header
	$options['header'] = array(
		'title'    => esc_html__( 'Header', 'elvira' ),
		'priority' => 60,
		'fields'   => array(
			array(
				'id'          => $prefix . 'header_layout',
				'label'       => esc_html__( 'Header Layout', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'header-1',
				'choices'     => $header_choices,
			),
			array(
				'id'          => $prefix . 'logo_border',
				'label'       => esc_html__( 'Display left and right separator borders on logo?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'header_padding_top',
				'label'       => esc_html__( 'Header Padding Top (px)', 'elvira' ),
				'type'        => 'number',
				'default'     => 30,
			),
			array(
				'id'          => $prefix . 'header_padding_bottom',
				'label'       => esc_html__( 'Header Padding Bottom (px)', 'elvira' ),
				'type'        => 'number',
				'default'     => 30,
			),
			array(
				'id'          => $prefix . 'html_2a',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'search_placeholder',
				'label'       => esc_html__( 'Search Placeholder Text', 'elvira' ),
				'type'        => 'text',
				'default'     => 'Search&hellip;',
			),
			array(
				'id'          => $prefix . 'html_2',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'nav_style',
				'label'       => esc_html__( 'Navigation Style', 'elvira' ),
				'type'        => 'select',
				'default'     => 'boxed',
				'choices'     => array(
					'boxed'     => esc_html__( 'Boxed', 'elvira' ),
					'stretched' => esc_html__( 'Stretched', 'elvira' ),
				),
			),
			array(
				'id'          => $prefix . 'nav_alignment',
				'label'       => esc_html__( 'Navigation Alignment', 'elvira' ),
				'type'        => 'select',
				'default'     => 'center',
				'choices'     => array(
					'left'   => esc_html__( 'Left', 'elvira' ),
					'center' => esc_html__( 'Center', 'elvira' ),
					'right'  => esc_html__( 'Right', 'elvira' ),
				),
			),
			array(
				'id'          => $prefix . 'sticky_navigation',
				'label'       => esc_html__( 'Enable floating/sticky navigation.', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => false,
			),
			array(
				'id'          => $prefix . 'html_3',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'enable_topbar',
				'label'       => esc_html__( 'Enable Topbar', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'topbar_left',
				'label'       => esc_html__( 'Topbar Left', 'elvira' ),
				'type'        => 'select',
				'default'     => 'menu',
				'choices'     => array(
					'menu'   => esc_html__( 'Menu (Top Menu)', 'elvira' ),
					'social' => esc_html__( 'Social Icons & Search', 'elvira' ),
					'html'   => esc_html__( 'Custom HTML', 'elvira' ),
				),
			),
			array(
				'id'          => $prefix . 'topbar_right',
				'label'       => esc_html__( 'Topbar Right', 'elvira' ),
				'type'        => 'select',
				'default'     => 'html',
				'choices'     => array(
					'menu'   => esc_html__( 'Menu (Top Menu)', 'elvira' ),
					'social' => esc_html__( 'Social Icons & Search', 'elvira' ),
					'html'   => esc_html__( 'Custom HTML', 'elvira' ),
				),
			),
			array(
				'id'          => $prefix . 'topbar_html',
				'label'       => esc_html__( 'Topbar Custom HTML', 'elvira' ),
				'description' => esc_html__( 'Write your html content here. Allowed html tags: i, a, br, hr, p, strong', 'elvira' ),
				'type'        => 'code',
				'default'     => "",
			),
		),
	);	


	// Blog General
	$options['blog_general'] = array(
		'title'    => esc_html__( 'General', 'elvira' ),
		'priority' => 10,
		'panel'    => 'blog',
		'fields'   => array( 
			array(
				'id'          => $prefix . 'general_sidebar',
				'label'       => esc_html__( 'General Sidebar Position', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'right-sidebar',
				'choices'     => $sidebar_choices,
			),			
			array(
				'id'          => $prefix . 'html_4',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'show_meta_category',
				'label'       => esc_html__( 'Display category', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
			/*
			array(
				'id'          => $prefix . 'show_meta_date',
				'label'       => esc_html__( 'Display meta date', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'show_meta_comment',
				'label'       => esc_html__( 'Display meta comment', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'show_meta_author',
				'label'       => esc_html__( 'Display meta author', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
			*/
			array(
				'id'          => $prefix . 'date_format',
				'label'       => esc_html__( 'Date Format', 'elvira' ),
				'type'        => 'select',				
				'default'     => 'date-1',
				'choices'     => array(
					'date_1'  => esc_html__( 'Jan 31, 2016', 'elvira' ),
					'date_2'  => esc_html__( 'January 31, 2016', 'elvira' ),
					'date_3'  => esc_html__( '01/31/2016', 'elvira' ),
					'date_4'  => esc_html__( '31 Jan 2016', 'elvira' ),
					'date_5'  => esc_html__( '31 January 2016', 'elvira' ),
					'date_6'  => esc_html__( '31/01/2016', 'elvira' ),
				),
			),
			array(
				'id'          => $prefix . 'author_by',
				'label'       => esc_html__( 'Author "By" Text', 'elvira' ),
				'type'        => 'text',				
				'default'     => 'By',
			),
			array(
				'id'          => $prefix . 'comment_no_comment',
				'label'       => esc_html__( '"No Comments" Text', 'elvira' ),
				'type'        => 'text',				
				'default'     => 'No Comments',
			),
			array(
				'id'          => $prefix . 'comment_one_comment',
				'label'       => esc_html__( '"One Comment" Text', 'elvira' ),
				'type'        => 'text',				
				'default'     => '1 Comment',
			),
			array(
				'id'          => $prefix . 'comment_many_comments',
				'label'       => esc_html__( '"Comments" Text', 'elvira' ),
				'type'        => 'text',				
				'default'     => 'Comments',
			),
			array(
				'id'          => $prefix . 'html_5',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'readmore',
				'label'       => esc_html__( 'Readmore Text', 'elvira' ),
				'type'        => 'text',				
				'default'     => 'Continue Reading',
			),
			array(
				'id'          => $prefix . 'html_6',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'share_facebook',
				'label'       => esc_html__( 'Display Facebook Share', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'share_twitter',
				'label'       => esc_html__( 'Display Twitter Share', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'share_googleplus',
				'label'       => esc_html__( 'Display Google+ Share', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'share_pinterest',
				'label'       => esc_html__( 'Display Pinterest Share', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'share_linkedin',
				'label'       => esc_html__( 'Display LinkedIn Share', 'elvira' ),
				'type'        => 'checkbox',				
				'default'     => true,
			),
		),
	);

	// Blog Homepage
	$options['blog_homepage'] = array(
		'title'    => esc_html__( 'Homepage', 'elvira' ),
		'priority' => 20,
		'panel'    => 'blog',
		'fields'   => array( 
			array(
				'id'          => $prefix . 'homepage_sidebar',
				'label'       => esc_html__( 'Homepage Sidebar Position', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'right-sidebar',
				'choices'     => $sidebar_choices,
			),
			array(
				'id'          => $prefix . 'homepage_blogroll',
				'label'       => esc_html__( 'Homepage Posts Layout', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'classic-alt',
				'choices'     => $blogroll_choices,
			),
		),
	);

	// Blog Archive
	$options['blog_archive'] = array(
		'title'    => esc_html__( 'Archive', 'elvira' ),
		'priority' => 30,
		'panel'    => 'blog',
		'fields'   => array( 
			array(
				'id'          => $prefix . 'archive_sidebar',
				'label'       => esc_html__( 'Archive Page Sidebar Position', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'right-sidebar',
				'choices'     => $sidebar_choices,
			),
			array(
				'id'          => $prefix . 'archive_blogroll',
				'label'       => esc_html__( 'Archive Posts Layout', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'list',
				'choices'     => $blogroll_choices,
			),
		),
	);

	// Blog Archive
	$options['blog_search'] = array(
		'title'    => esc_html__( 'Search', 'elvira' ),
		'priority' => 40,
		'panel'    => 'blog',
		'fields'   => array( 
			array(
				'id'          => $prefix . 'search_sidebar',
				'label'       => esc_html__( 'Search Page Sidebar Position', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'right-sidebar',
				'choices'     => $sidebar_choices,
			),
			array(
				'id'          => $prefix . 'search_blogroll',
				'label'       => esc_html__( 'Search Results Layout', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'list',
				'choices'     => $blogroll_choices,
			),
		),
	);

	// Blog Single
	$options['blog_single'] = array(
		'title'    => esc_html__( 'Post', 'elvira' ),
		'priority' => 50,
		'panel'    => 'blog',
		'fields'   => array( 
			array(
				'id'          => $prefix . 'single_sidebar',
				'label'       => esc_html__( 'Single Post Sidebar Position', 'elvira' ),
				'type'        => 'radio-image',
				'default'     => 'right-sidebar',
				'choices'     => $sidebar_choices,
			),
			array(
				'id'          => $prefix . 'featured_image',
				'label'       => esc_html__( 'Display featured image?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'related_posts',
				'label'       => esc_html__( 'Display related posts?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'author_info',
				'label'       => esc_html__( 'Display author info?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'related_posts_title',
				'label'       => esc_html__( 'Related Post Title', 'elvira' ),
				'type'        => 'text',
				'default'     => 'Related Posts',
			),
			array(
				'id'          => $prefix . 'author_info_title',
				'label'       => esc_html__( 'Author Info Title', 'elvira' ),
				'type'        => 'text',
				'default'     => 'Written By',
			),
		),
	);

	// Page
	$options['blog_page'] = array(
		'title'    => esc_html__( 'Page', 'elvira' ),
		'priority' => 60,
		'panel'    => 'blog',
		'fields'   => array(
			array(
				'id'          => $prefix . 'page_featured_image',
				'label'       => esc_html__( 'Display featured image?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => false,
			),
			array(
				'id'          => $prefix . 'page_disable_comment',
				'label'       => esc_html__( 'Disable comment on all pages', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => false,
			),
			array(
				'id'          => $prefix . 'page_author_info',
				'label'       => esc_html__( 'Display author info?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => false,
			),
			array(
				'id'          => $prefix . 'exclude_pages',
				'label'       => esc_html__( 'Exclude pages from search', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
		),
	);	


	// Styling
	$options['styling'] = array(
		'title'    => esc_html__( 'Styling', 'elvira' ),
		'priority' => 80,
		'fields'   => array(
			array(
				'id'          => $prefix . 'sidebar_style',
				'label'       => esc_html__( 'Widget Style', 'elvira' ),
				'type'        => 'select',
				'default'     => 'sidebar-1',
				'choices'     => array(
					'sidebar-1' => esc_html__( 'Transparent with Border', 'elvira' ),
					'sidebar-2' => esc_html__( 'White Block', 'elvira' ),
				),
			),
			array(
				'id'          => $prefix . 'sidebar_title_uppercase',
				'label'       => esc_html__( 'Uppercase widget title', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'html_14',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'text_color',
				'label'       => esc_html__( 'Text Color', 'elvira' ),
				'type'        => 'color',
				'default'     => '#4a4a4a',
			),
			array(
				'id'          => $prefix . 'accent_color',
				'label'       => esc_html__( 'Accent Color', 'elvira' ),
				'type'        => 'color',
				'default'     => '#89B399',
			),
			array(
				'id'          => $prefix . 'border_color',
				'label'       => esc_html__( 'Content & Widget Border Color', 'elvira' ),
				'type'        => 'color',
				'default'     => '#eeeeee',
			),
			array(
				'id'          => $prefix . 'readmore_background',
				'label'       => esc_html__( 'Readmore Button Background', 'elvira' ),
				'type'        => 'color',
				'default'     => '#89B399',
			),
			array(
				'id'          => $prefix . 'html_9',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'nav_background',
				'label'       => esc_html__( 'Menu Background', 'elvira' ),
				'type'        => 'color',
				'default'     => '#89B399',
			),
			array(
				'id'          => $prefix . 'nav_color',
				'label'       => esc_html__( 'Menu Color', 'elvira' ),
				'type'        => 'color',
				'default'     => '#ffffff',
			),
			array(
				'id'          => $prefix . 'nav_hover_color',
				'label'       => esc_html__( 'Menu Hover Color', 'elvira' ),
				'type'        => 'color',
				'default'     => '#ffffff',
			),
			array(
				'id'          => $prefix . 'nav_hover_background',
				'label'       => esc_html__( 'Menu Hover Background', 'elvira' ),
				'type'        => 'color',
				'default'     => '#7aa088',
			),
			array(
				'id'          => $prefix . 'nav_child_background',
				'label'       => esc_html__( 'Child Menu Background', 'elvira' ),
				'type'        => 'color',
				'default'     => '#93BFA3',
			),
			array(
				'id'          => $prefix . 'html_10',
				'type'        => 'html',
				'content'     => '<hr>',
			),		
			array(
				'id'          => $prefix . 'background_color',
				'label'       => esc_html__( 'Background Color', 'elvira' ),
				'type'        => 'color',
				'default'     => '#f9f9f9',
			),		
			array(
				'id'          => $prefix . 'background_image',
				'label'       => esc_html__( 'Background Image', 'elvira' ),
				'type'        => 'image',
				'default'     => '',
			),
			array(
				'id'          => $prefix . 'background_repeat',
				'label'       => esc_html__( 'Background Repeat', 'elvira' ),
				'type'        => 'select',
				'default'     => 'repeat',
				'choices'     => array(
					'repeat'    => 'Repeat',
					'repeat-x'  => 'Repeat X',
					'repeat-y'  => 'Repeat Y',
					'no-repeat' => 'No Repeat',
				),
			),
			array(
				'id'          => $prefix . 'background_size',
				'label'       => esc_html__( 'Background Size', 'elvira' ),
				'type'        => 'select',
				'default'     => 'auto',
				'choices'     => array(
					'auto'  => 'Auto',
					'cover' => 'Cover',
				),
			),
			array(
				'id'          => $prefix . 'background_position',
				'label'       => esc_html__( 'Background Position', 'elvira' ),
				'type'        => 'select',
				'default'     => 'left-top',
				'choices'     => array(
					'left-top'      => 'Left Top',
					'left-center'   => 'Left Center',
					'left-bottom'   => 'Left Bottom', 
					'right-top'     => 'Right Top',
					'right-center'  => 'Right Center',
					'right-bottom'  => 'Right Bottom',
					'center-top'    => 'Center Top',
					'center-center' => 'Center Center',
					'center-bottom' => 'Center Bottom',
				),
			),
			array(
				'id'          => $prefix . 'background_attachment',
				'label'       => esc_html__( 'Background Attachment', 'elvira' ),
				'type'        => 'select',
				'default'     => 'scroll',
				'choices'     => array(
					'scroll' => 'Scroll',
					'fixed'  => 'Fixed',
				),
			),
			array(
				'id'          => $prefix . 'html_11',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'footer_logo_background',
				'label'       => esc_html__( 'Footer Logo Background Color', 'elvira' ),
				'type'        => 'color',
				'default'     => '#89B399',
			),
		),
	);


	// Custom CSS
	$options['custom_css'] = array(
		'title'    => esc_html__( 'Custom CSS', 'elvira' ),
		'priority' => 80,
		'fields'   => array(
			array(
				'id'          => $prefix . 'custom_css',
				'label'       => esc_html__( 'Custom CSS', 'elvira' ),
				'description' => esc_html__( 'If you have any custom css code, you can add them here.', 'elvira' ),
				'type'        => 'code',
				'default'     => '',
			),
		),
	);


	// Typography
	$options['typography'] = array(
		'title'    => esc_html__( 'Typography', 'elvira' ),
		'priority' => 80,
		'fields'   => array(
			array(
				'id'          => $prefix . 'body_font_family',
				'label'       => esc_html__( 'Body Font', 'elvira' ),
				'type'        => 'typography',
				'default'     => 'rubik',
				'choices'     => themedsgn_get_fonts(),
				'style'       => 'width:100%;',
			),			
			array(
				'id'          => $prefix . 'body_font_size',
				'label'       => esc_html__( 'Body Font Size (px)', 'elvira' ),	
				'type'        => 'select',
				'default'     => '14',
				'choices'     => $font_size_choices,
			),
			array(
				'id'          => $prefix . 'html_12',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'nav_font_family',
				'label'       => esc_html__( 'Menu Font', 'elvira' ),
				'type'        => 'typography',
				'default'     => 'rubik',
				'choices'     => themedsgn_get_fonts(),
			),
			array(
				'id'          => $prefix . 'nav_font_size',
				'label'       => esc_html__( 'Menu Font Size (px)', 'elvira' ),
				'type'        => 'select',
				'default'     => '14',
				'choices'     => $font_size_choices,
			),
			array(
				'id'          => $prefix . 'nav_font_uppercase',
				'label'       => esc_html__( 'Uppercase menu text', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'html_13',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'heading_font_family',
				'label'       => esc_html__( 'Headings Font', 'elvira' ),
				'description' => esc_html__( 'Set headings font / H1, H2, H3, H4, H5, H6 including post title font.', 'elvira' ),
				'type'        => 'typography',
				'default'     => 'domine',
				'choices'     => themedsgn_get_fonts(),
			),
		),
	);


	$socials = array(
		'bloglovin'   => 'Bloglovin',
		'dribbble'    => 'Dribbble',
		'facebook'    => 'Facebook',
		'flickr'      => 'Flickr',
		'google_plus' => 'Google+',
		'instagram'   => 'Instagram',
		'lastfm'      => 'Last.fm',
		'linkedin'    => 'LinkedIn',
		'medium'      => 'Medium',
		'pinterest'   => 'Pinterest',
		'skype'       => 'Skype',
		'soundcloud'  => 'Soundcloud',
		'spotify'     => 'Spotify',
		'tumblr'      => 'Tumblr',
		'twitter'     => 'Twitter',
		'vimeo'       => 'Vimeo',
		'vine'        => 'Vine',
		'vk'          => 'Vk',
		'youtube'     => 'YouTube',
	);
	$social_media_fields = array();
	foreach( $socials as $key => $value ) {
		$social_media_fields[] = array(
			'id'          => $prefix . $key,
			'label'       => esc_html( $value ),
			'type'        => 'url',
			'default'     => '',
		);
	}
	// Socials
	$options['social'] = array(
		'title'       => esc_html__( 'Social Media', 'elvira' ),
		'description' => esc_html__( 'Enter your social media profile url on each field below (Note: Not your username). To disable, simply leave it blank.', 'elvira' ),
		'priority'    => 80,
		'fields'      => $social_media_fields,
	);


	// Footer
	$options['footer'] = array(
		'title'    => esc_html__( 'Footer', 'elvira' ),
		'priority' => 80,
		'fields'   => array(
			array(
				'id'          => $prefix . 'footer_socials',
				'label'       => esc_html__( 'Display footer social media links?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'show_footer_logo',
				'label'       => esc_html__( 'Display footer logo?', 'elvira' ),
				'type'        => 'checkbox',
				'default'     => true,
			),
			array(
				'id'          => $prefix . 'footer_logo',
				'label'       => esc_html__( 'Footer Logo', 'elvira' ),
				'type'        => 'image',
				'default'     => get_template_directory_uri() . '/assets/images/footer-logo.png',
			),
			array(
				'id'          => $prefix . 'html_7',
				'content'     => '<hr><br>Set your footer logo width and height. To use default fullsized image, set each field with 0 [zero].',
				'type'        => 'html',
			),
			array(
				'id'          => $prefix . 'footer_logo_width',
				'label'       => esc_html__( 'Footer Logo Width (px)', 'elvira' ),
				'type'        => 'number',
				'default'     => 0,
			),
			array(
				'id'          => $prefix . 'footer_logo_height',
				'label'       => esc_html__( 'Footer Logo Height (px)', 'elvira' ),
				'type'        => 'number',
				'default'     => 0,
			),
			array(
				'id'          => $prefix . 'html_8',
				'type'        => 'html',
				'content'     => '<hr>',
			),			
			array(
				'id'          => $prefix . 'copyright',
				'label'       => esc_html__( 'Copyright Text', 'elvira' ),
				'type'        => 'text',
				'default'     => 'Copyright &copy; ' . date('Y') . ' ' . get_bloginfo( 'name' ),
			),
			array(
				'id'          => $prefix . 'html_9',
				'type'        => 'html',
				'content'     => '<hr>',
			),
			array(
				'id'          => $prefix . 'footer_script',
				'label'       => esc_html__( 'Footer Script', 'elvira' ),
				'description' => esc_html__( 'If you have any additional script like Google Analytic code, you can add them here.', 'elvira' ),
				'type'        => 'code',
				'default'     => '',
			),
		),
	);
	

	// Panels
	$panels = array(
		'blog' => array(
			'title'       => esc_html__( 'Blog', 'elvira' ),
			'priority'    => 80,
			'description' => esc_html__( 'Customize your blog layout and appearance. Select sections below to start customizing.', 'elvira' ),
		),
	);

	$themedsgn_customizer = Themedsgn_Customizer::Instance();
	$themedsgn_customizer->set_panels( $panels );
	$themedsgn_customizer->set_options( $options );	
}
add_action( 'init', 'themedsgn_elvira_customizer' );