<?php
/**
 * Theme setup.
 */
/*MAIL FROM */
add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');

function new_mail_from($old) {
 return 'info@finteline.com';
}

function new_mail_from_name($old) {
 return 'INFO finteline.com';
}

/* CONFIGURACIÓN SMTP */
add_action('phpmailer_init','send_smtp_email');
function send_smtp_email( $phpmailer )
{
    // Define que estamos enviando por SMTP
    $phpmailer->isSMTP();
    // La dirección del HOST del servidor de correo SMTP p.e. smtp.midominio.com
    $phpmailer->Host = "smtp.finteline.com";
    // Uso autenticación por SMTP (true|false)
    $phpmailer->SMTPAuth = true;
    // Puerto SMTP - Suele ser el 25, 465 o 587
    $phpmailer->Port = "587";
    // Usuario de la cuenta de correo
    $phpmailer->Username = "info@finteline.com";
    // Contraseña para la autenticación SMTP
    $phpmailer->Password = "ArsysD0m100";
    // El tipo de encriptación que usamos al conectar - ssl (deprecated) o tls
    $phpmailer->SMTPSecure = "tls";
    $phpmailer->From = "info@finteline.com";
    $phpmailer->FromName = "INFO finteline.com";
}


if( !function_exists( 'themedsgn_elvira_theme_setup' ) ):
function themedsgn_elvira_theme_setup() {
	// Make the theme available for translation.
	load_theme_textdomain( 'elvira', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Let WordPress manage the document title.
	add_theme_support( 'title-tag' );

	// Enable supports for Post Thumbnails on Posts and Pages.
	add_theme_support( 'post-thumbnails' );

	// Register menus.
	register_nav_menus( array(
		'top' 		=> esc_html__( 'Top Menu', 'elvira' ),
		'main' 		=> esc_html__( 'Main Menu', 'elvira' ),
		'mobile' 	=> esc_html__( 'Mobile Menu', 'elvira' ),
	) );

	// Output valid html5.
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Custom image size.
	add_image_size( 'list-thumbnail', 700, 700, true );
	add_image_size( 'related-thumbnail', 400, 275, true );
	add_image_size( 'mini-thumbnail', 100, 80 );

	// Editor style.
	add_editor_style( array( 'assets/css/editor-style.css' ) );
}
endif;
add_action( 'after_setup_theme', 'themedsgn_elvira_theme_setup' );


/**
 * Set the content width.
 */
if( !function_exists( 'themedsgn_elvira_content_width' ) ):
function themedsgn_elvira_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'themedsgn_elvira_content_width', 784 );
}
endif;
add_action( 'after_setup_theme', 'themedsgn_elvira_content_width', 0 );


/**
 * Fonts url.
 */
if( !function_exists( 'themedsgn_elvira_fonts_url' ) ):
function themedsgn_elvira_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin';

	$body_font     = themedsgn_get_fonts( themedsgn_get_theme_mod( 'body_font_family', 'rubik' ) );
	$menu_font     = themedsgn_get_fonts( themedsgn_get_theme_mod( 'nav_font_family', 'rubik' ) );
	$headings_font = themedsgn_get_fonts( themedsgn_get_theme_mod( 'heading_font_family', 'domine' ) );

	// Main font
	$fonts[ themedsgn_get_theme_mod( 'body_font_family', 'rubik' ) ] = isset( $body_font['link'] ) ? $body_font['link'] : '';
	// Nav
	$fonts[ themedsgn_get_theme_mod( 'nav_font_family', 'rubik' ) ] = isset( $menu_font['link'] ) ? $menu_font['link'] : '';
	// Heading font
	$fonts[ themedsgn_get_theme_mod( 'heading_font_family', 'domine' ) ] = isset( $headings_font['link'] ) ? $headings_font['link'] : '';

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => implode( '|', $fonts ),
			'subset' => $subsets,
		), 'https://fonts.googleapis.com/css' );
	}
	return $fonts_url;
}
endif;


/**
 * Enqueue scripts and styles.
 */
if( !function_exists( 'themedsgn_elvira_scripts' ) ):
function themedsgn_elvira_scripts() {
	wp_enqueue_style( 'elvira-fonts', themedsgn_elvira_fonts_url(), array(), null );
	wp_enqueue_style( 'elvira-fontawesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css', array(), null );
	wp_enqueue_style( 'elvira-style', get_stylesheet_uri() );

	wp_enqueue_script( 'elvira-plugins', get_template_directory_uri() . '/assets/js/plugins.js', array( 'jquery', 'jquery-masonry' ), false, true );
	wp_enqueue_script( 'elvira-custom', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery', 'jquery-masonry' ), false, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
endif;
add_action( 'wp_enqueue_scripts', 'themedsgn_elvira_scripts' );


/**
 * Sidebar and widgets.
 */
if( !function_exists( 'themedsgn_elvira_widgets_init' ) ):
function themedsgn_elvira_widgets_init() {
	register_sidebar( array(
		'name'			=> esc_html__( 'Sidebar', 'elvira' ),
		'id'			=> 'sidebar-primary',
		'description'	=> esc_html__( 'Primary sidebar to display widgets.', 'elvira' ),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s clearfix">',
		'after_widget'	=> '</section>',
		'before_title'	=> '<h3 class="widget-title">',
		'after_title'	=> '</h3>',
	) );

	register_sidebar( array(
		'name'			=> esc_html__( 'Homepage Top', 'elvira' ),
		'id'			=> 'sidebar-homepage-top',
		'description'	=> esc_html__( 'Display widgets before website content. You can add posts slider or posts carousel into this area. Visible only on Homepage.', 'elvira' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s clearfix">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="widget-title"><span>',
		'after_title'	=> '</span></h3>',
	) );

	register_sidebar( array(
		'name'			=> esc_html__( 'Homepage Bottom', 'elvira' ),
		'id'			=> 'sidebar-homepage-bottom',
		'description'	=> esc_html__( 'Display widgets after website content. You can add advertisement or posts carousel into this area. Visible only on Homepage.', 'elvira' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s clearfix">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="widget-title"><span>',
		'after_title'	=> '</span></h3>',
	) );

	register_sidebar( array(
		'name'			=> esc_html__( 'Top Widgets Area', 'elvira' ),
		'id'			=> 'sidebar-top',
		'description'	=> esc_html__( 'Display widgets before website content. Visible on all pages except Homepage.', 'elvira' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s clearfix">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="widget-title"><span>',
		'after_title'	=> '</span></h3>',
	) );

	register_sidebar( array(
		'name'			=> esc_html__( 'Bottom Widgets Area', 'elvira' ),
		'id'			=> 'sidebar-bottom',
		'description'	=> esc_html__( 'Display widgets after website content. You can add advertisement or posts carousel into this area. Visible on all pages except Homepage.', 'elvira' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s clearfix">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="widget-title"><span>',
		'after_title'	=> '</span></h3>',
	) );

	register_sidebar( array(
		'name'			=> esc_html__( 'Instagram (Footer)', 'elvira' ),
		'id'			=> 'sidebar-instagram',
		'description'	=> esc_html__( 'Add instagram widget into this sidebar.', 'elvira' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s clearfix">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="widget-title">',
		'after_title'	=> '</h3>',
	) );

	register_sidebar( array(
		'name'			=> esc_html__( 'WooCommerce Sidebar', 'elvira' ),
		'id'			=> 'sidebar-woocommerce',
		'description'	=> esc_html__( 'Sidebar that appears on WooCommerce pages.', 'elvira' ),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s clearfix">',
		'after_widget'	=> '</section>',
		'before_title'	=> '<h3 class="widget-title">',
		'after_title'	=> '</h3>',
	) );

}
endif;
add_action( 'widgets_init', 'themedsgn_elvira_widgets_init' );


/**
 * Custom <body> classes
 */
if( !function_exists( 'themedsgn_elvira_body_classes' ) ):
function themedsgn_elvira_body_classes( $classes ) {
	if( is_404() || is_page_template( 'page-fullwidth.php' ) ) {
		$classes[] = 'fullwidth';
	} elseif( is_home() ) {
		$classes[] = themedsgn_get_theme_mod( 'homepage_sidebar', 'right-sidebar' );
	} elseif( is_archive() ) {
		$classes[] = themedsgn_get_theme_mod( 'archive_sidebar', 'right-sidebar' );
	} elseif( is_search() ) {
		$classes[] = themedsgn_get_theme_mod( 'search_sidebar', 'right-sidebar' );		
	} elseif( is_single() ) {
		$classes[] = themedsgn_get_theme_mod( 'single_sidebar', 'right-sidebar' );		
	} else {
		$classes[] = themedsgn_get_theme_mod( 'general_sidebar', 'right-sidebar' );
	}

	if( themedsgn_get_theme_mod( 'sticky_navigation', false ) ) {
		$classes[] = 'sticky-nav';
	}	

	return $classes;
}
endif;
add_filter( 'body_class', 'themedsgn_elvira_body_classes' );


/**
 * Custom post classes
 */
if( !function_exists( 'themedsgn_elvira_post_classes' ) ):
function themedsgn_elvira_post_classes( $classes ) {
	$classes[] = 'entry';
	$classes[] = 'clearfix';
	if( is_singular() ) {
		$classes[] = 'single-entry';
	}
	if( !has_post_thumbnail() ) {
		$classes[] = 'no-featured-image';
	}
	if( is_page_template( 'page-fullwidth.php' ) ) {
		$classes[] = 'fullwidth-template';
	}
	return $classes;
}
endif;
add_filter( 'post_class', 'themedsgn_elvira_post_classes' );


/**
 * Add custom script to wp_footer();
 */
if( !function_exists( 'themedsgn_elvira_wp_footer' ) ):
function themedsgn_elvira_wp_footer() {
	echo themedsgn_get_theme_mod( 'footer_script', '' );
}
endif;
add_action( 'wp_footer', 'themedsgn_elvira_wp_footer' );


/**
 * Get theme mod
 */
if( !function_exists( 'themedsgn_get_theme_mod' ) ):
function themedsgn_get_theme_mod( $key, $default ) {
	$prefix = 'elvira_option_';
	return get_theme_mod( $prefix . $key, $default );
}
endif;


/**
 * Load additional theme functions.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Custom widgets.
 */
require get_template_directory() . '/inc/widgets/widget-about.php';
require get_template_directory() . '/inc/widgets/widget-socials.php';
require get_template_directory() . '/inc/widgets/widget-posts.php';
require get_template_directory() . '/inc/widgets/widget-slider.php';
require get_template_directory() . '/inc/widgets/widget-carousel.php';

/**
 * Customizer.
 */
require get_template_directory() . '/inc/customizer/themedsgn-customizer.php';
require get_template_directory() . '/inc/customizer.php';

/**
 * TGMPA
 */
require get_template_directory() . '/inc/tgmpa.php';

/**
 * WooCommerce
 */
require get_template_directory() . '/inc/woocommerce.php';
