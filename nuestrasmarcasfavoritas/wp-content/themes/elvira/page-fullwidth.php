<?php 
/*
 * Template Name: Fullwidth Content
 */
get_header(); ?>

<div id="primary">
	<?php 
	if( have_posts() ):

		/* Start the loop. */
		while( have_posts() ): 
			the_post();
			get_template_part( 'content', 'page' );
		endwhile;

		/* Author info. */
		if( themedsgn_get_theme_mod( 'page_author_info', false ) ) {
			themedsgn_elvira_author_info();
		}

		/* Comments section. */		
		if ( ( comments_open() || get_comments_number() ) && !themedsgn_get_theme_mod( 'page_disable_comment', false ) ) :
			comments_template();
		endif;

	else: 

		/* No content found (404) */
		get_template_part( 'content', 'none' );

	endif;
	?>

</div>

<?php get_footer(); ?>