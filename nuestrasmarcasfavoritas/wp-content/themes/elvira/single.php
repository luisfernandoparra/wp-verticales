<?php get_header(); ?>

<div id="primary">
	<?php 
	if( have_posts() ):

		/* Start the loop. */
		while( have_posts() ): 
			the_post();
			get_template_part( 'content', 'single' );
		endwhile;

		/* Post navigation. */
		the_post_navigation();

		/* Related posts. */
		themedsgn_elvira_related_posts();

		/* Author info. */
		themedsgn_elvira_author_info();

		/* Comments section. */		
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;

	else: 

		/* No content found (404) */
		get_template_part( 'content', 'none' );

	endif;
	?>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>