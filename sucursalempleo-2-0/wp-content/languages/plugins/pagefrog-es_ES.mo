��          T      �       �   8   �      �         �     =   �     %  O  :  8   �     �     �  �   �     �     �                                        Facebook Instant Articles & Google AMP Pages by PageFrog PageFrog Team Save Settings The PageFrog plugin allows you to easily publish and manage your content directly from WordPress for Facebook Instant Articles (FBIA) and Google Accelerated Mobile Pages (AMP) with full support for ads and analytics. Used between list items, there is a space after the comma.,  http://pagefrog.com/ PO-Revision-Date: 2017-04-28 11:18:42+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Facebook Instant Articles &#38; Google AMP Pages by PageFrog - Development (trunk)
 Facebook Instant Articles & Google AMP Pages by PageFrog El equipo de PageFrog Guardar los Ajustes El plugin PageFrog te permite publicar y gestionar con facilidad tu contenido directamente desde WordPress a Facebook Instant Articles (FBIA) y Google Accelerated Mobile Pages (AMP) con total compatibilidad con anuncios y analíticas. ,  http://pagefrog.com/ 