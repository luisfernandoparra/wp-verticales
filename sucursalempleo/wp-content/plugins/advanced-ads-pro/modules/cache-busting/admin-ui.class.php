<?php
class Advanced_Ads_Pro_Module_Cache_Busting_Admin_UI {

	// Not ajax, is admin.
	public function __construct() {
		add_action( 'advanced-ads-placement-options-after', array( $this, 'admin_placement_options' ), 10, 2 );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );
		add_action( 'advanced-ads-ad-params-after', array( $this, 'check_ad' ), 9, 2 );
		//add_filter( 'advanced-ads-save-options', array( $this, 'save_options' ), 10, 2 );
	}

	/**
	 * add placement options on placement page
	 *
	 * @param string $placement_slug
	 * @param array  $placement
	 */
	public function admin_placement_options( $placement_slug, $placement ) {
		// l10n
		$label = _x( 'Cache-busting:', 'advanced-ads-pro', 'placement admin label' );
		$values = array(
			Advanced_Ads_Pro_Module_Cache_Busting::OPTION_ON => _x( 'ajax', 'advanced-ads-pro', 'setting label: ajax' ),
			Advanced_Ads_Pro_Module_Cache_Busting::OPTION_OFF => _x( 'off', 'advanced-ads-pro', 'setting label: off' ),
			Advanced_Ads_Pro_Module_Cache_Busting::OPTION_AUTO => _x( 'auto', 'advanced-ads-pro', 'setting label: auto' ),
		);

		// options
		$value = isset( $placement['options']['cache-busting'] ) ? $placement['options']['cache-busting'] : null;
		$value = $value === Advanced_Ads_Pro_Module_Cache_Busting::OPTION_ON ? Advanced_Ads_Pro_Module_Cache_Busting::OPTION_ON : ( $value === Advanced_Ads_Pro_Module_Cache_Busting::OPTION_OFF ? Advanced_Ads_Pro_Module_Cache_Busting::OPTION_OFF : Advanced_Ads_Pro_Module_Cache_Busting::OPTION_AUTO );

		$view = "<div id=\"advads-placement-${placement_slug}-cache-busting\"><span>$label</span>";
		foreach ( $values as $k => $l ) {
			$selected = checked( $value, $k, false );
			$view .= <<<EOD
<input${selected} type="radio" name="advads[placements][$placement_slug][options][cache-busting]" value="$k" id="advads-placement-${placement_slug}-cache-busting-{$k}"/><label for="advads-placement-${placement_slug}-cache-busting-${k}">$l</label>
EOD;
		}
		$view .= '</div>';

		echo $view;
	}

	/**
	 * enqueue scripts for validation the ad
	 */
	public function enqueue_admin_scripts() {
		$screen = get_current_screen();
		$uriRelPath = plugin_dir_url( __FILE__ );
		if ( isset( $screen->id ) && $screen->id === 'advanced_ads' ) { //ad edit page
			wp_register_script( 'krux/htmlParser', $uriRelPath . 'inc/htmlParser.js', array( 'jquery' ) );   
			wp_register_script( 'krux/postscribe', $uriRelPath . 'inc/postscribe.js', array( 'krux/htmlParser' ), '1.4.0' );
			wp_enqueue_script( 'cache-busting-admin', $uriRelPath . 'inc/admin.js', array( 'krux/postscribe' ) );

			// advads.get_cookie and similar functions may be used inside ad content
			Advanced_Ads_Plugin::get_instance()->enqueue_scripts();
		} elseif( $screen->id === 'advanced-ads_page_advanced-ads-groups' ) {
			wp_enqueue_script( 'cache-busting-admin', $uriRelPath . 'inc/admin.js', array() );
		}
	}

	/**
	 * add validation for cache-busting
	 *
	 * @param obj $ad ad object
	 * @param arr $types ad types
	 */
	public function check_ad( $ad, $types = array()  ) {
		$options = $ad->options();
		include dirname( __FILE__ ) . '/views/settings_check_ad.php';
	}

	// public function save_options( $options = array(), $ad = 0 ) {
	// 	if ( isset( $_POST['advanced_ad']['cache-busting']['possible'] ) ) {
	// 		$options['cache-busting']['possible'] = ('true' === $_POST['advanced_ad']['cache-busting']['possible'] ) ? true : false;
	// 	}
	// 	return $options;
	// }
}