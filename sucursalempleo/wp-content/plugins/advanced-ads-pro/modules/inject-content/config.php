<?php

// module configuration

$path = dirname( __FILE__ );

return array(
	'classmap' => array(
		'Advanced_Ads_Pro_Placement_Tests' => $path . '/placement-tests.class.php',
	),
	'textdomain' => null,
);