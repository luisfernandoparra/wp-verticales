jQuery(document).ready(function($){
	$('.advads-ad-group-type input').click(function(){
	    var grid_group_options = $(this).parents('.advads-ad-group-form').find('.advads-group-grid-options');
	    var refresh_group_options = $(this).parents('.advads-ad-group-form').find('.advads-group-refresh-options');
	    var grid_group_number_option = $(this).parents('.advads-ad-group-form').find('.advads-ad-group-number');
	    grid_group_options.hide();
	    if( 'grid' === $(this).val() ) {
			grid_group_options.show();
			// set number to all and hide setting
			grid_group_number_option.val('all').hide();
			refresh_group_options.hide();
	    } else if( 'default' === $(this).val() || 'ordered' === $(this).val() ) {
			grid_group_options.hide();
			grid_group_number_option.show();
			refresh_group_options.show();
	    }
	});
	$('.advads-ad-group-form').each(function(){
		if( 'grid' === $(this).find('.advads-ad-group-type input:checked').val()){
			$(this).find('.advads-ad-group-number').val('all').hide();
		}
	});
});