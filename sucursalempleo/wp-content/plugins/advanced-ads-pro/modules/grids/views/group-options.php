<div class="advads-group-grid-options"
    <?php if( 'grid' !== $group->type ) : ?>
	style="display: none;"
    <?php endif; ?>>
    <div>
        <label>
            <strong>
                <?php _e('Size', 'advanced-ads-pro' ); ?>
            </strong>
            <label><select name="advads-groups[<?php echo $group->id; ?>][options][grid][columns]">
		<?php for( $i = 1; $i <= 10; $i++ ) : ?>
		<option value="<?php echo $i; ?>"<?php selected( $i, $columns ); ?>><?php echo $i; ?></option>
		<?php endfor; ?>
	    </select><?php _e( 'columns', 'advanced-ads-pro' ); ?> x </label>
            <label><select name="advads-groups[<?php echo $group->id; ?>][options][grid][rows]">
		<?php for( $i = 1; $i <= 10; $i++ ) : ?>
		<option value="<?php echo $i; ?>"<?php selected( $i, $rows ); ?>><?php echo $i; ?></option>
		<?php endfor; ?>
	    </select><?php _e( 'rows', 'advanced-ads-pro' ); ?></label>
        </label>
        <br/>
        <label>
            <strong>
                <?php _e('Inner margin', 'advanced-ads-pro' ); ?>
            </strong>
            <input style="width:4em;" type="number" name="advads-groups[<?php echo $group->id; ?>][options][grid][inner_margin]" value="<?php echo $inner_margin; ?>" min="0" max="50"/>(<?php _e('in %', 'advanced-ads-pro' ); ?>)
        </label>
        <br/>
        <label>
            <strong>
                <?php _e('Min. width', 'advanced-ads-pro' ); ?>
            </strong>
            <input style="width:4em;" type="number" name="advads-groups[<?php echo $group->id; ?>][options][grid][min_width]" value="<?php echo $min_width; ?>"/>(<?php _e('in px', 'advanced-ads-pro' ); ?>)
        </label>
	<p class="description"><?php _e( 'Minimum width of an item. Prevents too small columns on mobile devices.', 'advanced-ads-pro' ); ?></p>
        <br/>
        <label>
            <strong>
                <?php _e('Random order', 'advanced-ads-pro' ); ?>
            </strong>
            <input type="checkbox" name="advads-groups[<?php echo $group->id; ?>][options][grid][random]"
            <?php if ($random) : ?>
                checked = "checked"
            <?php endif; ?>
            />
        </label>
    </div>
</div>