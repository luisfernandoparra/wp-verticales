<?php
$options = isset( $placement['options']['sticky'] ) ? $placement['options']['sticky'] : array();

//$enabled    = isset( $options['enabled'] ) ? $options['enabled'] : false;
$trigger    = isset( $options['trigger'] ) ? $options['trigger'] : '';
$delay     = isset( $options['delay'] ) ? absint( $options['delay'] ) : 0;
$effect     = isset( $options['effect'] ) ? $options['effect'] : 'show';
$duration   = isset( $options['duration'] ) ? absint( $options['duration'] ) : 0;

$option_name = "advads[placements][$placement_slug][options][sticky]";

?><div>
    <!--<input type="hidden" name="<?php echo $option_name; ?>[enabled]" value="1">-->

    <p class="description"><?php _e( 'Choose when the ad should show up.', AASADS_SLUG ); ?></p>
    <ul>
        <li><label><input type="radio" name="<?php echo $option_name; ?>[trigger]" value="" <?php checked( $trigger, '' ); ?>/><?php _e( 'right away', AASADS_SLUG ); ?></label></li>
        <li><label><input type="radio" name="<?php echo $option_name; ?>[trigger]" value="effect" <?php checked( $trigger, 'effect' ); ?>/><?php _e( 'right away with effect', AASADS_SLUG ); ?></label></li>
        <li>
            <label><input type="radio" name="<?php echo $option_name; ?>[trigger]" value="timeout" <?php checked( $trigger, 'timeout' ); ?>/><?php 
            printf( __( 'after %s seconds', AASADS_SLUG ), '<input type="number" name="' . $option_name . '[delay]" value="'.$delay.'"/>'); ?></label>
        </li>
    </ul>
    
    <!--<h4><?php _e( 'Effects', AASADS_SLUG ); ?></h4>-->
    <p class="description"><?php _e( 'Type of effect when the ad is being displayed', AASADS_SLUG ); ?></p>
        <label><input type="radio" name="<?php echo $option_name; ?>[effect]" value="show" <?php checked( $effect, 'show' ); ?>/><?php _e( 'Show', AASADS_SLUG ); ?></label>
        <label><input type="radio" name="<?php echo $option_name; ?>[effect]" value="fadein" <?php checked( $effect, 'fadein' ); ?>/><?php _e( 'Fade in', AASADS_SLUG ); ?></label>
        <label><input type="radio" name="<?php echo $option_name; ?>[effect]" value="slidedown" <?php checked( $effect, 'slidedown' ); ?>/><?php _e( 'Slide in', AASADS_SLUG ); ?></label>
    <p class="description"><?php _e( 'Duration of the effect (in milliseconds).', AASADS_SLUG ); ?></p>
    <input type="number" name="<?php echo $option_name; ?>[duration]" value="<?php echo $duration; ?>"/>
</div>
