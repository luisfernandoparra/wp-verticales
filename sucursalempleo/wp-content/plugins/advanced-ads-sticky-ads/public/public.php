<?php

class Advanced_Ads_Sticky {

		/**
		 * holds plugin base class
		 *
		 * @var Advanced_Ads_Sticky_Plugin
		 * @since 1.2.0
		 */
		protected $plugin;
		
		/**
		 * hold names of new placements
		 */
		protected $sticky_placements = array( 
		    'sticky_left_sidebar', 'sticky_right_sidebar',
		    'sticky_header', 'sticky_footer',
		    'sticky_left_window', 'sticky_right_window' );

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	public function __construct( $is_admin, $is_ajax ) {

		$this->plugin = Advanced_Ads_Sticky_Plugin::get_instance();

		add_action( 'plugins_loaded', array( $this, 'wp_plugins_loaded_ad_actions' ), 20 );

		// handle special ajax events
		if ( $is_ajax ) {
		} elseif ( ! $is_admin ) {
			// no ajax, no admin
			add_action( 'plugins_loaded', array( $this, 'wp_plugins_loaded' ) );
		}

	}

	/**
	 * load actions and filters needed only for ad rendering
	 *  this will make sure options get loaded for ajax and non-ajax-calls
	 */
	public function wp_plugins_loaded_ad_actions(){
		// stop, if main plugin doesn’t exist
		if ( ! class_exists( 'Advanced_Ads', false ) ) {
			return ;
		}

		// add wrapper options
		add_filter( 'advanced-ads-output-wrapper-options', array( $this, 'add_wrapper_options' ), 20, 2 );
		// action after ad output is created; used for js injection
		add_filter( 'advanced-ads-ad-output', array( $this, 'after_ad_output' ), 10, 2 );
		// add button to wrapper content
		add_filter('advanced-ads-output-wrapper-before-content', array($this, 'add_button'), 20, 2);
		// add close js to wrapper content
		add_filter('advanced-ads-output-wrapper-after-content', array($this, 'close_script'), 20, 2);
		// check if current placement can be displayed at all
		add_filter('advanced-ads-can-display-placement', array($this, 'placement_can_display'), 10, 2);
	}

	/**
	* load actions and filters
	*/
	public function wp_plugins_loaded() {
		// stop, if main plugin doesn’t exist
		if ( ! class_exists( 'Advanced_Ads', false ) ) {
			return ;
		}

		// add options to the wrapper
		add_filter( 'advanced-ads-set-wrapper', array( $this, 'set_wrapper' ), 20, 2 );
		// append js file into footer
		add_action( 'wp_enqueue_scripts', array( $this, 'footer_scripts' ) );
		// register sticky ad array in header
		add_action( 'wp_head', array( $this, 'register_js_array' ) );
		// inject ad content into footer
		add_action( 'wp_footer', array( $this, 'footer_injection' ), 10 );
	}

	/**
	 * register an array where all sticky ad ids are stored later
	 *
	 * @since 1.0.0
	 */
	public function register_js_array(){
		// print only, if js method is enabled
		$options = $this->plugin->options();
		if ( isset($options['sticky']['check-position-fixed']) && $options['sticky']['check-position-fixed'] ){
			echo '<script>advanced_ads_sticky_ads = new Array();</script>';
		}
	}

	/**
	 * append js file in footer
	 *
	 * @since 1.0.0
	 */
	public function footer_scripts(){
		// include file only if js method is enabled
		$options = $this->plugin->options();
		if ( isset($options['sticky']['check-position-fixed']) && $options['sticky']['check-position-fixed'] ){
			wp_enqueue_script( 'advanced-ads-sticky-footer-js', AASADS_BASE_URL . 'public/assets/js/sticky.js', array(), '1.1.1', true );
		}
	}

	/**
	 * set the ad wrapper options
	 *
	 * @since 1.0.0
	 * @param arr $wrapper wrapper options
	 * @param obj $ad ad object
	 */
	public function set_wrapper($wrapper = array(), $ad){
		$options = $ad->options();

		// define basic layer options
		if ( ! empty($options['sticky']['enabled']) && ! empty($options['sticky']['type']) ){
			$wrapper['class'][] = 'advads-sticky';
			$wrapper['style']['position'] = 'fixed';
			$wrapper['style']['z-index'] = 9999;

			switch ( $options['sticky']['type'] ) {
				case 'absolute' :

					if ( isset($options['sticky']['position']['top']) ) { $wrapper['style']['top'] = $options['sticky']['position']['top'] . 'px'; }
					if ( isset($options['sticky']['position']['right']) ) { $wrapper['style']['right'] = $options['sticky']['position']['right'] . 'px'; }
					if ( isset($options['sticky']['position']['bottom']) ) { $wrapper['style']['bottom'] = $options['sticky']['position']['bottom'] . 'px'; }
					if ( isset($options['sticky']['position']['left']) ) { $wrapper['style']['left'] = $options['sticky']['position']['left'] . 'px'; }
					break;
				case 'assistant' :
					$width = absint( $options['sticky']['position']['width'] );

					switch ( $options['sticky']['assistant'] ){
						case 'topleft' :
							$wrapper['style']['top'] = 0;
							$wrapper['style']['left'] = 0;
						break;
						case 'topcenter' :
							$wrapper['style']['margin-left'] = '-' . $width / 2 . 'px';
							$wrapper['style']['top'] = 0;
							$wrapper['style']['left'] = '50%';
						break;
						case 'topright' :
							$wrapper['style']['top'] = 0;
							$wrapper['style']['right'] = 0;
						break;
						case 'centerleft' :
							$wrapper['style']['bottom'] = '50%';
							$wrapper['style']['left'] = 0;
						break;
						case 'center' :
							$wrapper['style']['margin-left'] = '-' . $width / 2 . 'px';
							$wrapper['style']['bottom'] = '50%';
							$wrapper['style']['left'] = '50%';
						break;
						case 'centerright' :
							$wrapper['style']['bottom'] = '50%';
							$wrapper['style']['right'] = 0;
						break;
						case 'bottomleft' :
							$wrapper['style']['bottom'] = 0;
							$wrapper['style']['left'] = 0;
						break;
						case 'bottomcenter' :
							$wrapper['style']['margin-left'] = '-' . $width / 2 . 'px';
							$wrapper['style']['bottom'] = 0;
							$wrapper['style']['left'] = '50%';
						break;
						case 'bottomright' :
							$wrapper['style']['bottom'] = 0;
							$wrapper['style']['right'] = 0;
						break;
					}
				break;
			}
		} else {
			return $wrapper;
		}

		return $wrapper;
	}

	/**
	 * render a normal fixed box that is not moving
	 *
	 * @since 1.0.0
	 * @param string $content normal (html) content
	 * @param array $position position of the element with optional keys top, left, right, bottom
	 */
	public function render_fixed( $content = '', $position = 0 ) {
		// get random id for the ad in case there is more than one sticky ads
		list($output, $id) = self::_wrapper( $content, $position );
		return $output;
	}

	/**
	 * ad is moving until a specific position on the screen and is sticky there
	 *
	 * @since not yet implemented
	 * @param string $content normal (html) content
	 * @param array $position position of the element with optional keys top, left, right, bottom
	 */
	public function render_scroll_to($content = '', $position = 0) {
		if ( empty($position) ) {
			return $content; }
		// wrap the content without fixed position
		list($output, $id) = self::_wrapper( $content, 0 );

		// attach JS object with sticky information
		$js = '<script>jQuery(document).ready(function(){ wzSticky.init("' . $id . '", "", ' . $position['top'] . ');})</script>';
		$output .= $js;

		return $output;
	}

	/**
	 * ad is moving when a specific position on the screen is reached
	 */
	public function render_scroll_from($content = '', $position = 0) {
		if ( empty($position) ) {
			return $content; }

		// STILL NEEDS TO BE WRITTEN
	}

	/**
	 * inject ad placement into footer
	 *
	 * @since 1.2.3
	 */
	public function footer_injection(){
	    $placements = get_option( 'advads-ads-placements', array() );
	    foreach ( $placements as $_placement_id => $_placement ){
		    if ( isset($_placement['type']) && $this->is_sticky_placement( $_placement['type'] ) ){
			    echo Advanced_Ads_Select::get_instance()->get_ad_by_method( $_placement_id, Advanced_Ads_Select::PLACEMENT );
		    }
	    }
	}

	/**
	 * add sticky attributes to wrapper
	 *
	 * @since 1.2.3
	 * @param arr $options
	 * @param obj $ad ad object
	 */
	public function add_wrapper_options( $options = array(), Advanced_Ads_Ad $ad ){

	    if ( isset ( $ad->args['placement_type'] ) && $this->is_sticky_placement( $ad->args['placement_type'] ) ){
		    // hide ad if sticky trigger is given
		    if ( isset( $ad->args['sticky']['trigger'] ) && '' !== $ad->args['sticky']['trigger'] ){
			    $options['style']['display'] = 'none';
		    }
		    switch ( $ad->args['placement_type'] ){
			    case 'sticky_header' :
				    $options['style']['position'] = 'fixed';
				    $options['style']['top'] = '0';
				    if ( isset( $ad->args['sticky_bg_color'] ) && '' !== $ad->args['sticky_bg_color'] ){
					    $options['style']['left'] = '0';
					    $options['style']['right'] = '0';
					    $options['style']['background-color'] = $ad->args['sticky_bg_color'];
				    }
				    $options['style']['z-index'] = '1000';
				break;
			    case 'sticky_footer' :
				    $options['style']['position'] = 'fixed';
				    $options['style']['bottom'] = '0';
				    if ( isset( $ad->args['sticky_bg_color'] ) && '' !== $ad->args['sticky_bg_color'] ){
					    $options['style']['left'] = '0';
					    $options['style']['right'] = '0';
					    $options['style']['background-color'] = $ad->args['sticky_bg_color'];
				    }
				    $options['style']['z-index'] = '1000';
				break;
			    case 'sticky_left_sidebar' :
				    $options['style']['position'] = 'absolute';
				    $options['style']['display'] = 'inline-block';
				    $options['style']['left'] = '-' . $ad->width . 'px';
				    $options['style']['z-index'] = '1000';
				break;
			    case 'sticky_right_sidebar' :
				    $options['style']['position'] = 'absolute';
				    $options['style']['display'] = 'inline-block';
				    $options['style']['right'] = '-' . $ad->width . 'px';
				    $options['style']['z-index'] = '1000';
				break;
			    case 'sticky_left_window' :
				    $options['style']['position'] = 'absolute';
				    $options['style']['display'] = 'inline-block';
				    $options['style']['left'] = '0px';
				    $options['style']['z-index'] = '1000';
				break;
			    case 'sticky_right_window' :
				    $options['style']['position'] = 'absolute';
				    $options['style']['display'] = 'inline-block';
				    $options['style']['right'] = '0px';
				    $options['style']['z-index'] = '1000';
				break;
		    }
	    }
	    return $options;
	}

	/**
	 * inject js code to move ad into another element
	 *
	 * @since 1.2.3
	 * @param str $content ad content
	 * @param obj $ad ad object
	 */
	public function after_ad_output( $content = '', Advanced_Ads_Ad $ad ){
	    
	    // don’t use this if we are not in a sticky ad
	    if( ! isset( $ad->args['placement_type'] ) || ! $this->is_sticky_placement( $ad->args['placement_type'] ) ){
		    return $content;
	    }
	    
	    $target = '';
	    $options = array();
	    $fixed = ( isset( $ad->args['sticky_is_fixed'] ) && $ad->args['sticky_is_fixed'] ) ? true : false;
	    $centered = false;
	    $is_invisible = false;
	    switch ( $ad->args['placement_type'] ){
		    case 'sticky_left_sidebar' :
				if ( isset( $ad->args['sticky_element'] ) && '' !== $ad->args['sticky_element'] ){
					$target = $ad->args['sticky_element'];
				} else {
					$options[] = 'target:"wrapper"';
					$options[] = 'offset:"left"';
				}
			break;
		    case 'sticky_right_sidebar' :
				if ( isset( $ad->args['sticky_element'] ) && '' !== $ad->args['sticky_element'] ){
					$target = $ad->args['sticky_element'];
				} else {
					$options[] = 'target:"wrapper"';
					$options[] = 'offset:"right"';
				}
		    break;
		    case 'sticky_header' :
		    case 'sticky_footer' :
			    $centered = true;
		    break;
		    case 'sticky_left_window' :
		    case 'sticky_right_window' :
			    $target = 'body';
		    break;
		    default : return $content; // dont add output on placements not added by this plugin
	    }

	    $content .= '<script>advads.move("#'. $ad->wrapper['id'] .'", "' . $target . '", { '. implode( ',', $options ) .' });';
	    if ( $fixed ){
		    // add is_invisible option, if trigger and duration are set
		    if( isset( $ad->args['sticky']['trigger'] ) 
			&& 'timeout' === $ad->args['sticky']['trigger'] 
			&& isset( $ad->args['sticky']['duration'] ) ) {
			$options = ', {is_invisible: true}';
		    } else {
			$options = '';
		    }

		    $content .= 'jQuery(document).ready(function(){ advads.fix_element( "#'. $ad->wrapper['id'] .'"'.$options.' )});';
	    }
	    if ( $centered ){
		    // use width to center the ad
		    // might be resent, if background given
		    if( isset( $ad->width ) && $ad->width ){
			    $width = absint( $ad->width );
			    $content .= 'jQuery("#'. $ad->wrapper['id'] .'" ).css("width", ' . $width  . ');';
		    }
		    // center element with text-align, if background is selected
		    if ( isset( $ad->args['sticky_bg_color'] ) && '' !== $ad->args['sticky_bg_color'] ){
			    // check if there is a display setting already (maybe due to timeout
			    if ( isset( $ad->args['sticky']['trigger'] ) && '' !== $ad->args['sticky']['trigger'] ){
				$display = 'none';
			    } else {
				$display = 'block';
			    }
			    $content .= 'jQuery( "#'. $ad->wrapper['id'] .'" ).css({ textAlign: "center", display: "'. $display .'", width: "auto" });';
		    } else {
			    $content .= 'jQuery(document).ready(function(){ advads.center_fixed_element( "#'. $ad->wrapper['id'] .'" )});';
		    }
	    }

	    // center ad container vertically
	    if ( isset( $ad->args['sticky_center_vertical'] ) ){
		    // use height to center the ad
		    if( isset( $ad->height ) && $ad->height ){
			    $height = absint( $ad->height );
			    $content .= 'jQuery("#'. $ad->wrapper['id'] .'" ).css("height", ' . $height  . ');';

		    }
		    $content .= 'jQuery(document).ready(function(){ advads.center_vertically( "#'. $ad->wrapper['id'] .'" )});';
	    }

	    // choose effect and duration
	    $effect = false;
	    if( isset( $ad->args['sticky']['trigger'] ) 
		    && '' !== $ad->args['sticky']['trigger'] 
		    && isset( $ad->args['sticky']['effect'] ) ) {

		    $duration = isset( $ad->args['sticky']['duration'] ) ? absint( $ad->args['sticky']['duration'] ) : 0;
		    switch( $ad->args['sticky']['effect'] ){
			    case 'fadein' :
				$effect = "fadeIn($duration)";
				break;
			    case 'slidein' :
				$effect = "slideDown($duration)";
				break;
			    default : // show
				$effect = "show($duration)";
		    }
	    }

	    // use trigger
	    if( isset( $ad->args['sticky']['trigger'] ) ) {
			switch( $ad->args['sticky']['trigger'] ){
			    case 'effect' :
				$content .= 'jQuery("#'. $ad->wrapper['id'] .'").' . $effect . ';';
				break;
			    case 'timeout' :
				$delay = isset( $ad->args['sticky']['delay'] ) ? absint( $ad->args['sticky']['delay'] ) * 1000 : 0;
				$content .= 'jQuery("#'. $ad->wrapper['id'] .'").delay('. $delay .').' . $effect . ';';
				break;
		    }
	    }

	    $content .= '</script>';

	    return $content;
	}

	/**
	* add the close button to the wrapper

	* @since 1.4.1
	* @param string $content additional content added
	* @param obj $ad ad object
	*/
	public function add_button( $content = '', $ad = '' ) {

	    $options = $ad->options();
	    if( isset($options['close']['enabled']) && $options['close']['enabled'] ){
		    // build close button
		    $content .= $this->build_close_button($options['close']);
	    }

	    return $content;
	}

	/**
	* build the close button
	*
	* @since 1.4.1
	* @param arr $options original [close] part of the ad options array
	*/
	public function build_close_button( $options ){
	    $closebutton = '';
	    if(!empty($options['where']) && !empty($options['side'])) {
		switch($options['where']){
		    case 'inside' :
			$offset = '0';
			break;
		    default : $offset = '-15px';
		}
		switch($options['side']){
		    case 'left' :
			$side = 'left';
			break;
		    default : $side = 'right';
		}
		$closebutton = '<span class="advads-close-button" title="'.__('close', AASADS_SLUG)
			.'" style="width: 15px; height: 15px; background: #fff; position: absolute; top: 0; line-height: 15px; text-align: center; cursor: pointer; '.$side.':'.$offset.'">×</span>';
	    }

	    return $closebutton;
	}

	/**
	 * add the javascript for close and timeout feature

	 * @since 1.4.1
	 * @param string $content additional content added
	 * @param obj $ad ad object
	 */
	public function close_script( $content = '', $ad = '' ) {

		$options = $ad->options();
		// add close button
		if( isset($options['close']['enabled']) && $options['close']['enabled'] ){
			$script = 'jQuery("#' . $ad->wrapper['id'] . '").click( function() { advads.close("#'. $ad->wrapper['id'] .'"); ';
			// set time cookie
			if(!empty( $options['close']['timeout_enabled'])){
				$script .= 'advads.set_cookie("timeout_placement_' . $options['output']['placement_id'] . '", 1, '. absint( $options['close']['timeout'] ) .');';
				absint(  $ad->args['close']['timeout'] );
			}
			$content .= '<script>'. $script .'});</script>';
		}

		return $content;
	}

	/**
	 * check if placement was closed with a cookie before
	 *
	 * @since 1.4.1
	 * @param int $id placement id
	 * @return bool whether placement can be displayed or not
	 * @return bool false if placement was closed for this user
	 */
	public function placement_can_display( $return, $id = 0 ){

		// get all placements
		$placements = Advanced_Ads::get_ad_placements_array();

		if( ! isset( $placements[ $id ]['options']['close']['enabled'] ) || ! $placements[ $id ]['options']['close']['enabled'] ){
			return $return;
		}

		if( isset( $placements[ $id ]['options']['close']['timeout_enabled'] ) && $placements[ $id ]['options']['close']['timeout_enabled'] ){
			$slug = sanitize_title( $placements[ $id ]['name'] );
			if( isset( $_COOKIE[ 'timeout_placement_' . $slug ] ) ){
				return false;
			}
		}

		return $return;
	}
	
	/**
	 * add check if a specific placement belongs to Advanced Ads Sticky
	 * 
	 * @since 1.4.7
	 * @param str $placement string with placement
	 * @return bool true, if placement belongs to this add-on
	 */
	private function is_sticky_placement( $placement = '' ){
	    
		if( !$placement ){
		    return false;
		}
		
		return in_array( $placement, $this->sticky_placements );
		
	}
}
