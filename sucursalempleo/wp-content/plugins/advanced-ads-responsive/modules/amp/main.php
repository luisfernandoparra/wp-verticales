<?php
defined( 'WPINC' ) || exit;

define( 'AAR_AMP_PATH', plugin_dir_path( __FILE__ ) );
define( 'AAR_AMP_URL', plugin_dir_url( __FILE__ ) );

/**
 * Are we currently on an AMP URL?
 * Will always return `false` if called before the `parse_query` hook.
 *
 * @return bool true if amp url, false otherwise
 */
function advanced_ads_is_amp() {
	return ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() )
	|| ( function_exists( 'is_wp_amp' ) && is_wp_amp() );
}

if ( is_admin() ) {
	require_once( AAR_AMP_PATH . 'admin/admin.php' );
	new Advanced_Ads_Responsive_Amp_Admin;
}

require_once( AAR_AMP_PATH . 'public/public.php' );
new Advanced_Ads_Responsive_Amp;
