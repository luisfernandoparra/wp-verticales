<?php
/**
 * Add WooCommerce Support
 */
add_action( 'after_setup_theme', 'themedsgn_elvira_woocommerce_support' );
if( !function_exists( 'themedsgn_elvira_woocommerce_support' ) ):
function themedsgn_elvira_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
endif;


/**
 * WooCommerce Related Product Count
 */
add_filter( 'woocommerce_output_related_products_args', 'themedsgn_elvira_woocommerce_product_args' );
if( !function_exists( 'themedsgn_elvira_woocommerce_product_args' ) ):
function themedsgn_elvira_woocommerce_product_args( $args ) {
	$args[ 'posts_per_page' ] = 3;
	$args[ 'columns' ] = 1;
	return $args;
}
endif;