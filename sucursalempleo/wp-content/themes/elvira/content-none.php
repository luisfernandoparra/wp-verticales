<article class="entry entry-error-404 clearfix">
	<div class="entry-wrapper">
		<header class="entry-header">
			<?php 			
			if ( is_home() && current_user_can( 'publish_posts' ) ) {
				$title       = esc_html__( "Welcome", 'elvira' ); 
				$subtitle    = esc_html__( "Ready to publish your first post?", 'elvira' );
				$button_text = esc_html__( 'Get started here', 'elvira' );
				$button_url  = admin_url( 'post-new.php' );
				$show_button = true;
			} elseif( is_search() ) {
				$title       = esc_html__( "Nothing Found", 'elvira' );
				$subtitle    = esc_html__( "Sorry, but nothing matched your search terms. Please try again with some different keywords.", 'elvira' );
				$button_text = '';
				$button_url  = '';
				$show_button = false;
			} else {
				$title       = esc_html__( "Sorry this page isn't available", 'elvira' ); 
				$subtitle    = esc_html__( "The link you followed may be broken, or the page may have been removed.", 'elvira' );
				$button_text = esc_html__( 'Back to Homepage', 'elvira' );
				$button_url  = home_url( '/' );
				$show_button = true;
			}
			?>
			
			<?php if( !empty( $title ) ): ?>
				<h1><?php echo esc_html( $title ); ?></h1>
			<?php endif; ?>

			<?php if( !empty( $subtitle ) ): ?>
				<h2><?php echo esc_html( $subtitle ); ?></h2>
			<?php endif; ?>

			<?php if( $show_button ): ?>
				<a href="<?php echo esc_url( $button_url ); ?>" class="button round"><?php echo esc_html( $button_text ); ?></a>
			<?php endif; ?>			
								
		</header>
	</div>
</article><!-- .entry -->		