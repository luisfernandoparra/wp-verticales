<?php get_header(); ?>
<?php themedsgn_elvira_archive(); ?>

<div id="primary">
	<?php 
	if( have_posts() ):		

		themedsgn_elvira_before_blogroll();

		/* Start the loop. */
		while( have_posts() ): 
			the_post();
			get_template_part( 'content', themedsgn_get_theme_mod( 'archive_blogroll', 'list' ) );
		endwhile;

		themedsgn_elvira_after_blogroll();

		/* Navigation. */
		themedsgn_elvira_navigation();

	else: 

		/* No content found (404) */
		get_template_part( 'content', 'none' );

	endif;
	?>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>