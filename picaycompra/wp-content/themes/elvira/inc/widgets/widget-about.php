<?php
add_action( 'widgets_init', create_function('', 'return register_widget("themedsgn_widget_about");') );

class themedsgn_widget_about extends WP_Widget {

	public function __construct() {
		$widget_details = array(
			'classname'   => 'themedsgn_widget_about',
			'description' => esc_html__( 'Display your profile info.', 'elvira' ),
		);

		parent::__construct( 'themedsgn_widget_about', 'ThemeDsgn About Widget', $widget_details );

		add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
	}

	public function upload_scripts() {
		wp_enqueue_script( 'media-upload' );
		wp_enqueue_script( 'thickbox' );
		wp_enqueue_script( 'upload_about_widget', get_template_directory_uri() . '/inc/widgets/widget-about.js', array( 'jquery' ) );

		wp_enqueue_style( 'thickbox' );
	}

	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$image = isset( $instance['image'] ) ? $instance['image'] : '';
		$name  = isset( $instance['name'] ) ? $instance['name'] : '';
		$desc  = isset( $instance['desc'] ) ? $instance['desc'] : '';
		$text  = isset( $instance['text'] ) ? $instance['text'] : '';
		$round = isset( $instance['round'] ) ? (bool) $instance['round'] : false;
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:', 'elvira' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php esc_html_e( 'Image:', 'elvira' ); ?></label><br>
			<input class="widefat about_image_text" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" type="hidden" value="<?php echo esc_url( $image ); ?>">
			<input style="margin: 5px 0 10px;" type="button" class="about_image_upload button button-primary" value="Upload">
			<input style="margin: 5px 0 10px;" type="button" class="about_image_remove button" value="Remove">
			<?php if( !empty( $image ) ): ?>
			<img src="<?php echo esc_url( $image ); ?>" width="200">
			<?php endif; ?>
		</p>

		<p>
			<input class="checkbox" type="checkbox"<?php checked( $round ); ?> id="<?php echo $this->get_field_id( 'round' ); ?>" name="<?php echo $this->get_field_name( 'round' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'round' ); ?>"><?php esc_html_e( 'Use circle image?', 'elvira' ); ?></label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'name' ); ?>"><?php esc_html_e( 'Name:', 'elvira' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'name' ); ?>" name="<?php echo $this->get_field_name( 'name' ); ?>" type="text" value="<?php echo esc_attr( $name ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'desc' ); ?>"><?php esc_html_e( 'Short description:', 'elvira' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'desc' ); ?>" name="<?php echo $this->get_field_name( 'desc' ); ?>" type="text" value="<?php echo esc_attr( $desc ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php esc_html_e( 'Long description:', 'elvira' ); ?></label> 
			<textarea class="widefat" rows="15" cold="50" id="<?php echo $this->get_field_id( 'text' ) ?>" name="<?php echo $this->get_field_name( 'text' ) ?>"><?php echo esc_textarea( $text ); ?></textarea>
		</p>
		<?php

	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['image'] = esc_url_raw( $new_instance['image'] );
		$instance['name']  = sanitize_text_field( $new_instance['name'] );
		$instance['desc']  = sanitize_text_field( $new_instance['desc'] );
		$instance['text']  = wp_kses_post( $new_instance['text'] );
		$instance['round'] = (bool) $new_instance['round'];
		return $instance;
	}

	public function widget( $args, $instance ) {
		$title = !empty( $instance['title'] ) ? $instance['title'] : '';
		$image = !empty( $instance['image'] ) ? $instance['image'] : '';
		$name  = !empty( $instance['name'] ) ? $instance['name'] : '';
		$desc  = !empty( $instance['desc'] ) ? $instance['desc'] : '';
		$text  = !empty( $instance['text'] ) ? $instance['text'] : '';
		$round = !empty( $instance['round'] ) ? (bool) $instance['round'] : false;

		echo $args['before_widget'];
		if( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		?>
		<div class="about-widget">
			<?php if( $image ): ?><div class="about-image <?php echo $round ? 'circle-mask' : ''; ?>"><img src="<?php echo esc_url( $image ); ?>" alt="" width="200" height="200"></div><?php endif; ?>

			<div class="about-title">
				<?php if( $name ): ?><h3><?php echo esc_html( $name ); ?></h3><?php endif; ?>
				<?php if( $desc ): ?><div><?php echo esc_html( $desc ); ?></div><?php endif; ?>
			</div>

			<?php if( $text ): ?><div class="about-text"><?php echo esc_textarea( $text ); ?></div><?php endif; ?>
		</div>
		<?php
		echo $args['after_widget'];
	}

}