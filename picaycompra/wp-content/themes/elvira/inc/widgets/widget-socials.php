<?php
add_action( 'widgets_init', create_function('', 'return register_widget("themedsgn_widget_socials");') );

class themedsgn_widget_socials extends WP_Widget {
	public $social_icons;

	public function __construct() {
		$widget_details = array(
			'classname'   => 'themedsgn_widget_socials',
			'description' => esc_html__( 'Display social media links with icons.', 'elvira' ),
		);

		parent::__construct( 'themedsgn_widget_socials', 'ThemeDsgn Social Icons', $widget_details );

		$this->social_icons = array(
			'bloglovin'   => array( 'Bloglovin', 'fa fa-heart' ),
			'dribbble'    => array( 'Dribbble', 'fa fa-dribbble' ),
			'facebook'    => array( 'Facebook', 'fa fa-facebook' ),
			'flickr'      => array( 'Flickr', 'fa fa-flickr' ),
			'google_plus' => array( 'Google+', 'fa fa-google-plus' ),
			'instagram'   => array( 'Instagram', 'fa fa-instagram' ),
			'lastfm'      => array( 'Last.fm', 'fa fa-lastfm' ),
			'linkedin'    => array( 'LinkedIn', 'fa fa-linkedin' ),
			'medium'      => array( 'Medium', 'fa fa-medium' ),
			'pinterest'   => array( 'Pinterest', 'fa fa-pinterest' ),
			'skype'       => array( 'Skype', 'fa fa-skype' ),
			'soundcloud'  => array( 'Soundcloud', 'fa fa-soundcloud' ),
			'spotify'     => array( 'Spotify', 'fa fa-spotify' ),
			'tumblr'      => array( 'Tumblr', 'fa fa-tumblr' ),
			'twitter'     => array( 'Twitter', 'fa fa-twitter' ),
			'vimeo'       => array( 'Vimeo', 'fa fa-vimeo' ),
			'vine'        => array( 'Vine', 'fa fa-vine' ),
			'vk'          => array( 'Vk', 'fa fa-vk' ),
			'youtube'     => array( 'YouTube', 'fa fa-youtube-play' ),
		);
	}

	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$style = isset( $instance['style'] ) ? $instance['style'] : '';
		?>
		<p class="howto">
			Fill each social media field below with your social media profile URL (not username). To hide, simply leave the field blank.
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:', 'elvira' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'style' ); ?>"><?php esc_html_e( 'Icon style:', 'elvira' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>">
				<option <?php selected( $style, 'icon-circle' ); ?> value="icon-circle">Circle</option>
				<option <?php selected( $style, 'icon-round' ); ?> value="icon-round">Round</option>
				<option <?php selected( $style, 'icon-square' ); ?> value="icon-square">Square</option>
			</select>
		</p>

		<?php 
		foreach( $this->social_icons as $key => $value ): 
			$url = isset( $instance[$key] ) ? $instance[$key] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo esc_html( $value[0] ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="text" value="<?php echo esc_attr( $url ); ?>">
		</p>
		<?php endforeach; ?>

		<?php

	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['style'] = $new_instance['style'];
		foreach( $this->social_icons as $key => $value ) {
			$instance[$key] = $new_instance[$key];
		}
		return $instance;
	}

	public function widget( $args, $instance ) {
		$title = !empty( $instance['title'] ) ? $instance['title'] : '';
		$style = !empty( $instance['style'] ) ? $instance['style'] : '';

		echo $args['before_widget'];
		if( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		?>
		<div class="social-icons <?php echo esc_attr( $style ); ?>">
			<?php 
			foreach( $this->social_icons as $key => $value ): 
				$url = !empty( $instance[$key] ) ? $instance[$key] : '';
				if( !empty( $url ) ): ?>
				<a href="<?php echo esc_url( $url ); ?>" target="_blank" title="<?php echo esc_attr( $value[0] ); ?>"><i class="<?php echo esc_attr( $value[1] ); ?>"></i></a>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<?php
		echo $args['after_widget'];
	}

}