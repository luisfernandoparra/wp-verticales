jQuery(document).ready(function($){

	$(document).on('click', '.about_image_upload', function(){
		var parent  = $(this).parent();
		var imgText = parent.find('.about_image_text');

		window.send_to_editor = function(html) {
			var imgUrl = $('img', html).attr('src');

			imgText.val( imgUrl );
			parent.append( '<img src="'+ imgUrl +'" width="200">' );

			tb_remove();
		};

		tb_show('', 'media-upload.php?type=image&TB_iframe=true');
		return false;
	});

	$(document).on('click', '.about_image_remove', function(){
		var parent  = $(this).parent();
		parent.find('.about_image_text').val( '' );
		parent.find('img').remove();
		return false;
	});
});