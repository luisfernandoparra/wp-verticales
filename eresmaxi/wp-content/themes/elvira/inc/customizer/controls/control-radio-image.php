<?php
if ( ! class_exists( 'WP_Customize_Control' ) ) {
	return NULL;
}

class Themedsgn_Customize_Radio_Image_Control extends WP_Customize_Control {

	public $type    = 'radio-image';
	public $default = '';

	public function render_content() {
		if ( empty( $this->choices ) ) {
			return;
		}

		$name = '_customize-radio-' . $this->id;
		?>
		<span class="customize-control-title">
			<?php echo esc_attr( $this->label ); ?>
			<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
			<?php endif; ?>
		</span>
		<div id="input_<?php echo $this->id; ?>" class="themedsgn-radio-image-control">
			<?php foreach ( $this->choices as $value => $label ) : ?>
				<input class="radio-image" type="radio" value="<?php echo esc_attr( $value ); ?>" id="<?php echo $this->id . $value; ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?>>
					<label for="<?php echo $this->id . $value; ?>">
						<img src="<?php echo esc_html( $label['image'] ); ?>" alt="<?php echo esc_attr( $label['title'] ); ?>" title="<?php echo esc_attr( $label['title'] ); ?>">
					</label>
				</input>
			<?php endforeach; ?>
		</div>
		<?php
	}

}