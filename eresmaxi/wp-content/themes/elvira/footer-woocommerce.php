		</div><!-- #content -->

		<footer id="footer">
			<?php
			/*
			 * Instagram sidebar.
			 */
			if( is_active_sidebar( 'sidebar-instagram' ) ): ?>
			<div id="instagram-sidebar">
				<?php dynamic_sidebar( 'sidebar-instagram' ); ?>
			</div>
			<?php endif; ?>
			
			<?php
			/*
			 * Footer logo.
			 */
			themedsgn_elvira_footer_logo(); ?>

			<div class="main-footer">
				<div class="container">					
					<?php 
					if( themedsgn_get_theme_mod( 'footer_socials', true ) ) {
						themedsgn_elvira_social_icons( 'footer-socials' );
					}
					?>
					<div class="colophon">
						<?php echo esc_html( themedsgn_get_theme_mod( 'copyright', 'Copyright &copy; ' . date('Y') . ' ' . get_bloginfo( 'name' ) ) ); ?>
					</div>
				</div>
			</div><!-- .main-footer -->
		</footer><!-- #footer -->

	</div><!-- #site -->

	<?php wp_footer(); ?>
</body>
</html>