<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-wrapper">

		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>
		
		<?php if( has_post_thumbnail() && themedsgn_get_theme_mod( 'page_featured_image', false ) ): ?>
		<div class="entry-image">
			<?php the_post_thumbnail( 'full' ); ?>
		</div>
		<?php endif; ?>
							
		<div class="entry-content clearfix">
			<?php 
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="link-pages">',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>'
			) );
			?>
		</div>
		
		<footer class="entry-footer clearfix">
			<div class="entry-comment-meta">
				<span>
					<?php 
					comments_number( 
						themedsgn_get_theme_mod( 'comment_no_comment', esc_html__( 'No Comments', 'elvira' ) ),
						themedsgn_get_theme_mod( 'comment_one_comment', esc_html__( '1 Comment', 'elvira' ) ),
						sprintf( '%1$s %2$s', get_comments_number(), themedsgn_get_theme_mod( 'comment_many_comments', esc_html__( 'Comments', 'elvira' ) ) )
					); 
					?>
				</span>
			</div>
			<?php themedsgn_elvira_entry_share(); ?>
		</footer>

	</div>
</article>