
<?php get_header(); ?>

<div id="primary">
	<?php 
	if( have_posts() ):

		woocommerce_content();

	else: 

		/* No content found (404) */
		get_template_part( 'content', 'none' );

	endif;
	?>

</div>

<?php get_sidebar( 'woocommerce' ); ?>
<?php get_footer( 'woocommerce' ); ?>