<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-wrapper clearfix">

		<?php if( has_post_thumbnail() ): ?>
		<div class="entry-image">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'list-thumbnail' ); ?>
			</a>
		</div>
		<?php endif; ?>

		<div class="entry-content-wrapper">
			<header class="entry-header">
				<?php
				themedsgn_elvira_category_meta(); 
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				themedsgn_elvira_entry_meta( true, true, false );
				?>
			</header>		
								
			<div class="entry-content">
				<?php echo wp_trim_words( get_the_excerpt(), 30, '&hellip;' ); ?>
			</div>

			<footer class="entry-footer clearfix">
				<?php themedsgn_elvira_readmore(); ?>
			</footer>
		</div>

		<?php if( is_sticky() ): ?>
		<div class="sticky-indicator"></div>
		<?php endif; ?>

	</div>
</article>