<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://cohhe.com
 * @since             1.0
 * @package           shopera_quick_func
 *
 * @wordpress-plugin
 * Plugin Name:       Shopera quick view functionality
 * Plugin URI:        https://cohhe.com/
 * Description:       This plugin contains Shopera quick view functionality
 * Version:           1.1
 * Author:            Cohhe
 * Author URI:        http://cohhe.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       shopera-quick-view
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-shopera-quick-view-activator.php
 */
function shopera_quick_activate_shopera_quick_func() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-shopera-quick-view-activator.php';
	shopera_quick_func_Activator::shopera_quick_activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-shopera-quick-view-deactivator.php
 */
function shopera_quick_deactivate_shopera_quick_func() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-shopera-quick-view-deactivator.php';
	shopera_quick_func_Deactivator::shopera_quick_deactivate();
}

register_activation_hook( __FILE__, 'shopera_quick_activate_shopera_quick_func' );
register_deactivation_hook( __FILE__, 'shopera_quick_deactivate_shopera_quick_func' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
define('SKYCLEANERS_PLUGIN', plugin_dir_path( __FILE__ ));
require plugin_dir_path( __FILE__ ) . 'includes/class-shopera-quick-view.php';

// require_once plugin_dir_path( __FILE__ ) . 'includes/widgets/widget-about-author.php';
// require_once plugin_dir_path( __FILE__ ) . 'includes/widgets/widget-recent-posts-plus.php';
// require_once plugin_dir_path( __FILE__ ) . 'includes/widgets/widget-fast-flickr.php';
// require_once plugin_dir_path( __FILE__ ) . 'includes/widgets/widget-followers.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_shopera_quick_func() {

	$plugin = new shopera_quick_func();
	$plugin->shopera_quick_run();

}
run_shopera_quick_func();

function shopera_get_the_excerpt($post_id) {
  global $post;  
  $save_post = $post;
  $post = get_post($post_id);
  $output = get_the_excerpt();
  $post = $save_post;
  return $output;
}

function shopera_quick_add_html() {
	global $product;
	$product_gallery = $product->get_gallery_attachment_ids();
	$main_img = wp_get_attachment_image_src(get_post_thumbnail_id($product->id), 'shop_catalog');
	$product_img = '';
	$output = '';

	if ( !empty($main_img['0']) ) {
		$product_img = '<li class="selected"><img src="'.$main_img['0'].'" alt="Product image"></li>';;
	}

	if ( !empty($product_gallery) ) {
		rsort($product_gallery);
		foreach ($product_gallery as $gallery_value) {
			$img = wp_get_attachment_image_src($gallery_value, 'shop_catalog');
			$class = '';
			if ( $product_img == '' ) {
				$class = 'selected';
			}
			$product_img .= '<li class="'.$class.'"><img src="'.$img['0'].'" alt="Product image"></li>';
		}
	}

	$output .= '
	<div class="cd-quick-view">
		<div class="cd-slider-wrapper">
			<ul class="cd-slider">
				'.$product_img.'
			</ul> <!-- cd-slider -->

			<ul class="cd-slider-navigation">
				<li><a class="cd-next glyphicon glyphicon-chevron-left" href="javascript:void(0)"></a></li>
				<li><a class="cd-prev glyphicon glyphicon-chevron-right" href="javascript:void(0)"></a></li>
			</ul> <!-- cd-slider-navigation -->
		</div> <!-- cd-slider-wrapper -->

		<div class="cd-item-info">
			<h2>'.get_the_title( $product->id ).'</h2>
			<p>'.shopera_get_the_excerpt( $product->id ).'</p>
			<ul class="cd-item-action">
				<li>';
				ob_start();
				woocommerce_template_single_price();
				woocommerce_template_single_add_to_cart();
				$output .= ob_get_contents();
				ob_end_clean();
				$output .= '</li>
				<li class="read-more"><a href="'.get_the_permalink( $product->id ).'">'.__('Read more', 'shopera-quick-view').'</a></li>	
			</ul> <!-- cd-item-action -->
		</div> <!-- cd-item-info -->
		<a href="javascript:void(0)" class="cd-close">x</a>
	</div> <!-- cd-quick-view -->';

	echo $output;
}
add_action( 'woocommerce_after_shop_loop_item', 'shopera_quick_add_html', 15 );

function shopera_quick_add_quick() {
	echo '<span class="glyphicon glyphicon-eye-open quick-view-icon"></span>';
}
add_action( 'woocommerce_before_shop_loop_item_title', 'shopera_quick_add_quick', 15 );

function shopera_get_add_to_cart() {
	$output = '';

	ob_start();
	woocommerce_template_single_add_to_cart();
	$output .= ob_get_contents();
	ob_end_clean();

	return;
}