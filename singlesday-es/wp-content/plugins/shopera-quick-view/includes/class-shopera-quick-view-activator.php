<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    shopera_quick_func
 * @subpackage shopera_quick_func/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    shopera_quick_func
 * @subpackage shopera_quick_func/includes
 * @author     Your Name <email@example.com>
 */
class shopera_quick_func_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function shopera_quick_activate() {

	}

}
