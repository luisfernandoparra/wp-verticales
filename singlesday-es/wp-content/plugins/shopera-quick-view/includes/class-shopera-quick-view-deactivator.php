<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    shopera_quick_func
 * @subpackage shopera_quick_func/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    shopera_quick_func
 * @subpackage shopera_quick_func/includes
 * @author     Your Name <email@example.com>
 */
class shopera_quick_func_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function shopera_quick_deactivate() {

	}

}
