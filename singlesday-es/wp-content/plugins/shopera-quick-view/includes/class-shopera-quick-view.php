<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    shopera_quick_func
 * @subpackage shopera_quick_func/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    shopera_quick_func
 * @subpackage shopera_quick_func/includes
 * @author     Your Name <email@example.com>
 */
class shopera_quick_func {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      shopera_quick_func_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $shopera_quick_func    The string used to uniquely identify this plugin.
	 */
	protected $shopera_quick_func;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->shopera_quick_func = 'shopera-quick-view';
		$this->version = '1.0.0';

		$this->shopera_quick_load_dependencies();
		$this->shopera_quick_set_locale();
		$this->shopera_quick_define_admin_hooks();
		$this->shopera_quick_define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - shopera_quick_func_Loader. Orchestrates the hooks of the plugin.
	 * - shopera_quick_func_i18n. Defines internationalization functionality.
	 * - shopera_quick_func_Admin. Defines all hooks for the admin area.
	 * - shopera_quick_func_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function shopera_quick_load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-shopera-quick-view-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-shopera-quick-view-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-shopera-quick-view-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-shopera-quick-view-public.php';

		$this->loader = new shopera_quick_func_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the shopera_quick_func_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function shopera_quick_set_locale() {

		$plugin_i18n = new shopera_quick_func_i18n();
		$plugin_i18n->shopera_quick_set_domain( $this->shopera_quick_get_shopera_quick_func() );

		$this->loader->shopera_quick_add_action( 'plugins_loaded', $plugin_i18n, 'shopera_quick_load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function shopera_quick_define_admin_hooks() {

		$plugin_admin = new shopera_quick_func_Admin( $this->shopera_quick_get_shopera_quick_func(), $this->shopera_quick_get_version() );

		$this->loader->shopera_quick_add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->shopera_quick_add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function shopera_quick_define_public_hooks() {

		$plugin_public = new shopera_quick_func_Public( $this->shopera_quick_get_shopera_quick_func(), $this->shopera_quick_get_version() );

		$this->loader->shopera_quick_add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->shopera_quick_add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function shopera_quick_run() {
		$this->loader->shopera_quick_run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function shopera_quick_get_shopera_quick_func() {
		return $this->shopera_quick_func;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    shopera_quick_func_Loader    Orchestrates the hooks of the plugin.
	 */
	public function shopera_quick_get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function shopera_quick_get_version() {
		return $this->version;
	}

}
