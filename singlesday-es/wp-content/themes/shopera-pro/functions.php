<?php
/**
 * Shopera 1.0 functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */

/**
 * Set up the content width value based on the theme's design.
 *
 * @see shopera_content_width()
 *
 * @since Shopera 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 680;
}

if ( ! function_exists( 'shopera_setup' ) ) :
/**
 * Shopera 1.0 setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Shopera 1.0
 */

define('THEMENAME', 'ShoperaPRO');
define('SHORTNAME', 'shoperapro');
define('VH_HOME_TITLE', 'Home');
define('VH_DEVELOPER_NAME_DISPLAY', 'Cohhe themes');
define('VH_DEVELOPER_URL', 'http://cohhe.com');

function shopera_setup() {
	require(get_template_directory() . '/inc/metaboxes/layouts.php');

	/**
	* Required: include TGM.
	*/
	require_once( get_template_directory() . '/functions/tgm-activation/class-tgm-plugin-activation.php' );

	/**
	* Required: 3 step installer
	*/
	require_once(get_template_directory() . '/functions/installer/importer/widgets-importer.php');
	require_once(get_template_directory() . '/functions/installer/functions-themeinstall.php');	

	/**
	* Required: widgets
	*/
	require_once(get_template_directory() . '/inc/widgets/fast-flickr-widget.php');
	require_once(get_template_directory() . '/inc/widgets/widget-recent-products.php');

	/**
	* Required: Multiple sidebar support
	*/
	require_once(get_template_directory() . '/inc/multiple_sidebars.php');

	/*
	 * Make Shopera 1.0 available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Shopera 1.0, use a find and
	 * replace to change 'shopera' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'shopera', get_template_directory() . '/languages' );

	// This theme styles the visual editor to resemble the theme style.
	add_editor_style( array( 'css/editor-style.css' ) );

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 672, 372, true );
	add_image_size( 'shopera-full-width', 1170, 600, true );
	add_image_size( 'shopera-huge-width', 1800, 600, true );
	add_image_size( 'shopera-cart-item', 46, 46, true );
	add_image_size( 'shopera-brand-image', 170, 110, true );
	add_image_size( 'shopera-post-picture', 400, 260, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Top primary menu', 'shopera' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'shopera_custom_background_args', array(
		'default-color' => 'fafafa',
	) ) );

	// Add support for featured content.
	add_theme_support( 'featured-content', array(
		'featured_content_filter' => 'shopera_get_featured_posts',
		'max_posts' => 6,
	) );

	add_theme_support( 'featured-content-side', array(
		'featured_content_filter' => 'shopera_get_featured_side_posts',
		'max_posts' => 5,
	) );

	add_theme_support( 'title-tag' );

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );

	// Add support for featured content.
	// add_theme_support( 'featured-content', array(
	// 	'featured_content_filter' => 'shopera_get_featured_posts',
	// 	'max_posts' => 6,
	// ) );
}
endif; // shopera_setup
add_action( 'after_setup_theme', 'shopera_setup' );

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Shopera 1.0
 *
 * @return void
 */
function shopera_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
add_action( 'template_redirect', 'shopera_content_width' );

/**
 * Getter function for Featured Content Plugin.
 *
 * @since Shopera 1.0
 *
 * @return array An array of WP_Post objects.
 */
function shopera_get_featured_posts() {
	/**
	 * Filter the featured posts to return in Shopera 1.0.
	 *
	 * @since Shopera 1.0
	 *
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'shopera_get_featured_posts', array() );
}

function shopera_get_featured_side_posts() {
	/**
	 * Filter the featured posts to return in Shopera 1.0.
	 *
	 * @since Shopera 1.0
	 *
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'shopera_get_featured_side_posts', array() );
}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Shopera 1.0
 *
 * @return bool Whether there are featured posts.
 */
function shopera_has_featured_posts() {
	return ! is_paged() && (bool) shopera_get_featured_posts();
}

function shopera_has_featured_side_posts() {
	return ! is_paged() && (bool) shopera_get_featured_side_posts();
}

function shopera_is_static_page() {
	if ( get_option( 'show_on_front' ) == 'page' ) {
		return true;
	} else {
		return false;
	}
}

function shopera_fallback_page_menu() {    

	echo '<div class="menu-primary-container"><ul id="menu-primary" class="nav-menu">';

		wp_list_pages(array('title_li' => '', 'depth' => 1));

	echo '</ul></div>';

}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Shopera 1.0
 *
 * @return bool Whether there are featured posts.
 */

add_filter('add_to_cart_fragments', 'shopera_woocommerce_header_add_to_cart_fragment');
function shopera_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	
	ob_start();
	
	?>
	<div class="cart-contents">
		<span class="cart-items paint-area paint-area--text glyphicon glyphicon-shopping-cart"><span>
		<span class="cart-items-total"><?php echo $woocommerce->cart->cart_contents_count?></span>
		<div class="cart-content-list">
			<?php
				$cart_items = $woocommerce->cart->cart_contents;
				foreach ($cart_items as $cart_value) {
					$price = get_post_meta( $cart_value['product_id'], '_regular_price', true);

					switch( get_option( 'woocommerce_currency_pos' ) ) {
						case 'right':
							$item_price = $cart_value['line_subtotal'].get_woocommerce_currency_symbol();
							$total_item_price = $price.get_woocommerce_currency_symbol();
							$cart_subtotal = $woocommerce->cart->cart_contents_total.get_woocommerce_currency_symbol();
							break;
						case 'right_space':
							$item_price = $cart_value['line_subtotal'].' '.get_woocommerce_currency_symbol();
							$total_item_price = $price.' '.get_woocommerce_currency_symbol();
							$cart_subtotal = $woocommerce->cart->cart_contents_total.' '.get_woocommerce_currency_symbol();
							break;
						case 'left':
							$item_price = get_woocommerce_currency_symbol().$cart_value['line_subtotal'];
							$total_item_price = get_woocommerce_currency_symbol().$price;
							$cart_subtotal = get_woocommerce_currency_symbol() . $woocommerce->cart->cart_contents_total;
							break;
						case 'left_space':
							$item_price = get_woocommerce_currency_symbol().' '.$cart_value['line_subtotal'];
							$total_item_price = get_woocommerce_currency_symbol().' '.$price;
							$cart_subtotal = get_woocommerce_currency_symbol().' '.$woocommerce->cart->cart_contents_total;
							break;
						default:
							$item_price = get_woocommerce_currency_symbol().$cart_value['line_subtotal'];
							$total_item_price = get_woocommerce_currency_symbol().$price;
							$cart_subtotal = get_woocommerce_currency_symbol() . $woocommerce->cart->cart_contents_total;
							break;
					}

					echo '<div class="cart-list-item">';
						echo get_the_post_thumbnail($cart_value['data']->id, 'shopera-cart-item');
						echo '<div class="product-info">';
							echo '<span class="cart-item-title">'.$cart_value['data']->post->post_title.'</span>';
							echo '<span class="quantity">'.$cart_value['quantity'].' x <span>'.$item_price.'</span></span>';
						echo '</div>';
						echo '<span class="cart-item-price">'.$total_item_price.'</span>';
						echo '<div class="clearfix"></div>';
					echo '</div>';
				}
			?>
			<div class="cart-lower">
				<?php 
				if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) { ?>
				<a href="<?php echo esc_url( $woocommerce->cart->get_checkout_url() ); ?>" class="button left" title="<?php _e( 'Checkout', 'shopera' ) ?>"><?php _e( 'Checkout', 'shopera' ) ?></a>
				<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="button right"><?php _e( 'View Cart', 'shopera' ); ?></a>
				
				<span class="subtotal">
					<?php
					echo __('Subtotal:', 'shopera'). ' <span>' . $cart_subtotal .'</span>';
					?>
				</span>
				<div class="clearfix"></div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php
	
	$fragments['div.cart-contents'] = ob_get_clean();
	
	return $fragments;
}

/**
 * Register three Shopera 1.0 widget areas.
 *
 * @since Shopera 1.0
 *
 * @return void
 */
function shopera_widgets_init() {
	// register_sidebar( array(
	// 	'name'          => __( 'Primary Sidebar', 'shopera' ),
	// 	'id'            => 'sidebar-1',
	// 	'class'			=> 'col-sm-4 col-md-4 col-lg-4',
	// 	'description'   => __( 'Main sidebar that appears on the left.', 'shopera' ),
	// 	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	// 	'after_widget'  => '</aside>',
	// 	'before_title'  => '<div class="divider"><h3 class="widget-title">',
	// 	'after_title'   => '</h3><div class="separator"></div></div>',
	// ) );
	// register_sidebar( array(
	// 	'name'          => __( 'Content Sidebar', 'shopera' ),
	// 	'id'            => 'sidebar-2',
	// 	'class'			=> 'col-sm-4 col-md-4 col-lg-4',
	// 	'description'   => __( 'Additional sidebar that appears on the right.', 'shopera' ),
	// 	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	// 	'after_widget'  => '</aside>',
	// 	'before_title'  => '<div class="divider"><h3 class="widget-title">',
	// 	'after_title'   => '</h3><div class="separator"></div></div>',
	// ) );
	// register_sidebar( array(
	// 	'name'          => __( 'Shop Sidebar', 'shopera' ),
	// 	'id'            => 'sidebar-6',
	// 	'class'			=> 'col-sm-4 col-md-4 col-lg-4',
	// 	'description'   => __( 'Additional sidebar that appears on the right.', 'shopera' ),
	// 	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	// 	'after_widget'  => '</aside>',
	// 	'before_title'  => '<div class="divider"><h3 class="widget-title">',
	// 	'after_title'   => '</h3><div class="separator"></div></div>',
	// ) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area 1', 'shopera' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears in the footer section of the site.', 'shopera' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="divider"><h3 class="widget-title">',
		'after_title'   => '</h3><div class="separator"></div></div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area 2', 'shopera' ),
		'id'            => 'sidebar-4',
		'description'   => __( 'Appears in the footer section of the site.', 'shopera' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="divider"><h3 class="widget-title">',
		'after_title'   => '</h3><div class="separator"></div></div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area 3', 'shopera' ),
		'id'            => 'sidebar-5',
		'description'   => __( 'Appears in the footer section of the site.', 'shopera' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="divider"><h3 class="widget-title">',
		'after_title'   => '</h3><div class="separator"></div></div>',
	) );
}
add_action( 'widgets_init', 'shopera_widgets_init' );

/**
 * Register Google fonts for Shopera 1.0.
 *
 * @since Shopera 1.0
 *
 * @return string
 */
function shopera_font_url() {
	$fonts_url        = '';
	$roboto           = get_theme_mod('shopera_roboto', true);
	$roboto_slab      = get_theme_mod('shopera_roboto_slab', true);
	$roboto_condensed = get_theme_mod('shopera_roboto_condensed', true);
	$open_sans        = get_theme_mod('shopera_opensans', true);
	$satisfy          = get_theme_mod('shopera_satisfy', true);

	$font_families = array();
	
	if ( $roboto ) {
		$font_families[] = 'Raleway:400,100,300,700,900';
	}

	if ( $roboto_slab ) {
		$font_families[] = 'Raleway+Slab:400,100,300,700';
	}

	if ( $roboto_condensed ) {
		$font_families[] = 'Roboto Condensed:400,100,300,700,900';
	}

	if ( $open_sans ) {
		$font_families[] = 'Open+Sans:400,300,700';
	}

	if ( $satisfy ) {
		$font_families[] = 'Satisfy:400,100,300,700,900';
	}

	if ( !empty($font_families) ) {

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin' ),
		);

		$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}

function shopera_woocommerce_output_related_products() {

	$args = array(
		'posts_per_page' => 4,
		'columns' => 4,
		'orderby' => 'rand'
	);

	woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
}

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
// add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
// add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 11 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action( 'woocommerce_after_single_product_sidebar', 'shopera_woocommerce_output_related_products', 20 );

if ( defined('YITH_YWZM_VERSION') ) {
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
}

function shopera_get_customizer_fonts() {
	$customizer_css = '';
	$font_array = shopera_get_font_settings();

	foreach ($font_array as $font_key => $font_value) {
		$font_classes = $font_value['class'];
		$font_size = get_theme_mod('shopera_'.$font_key.'_fontsize', $font_value['size']);
		$line_height = get_theme_mod('shopera_'.$font_key.'_lineheight', $font_value['lineheight']);
		$font_family = get_theme_mod('shopera_'.$font_key.'_fontfamily', $font_value['fontfamily']);
		$font_weight = get_theme_mod('shopera_'.$font_key.'_fontweight', $font_value['fontweight']);

		$customizer_css .= $font_classes.'{font-size: '.$font_size.'px;line-height: '.$line_height.'px;font-family: "'.str_replace('+', ' ', $font_family).'";font-weight: '.$font_weight.';}';
	}

	return $customizer_css;
}

function shopera_create_google_css ($filename, $content) {
	if (!$handle = fopen($filename, 'w')) {
         echo "Cannot open file ($filename)";
    }

    // Write $somecontent to our opened file.
    if (fwrite($handle, $content) === FALSE) {
        echo "Cannot write to file ($filename)";
    }
}

function shopera_get_google_fonts() {
	$font_settings = shopera_get_font_settings();
	$fonts = $weights = array();
	$result = '';

	foreach ($font_settings as $font_key => $font_value) {
		$fonts[] = get_theme_mod('shopera_'.$font_key.'_fontfamily', $font_value['fontfamily']);
		$weights[] = get_theme_mod('shopera_'.$font_key.'_fontweight', $font_value['fontweight']);
	}

	$fonts = array_unique($fonts);
	$weights = implode( ',', array_unique( $weights ) );

	foreach ($fonts as $font_value) {
		$result[] .= $font_value.':'.$weights;
	}

	return implode( '|', $result);
}

add_action('customize_save_after', 'shopera_save_settings');
function shopera_save_settings( $fonts = '' ) {
	if ( $fonts == '' ) {
		$fonts = shopera_get_google_fonts();
	}
	$fonts_content = '@import url(\'http://fonts.googleapis.com/css?family='.$fonts.'&subset=latin\');';
	shopera_create_google_css(get_template_directory() . '/css/gfonts.css', $fonts_content);
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Shopera 1.0
 *
 * @return void
 */
function shopera_scripts() {
	wp_enqueue_script( 'shopera-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '', false );

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array() );

	// Add Google fonts
	// wp_register_style('googleFonts');
	wp_enqueue_style( 'googleFonts', shopera_font_url());

	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.0.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'shopera-style', get_stylesheet_uri(), array( 'genericons' ) );

	wp_enqueue_style( 'shopera-responsiveness', get_template_directory_uri() . '/css/responsive.css', array(), '3.0.2' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'shopera-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		wp_enqueue_script( 'jquery-masonry' );
	}

	if ( is_front_page() || is_page_template('template-two.php') ) {
		wp_enqueue_script( 'shopera-slider', get_template_directory_uri() . '/js/slider.js', array( 'jquery' ), '20131205', true );
	}

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery' ), '20131209', true );
	wp_enqueue_script( 'jcarousel', get_template_directory_uri() . '/js/jquery.jcarousel.pack.js', array( 'jquery' ), '20131209', true );

	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.min.css', array() );

	wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/js/html5.js', array(), '', false );

	wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array(), '', false );

	wp_localize_script( 'shopera-script', 'my_ajax', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	));

	// Color customizer
	if ( !is_customize_preview() && ( get_theme_mod('shopera_color_customizer', true) || current_user_can( 'manage_options' ) ) ) {
		wp_enqueue_script( 'custom-modernizer', get_template_directory_uri() . '/js/modernizr.custom.js', array(), '', true );
		wp_enqueue_script( 'interact', get_template_directory_uri() . '/js/interact-1.2.4.min.js', array(), '', true );
		wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/classie.js', array(), '', true );
		wp_enqueue_script( 'main-customizer', get_template_directory_uri() . '/js/customizer-main.js', array('custom-modernizer'), '', true );
	}
	
	wp_enqueue_script( 'jquery.cookie', get_template_directory_uri() . '/js/jquery.cookie.js', array('jquery'), '', true );

	// Load color customizations
	wp_add_inline_style( 'shopera-style', shopera_prepare_customizations() );

	// Load custom font styles
	wp_add_inline_style( 'shopera-style', shopera_get_customizer_fonts() );

	// Load custom CSS
	wp_add_inline_style( 'shopera-style', get_theme_mod('shopera_custom_css', '') );

	wp_enqueue_style( 'gfonts', get_template_directory_uri() . '/css/gfonts.css', array(), filemtime(get_template_directory() . '/css/gfonts.css') );

	wp_enqueue_script( 'colpick-js', get_template_directory_uri() . '/js/colpick.js', array( 'jquery' ), '', true );
	wp_enqueue_style( 'colpick-css', get_template_directory_uri() . '/css/colpick.css', array() );

	$shop_breadcrumb_img = get_theme_mod('shopera_shop_breadcrumbs', get_template_directory_uri().'/images/breadcrumbs.jpg');
	if ( $shop_breadcrumb_img ) {
		wp_add_inline_style( 'shopera-style', '.woocommerce-breadcrumb { background-image: url('.$shop_breadcrumb_img.'); }' );
	}
}
add_action( 'wp_enqueue_scripts', 'shopera_scripts' );

function shopera_custom_js_script() {
	echo htmlspecialchars_decode(get_theme_mod('shopera_custom_js', ''));
} 
add_action('wp_footer', 'shopera_custom_js_script');

function shopera_customizer_scripts() {
	wp_enqueue_script( 'customizer-helper', get_template_directory_uri() . '/js/customizer-helper.js', array('jquery'), '', true );
	wp_localize_script( 'customizer-helper', 'my_ajax', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	));
}
add_action( 'customize_controls_enqueue_scripts', 'shopera_customizer_scripts' );

add_filter( 'script_loader_tag', function( $tag, $handle ) {
	if ( $handle === 'html5shiv' ) {
		$tag = "<!--[if lt IE 9]>$tag<![endif]-->";
	}
	return $tag;
}, 10, 2 );


if ( ( $GLOBALS['pagenow'] == 'post.php' || $GLOBALS['pagenow'] == 'post-new.php' || $GLOBALS['pagenow'] == 'themes.php' ) && is_admin() ) {
	// Admin Javascript
	function shopera_admin_scripts() {
		wp_register_script('master', get_template_directory_uri() . '/inc/js/admin-master.js', array('jquery'));
		wp_enqueue_script('master');
	}
	add_action( 'admin_enqueue_scripts', 'shopera_admin_scripts' );
}

// Admin CSS
function shopera_admin_css() {
	wp_enqueue_style( 'admin-css', get_template_directory_uri() . '/css/wp-admin.css' );
}
add_action('admin_head','shopera_admin_css');

if ( ! function_exists( 'shopera_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 * @since Shopera 1.0
 *
 * @return void
 */
function shopera_the_attached_image() {
	$post                = get_post();
	/**
	 * Filter the default Shopera 1.0 attachment size.
	 *
	 * @since Shopera 1.0
	 *
	 * @param array $dimensions {
	 *     An array of height and width dimensions.
	 *
	 *     @type int $height Height of the image in pixels. Default 810.
	 *     @type int $width  Width of the image in pixels. Default 810.
	 * }
	 */
	$attachment_size     = apply_filters( 'shopera_attachment_size', array( 810, 810 ) );
	$next_attachment_url = wp_get_attachment_url();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID',
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id ) {
			$next_attachment_url = get_attachment_link( $next_id );
		}

		// or get the URL of the first image attachment.
		else {
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
		}
	}

	printf( '<a href="%1$s" rel="attachment">%2$s</a>',
		esc_url( $next_attachment_url ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image.
 * 3. Index views.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since Shopera 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function shopera_body_classes( $classes ) {
	global $wp_version;
	
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		$classes[] = 'footer-widgets';
	}

	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	if ( is_front_page() || is_page_template('template-two.php') || is_page_template('template-three.php') ) {
		$classes[] = 'slider';
	} elseif ( is_front_page() ) {
		$classes[] = 'grid';
	}

	$post_grid = get_theme_mod( 'shopera_post_grid', true );
	if ( ( is_archive() || is_search() || is_home() ) && ( $post_grid && !is_single() ) ) {
		$classes[] = 'post-grid';
	}

	$slider_status = get_theme_mod('shopera_main_slider', true);
	if ( $slider_status == false ) {
		$classes[] = 'slider-off';
	}

	if ( version_compare($wp_version, '4.4', '>=') ) {
		$classes[] = 'wp-post-4-4';
	}

	$slider_style = get_theme_mod('shopera_main_slider_style', 'default-style-slider');
	if ( is_page_template('template-two.php') ) {
		$slider_style = 'default-style-slider';
	} else if ( is_page_template('template-three.php') ) {
		$slider_style = 'three-column-slider';
	}
	$classes[] = $slider_style;

	if ( is_page_template('template-two.php') || is_page_template('template-three.php') ) {
		$classes[] = 'home';
	}

	$classes[] = SHOPERA_LAYOUT;

	return $classes;
}
add_filter( 'body_class', 'shopera_body_classes' );

function shopera_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'shopera' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'shopera' ), '<span class="edit-link button blue">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>" class="comment">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<?php
						$avatar_size = 60;
						echo get_avatar( $comment, $avatar_size );							
					?>
				</div><!-- .comment-author .vcard -->
			</div>

			<div class="comment-content">
				<?php
					echo '<a href="'.get_author_posts_url($comment->user_id).'" class="fn paint-area paint-area--text">' . get_comment_author_link() . '</a>';
					echo '<div class="reply-edit-container">';
					echo '<span class="comment-time paint-area paint-area--text">'.get_comment_time("F d, Y g:i a").'</span>';
				?>
					<span class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'shopera' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</span><!-- end of reply -->
					<?php edit_comment_link( __( 'Edit', 'shopera' ), '<span class="edit-link button blue paint-area paint-area--text">', '</span>' ); ?>
					<div class="clearfix"></div>
				</div>
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'shopera' ); ?></em>
				<?php endif; ?>
				<?php comment_text(); ?>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div><!-- end of comment -->

	<?php
			break;
	endswitch;
}

// custom comment fields
function shopera_custom_comment_fields($fields) {
	global $post, $commenter;

	$url_status = get_theme_mod('shopera_commenturl');

	$fields['author'] = '<div class="comment_auth_email"><p class="comment-form-author">
							<span class="input-label">' . __( 'Name', 'shopera' ) . '</span>
							<input id="author" name="author" type="text" class="span4" value="' . esc_attr( $commenter['comment_author'] ) . '" aria-required="true" size="30" />
						 </p>';

	$fields['email'] = '<p class="comment-form-email">
							<span class="input-label">' . __( 'Email', 'shopera' ) . '</span>
							<input id="email" name="email" type="text" class="span4" value="' . esc_attr( $commenter['comment_author_email'] ) . '" aria-required="true" size="30" />
						</p><div class="clearfix"></div></div>';

	$url_field = '<p class="comment-form-url">
						<span class="input-label">' . __( 'Website', 'shopera' ) . '</span>
						<input id="url" name="url" type="text" class="span4" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />
					</p>';


	$fields = array( $fields['author'], $fields['email'] );

	if ( $url_status == false ) {
		$fields[] = $url_field;
	}

	return $fields;
}
add_filter( 'comment_form_default_fields', 'shopera_custom_comment_fields' );

function shopera_woo_edit_before_shop_page() {
	$layout = get_post_meta(woocommerce_get_page_id( 'shop' ), 'layouts', true);
	if ( get_post_type() == 'product' && !is_shop() ) {
		$extra = '';
	} elseif ( $layout != 'full' ) {
		$extra = ' col-sm-9 col-md-9 col-lg-9';
	} else {
		$extra = '';
	}

	if ( $layout == 'left' && get_post_type() != 'product' ) {
		generated_dynamic_sidebar();
	}
	echo '<div class="woo-shop-page'.$extra.'">';
}
add_action( 'woocommerce_before_shop_loop', 'shopera_woo_edit_before_shop_page' );

function shopera_woo_edit_after_shop_page() {
	echo '</div>';
	$layout = get_post_meta(woocommerce_get_page_id( 'shop' ), 'layouts', true);
	if ( $layout == 'right' ) {
		generated_dynamic_sidebar();
	}
}
add_action( 'woocommerce_after_shop_loop', 'shopera_woo_edit_after_shop_page' );

function shopera_widget_class($params) {

	// its your widget so you add  your classes
	$classe_to_add = (strtolower(str_replace(array(' '), array(''), $params[0]['widget_name']))); // make sure you leave a space at the end
	if ( $classe_to_add == 'woocommercepricefilter' ) {
		$classe_to_add = 'price_filter';
	}
	$classe_to_add = 'class="widget_'.$classe_to_add . ' ';
	$params[0]['before_widget'] = str_replace('class="', $classe_to_add, $params[0]['before_widget']);

	return $params;
}
add_filter('dynamic_sidebar_params', 'shopera_widget_class');

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Shopera 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function shopera_post_classes( $classes ) {
	if ( ! post_password_required() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}

	return $classes;
}
add_filter( 'post_class', 'shopera_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Shopera 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function shopera_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'shopera' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'shopera_wp_title', 10, 2 );

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Add Theme Customizer functionality.
require get_template_directory() . '/inc/customizer.php';

/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
if ( ! class_exists( 'Shopera_Featured_Content' ) ) {
	require get_template_directory() . '/inc/featured-content.php';
}

if ( ! class_exists( 'Shopera_Featured_Content_Side' ) ) {
	require get_template_directory() . '/inc/featured-content-side.php';
}

/**
 * Create HTML list of nav menu items.
 * Replacement for the native Walker, using the description.
 *
 * @see    http://wordpress.stackexchange.com/q/14037/
 * @author toscho, http://toscho.de
 */
class Shopera_Header_Menu_Walker extends Walker_Nav_Menu {

	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth     Depth of menu item. May be used for padding.
	 * @param  array $args    Additional strings.
	 * @return void
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$classes         = empty ( $item->classes ) ? array () : (array) $item->classes;
		$has_description = '';

		$class_names = join(
			' '
		,   apply_filters(
				'nav_menu_css_class'
			,   array_filter( $classes ), $item
			)
		);

		// insert description for top level elements only
		// you may change this
		$description = ( ! empty ( $item->description ) )
			? '<small>' . esc_attr( $item->description ) . '</small>' : '';

		$has_description = ( ! empty ( $item->description ) )
			? 'has-description ' : '';

		! empty ( $class_names )
			and $class_names = ' class="' . $has_description . esc_attr( $class_names ) . '"';

		$output .= "<li id='menu-item-$item->ID' $class_names>";

		$attributes  = '';

		if ( !isset($item->target) ) {
			$item->target = '';
		}

		if ( !isset($item->attr_title) ) {
			$item->attr_title = '';
		}

		if ( !isset($item->xfn) ) {
			$item->xfn = '';
		}

		if ( !isset($item->url) ) {
			$item->url = '';
		}

		if ( !isset($item->title) ) {
			$item->title = '';
		}

		if ( !isset($item->ID) ) {
			$item->ID = '';
		}

		if ( !isset($args->link_before) ) {
			$args = new stdClass();
			$args->link_before = '';
		}

		if ( !isset($args->before) ) {
			$args->before = '';
		}

		if ( !isset($args->link_after) ) {
			$args->link_after = '';
		}

		if ( !isset($args->after) ) {
			$args->after = '';
		}

		! empty( $item->attr_title )
			and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
		! empty( $item->target )
			and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
		! empty( $item->xfn )
			and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
		! empty( $item->url )
			and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';

		$attributes .= ' class="paint-area paint-area--text"';

		$title = apply_filters( 'the_title', $item->title, $item->ID );

		$item_output = $args->before
			. "<a $attributes>"
			. $args->link_before
			. '<span>' . $title . '</span>'
			. $description
			. '</a> '
			. $args->link_after
			. $args->after;

		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters(
			'walker_nav_menu_start_el'
		,   $item_output
		,   $item
		,   $depth
		,   $args
		);
	}
}

function shopera_favicon() {
	$favicon = get_theme_mod('shopera_favicon');
	if ( $favicon ) {
		echo '<link rel="shortcut icon" href="' . esc_url( $favicon ) . '" />';
	} else {
		echo '<link rel="shortcut icon" href="' . get_template_directory_uri() . '/images/favicon.png" />';
	}
}
add_action('wp_head', 'shopera_favicon');

function shopera_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'shopera_woocommerce_support' );

function shopera_woocommerce_breadcrumbs( $defaults ) {

	$defaults['wrap_before'] = '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb"><div class="woo-breadcrumb-content"><span class="product-category paint-area paint-area--text">' . __( 'Producto', 'shopera' ) . '</span><div class="woo-breadcrumb-inner">';
	$defaults['wrap_after'] = '</div></div></nav>';

	return $defaults;
}
add_filter( 'woocommerce_breadcrumb_defaults', 'shopera_woocommerce_breadcrumbs' );

function woo_archive_custom_cart_button_text() {
	 return '';
}
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );

function shopera_logo_text_size() {
	$large_logo_font_size = get_theme_mod('shopera_logo_font_size', '50');
	$logo = get_theme_mod( 'shopera_logo' );

	if ( !$logo ) { ?>
		<style type="text/css">a.site-title { font-size: <?php echo esc_attr( $large_logo_font_size ).'px'; ?> }</style>
	<?php
	}
}
add_action('wp_head', 'shopera_logo_text_size');

function shopera_register_required_plugins() {

	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(
		array(
			'name'     				=> 'Shopera Functionality', // The plugin name
			'slug'     				=> 'functionality-for-shopera-theme', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '1.4', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'Tiled Galleries Carousel Without Jetpack', // The plugin name
			'slug'     				=> 'tiled-gallery-carousel-without-jetpack', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '2.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'WooCommerce', // The plugin name
			'slug'     				=> 'woocommerce', // The plugin slug (typically the folder name)
			'source'   				=> 'http://downloads.wordpress.org/plugin/woocommerce.zip', // The plugin source
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '2.6.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'Bootstrap Shortcodes for WordPress', // The plugin name
			'slug'     				=> 'bootstrap-3-shortcodes', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '3.3.8', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'Contact Form 7', // The plugin name
			'slug'     				=> 'contact-form-7', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '4.4.2', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'Newsletter', // The plugin name
			'slug'     				=> 'newsletter', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '4.2.2', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'Simple Share Buttons Adder', // The plugin name
			'slug'     				=> 'simple-share-buttons-adder', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '6.1.5', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'Easy Testimonials', // The plugin name
			'slug'     				=> 'easy-testimonials', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '1.31.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'YITH WooCommerce Wishlist', // The plugin name
			'slug'     				=> 'yith-woocommerce-wishlist', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '2.0.15', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'YITH WooCommerce Zoom Magnifier', // The plugin name
			'slug'     				=> 'yith-woocommerce-zoom-magnifier', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '1.2.19', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'Shopera quick view functionality', // The plugin name
			'slug'     				=> 'shopera-quick-view', // The plugin slug (typically the folder name)
			'source'   				=> get_template_directory() . '/functions/tgm-activation/plugins/shopera-quick-view.zip', // The plugin source
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '1.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name'     				=> 'Page Builder by SiteOrigin', // The plugin name
			'slug'     				=> 'siteorigin-panels', // The plugin slug (typically the folder name)
			'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '2.4.9', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		)
	);

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'       		=> 'shopera',                   // Text domain - likely want to be the same as your theme.
		'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_slug'     	=> 'themes.php', 				// The parent menu slug for the plugin install page
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
		'message' 			=> '',							// Message to output right before the plugins table
		'strings'      		=> array(
			'page_title'                       			=> __( 'Install Required Plugins', 'shopera' ),
			'menu_title'                       			=> __( 'Install Plugins', 'shopera' ),
			'installing'                       			=> __( 'Installing Plugin: %s', 'shopera' ), // %1$s = plugin name
			'oops'                             			=> __( 'Something went wrong with the plugin API.', 'shopera' ),
			'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'shopera' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'shopera' ), // %1$s = plugin name(s)
			'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'shopera' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'shopera' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'shopera' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'shopera' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'shopera' ), // %1$s = plugin name(s)
			'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'shopera' ), // %1$s = plugin name(s)
			'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'shopera' ),
			'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'shopera' ),
			'return'                           			=> __( 'Return to Required Plugins Installer', 'shopera' ),
			'plugin_activated'                 			=> __( 'Plugin activated successfully.', 'shopera' ),
			'complete' 									=> __( 'All plugins installed and activated successfully. %s', 'shopera' ), // %1$s = dashboard link
			'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
		)
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'shopera_register_required_plugins' );

add_action( 'wp_ajax_nopriv_update_shopera_customizer', 'shopera_update_customizer' );
add_action( 'wp_ajax_update_shopera_customizer', 'shopera_update_customizer' );
function shopera_update_customizer() {
	if ( isset( $_POST['path'] ) ? $path = sanitize_text_field( rtrim($_POST['path'], '.') ) : $path = '' );
	if ( isset( $_POST['element'] ) ? $element = sanitize_text_field( rtrim($_POST['element'], '.') ) : $element = '' );
	if ( isset( $_POST['parent'] ) ? $parent = sanitize_text_field( rtrim($_POST['parent'], '.') ) : $parent = '' );
	if ( isset( $_POST['type'] ) ? $type = sanitize_text_field( $_POST['type'] ) : $type = '' );
	if ( isset( $_POST['color'] ) ? $color = sanitize_text_field( $_POST['color'] ) : $color = '' );
	if ( isset( $_POST['tag'] ) ? $tag = sanitize_text_field( $_POST['tag'] ) : $tag = '' );
	if ( $type == 'area' ) {
		$color_type = 'background-color:';
	} elseif ( $type == 'text' ) {
		$color_type = 'color:';
	}
	if ( $tag == 'body' ) {
		$path = 'html body';
	}

	if ( $parent == '.undefined' ) {
		$parent = '';
	}

	if ( is_user_logged_in() && current_user_can( 'manage_options' ) ) {
		$customizer = get_option( 'shopera_customizer', '' );
		if ( $customizer == '' ) {
			$customizations = array($path => '{'.$color_type.$color.';}');
			update_option( 'shopera_customizer', $customizations);
		} else {
			$customizer[$path] = '{'.$color_type.$color.';}';
			update_option( 'shopera_customizer', $customizer);
		}
	} else {
		if ( !isset($_COOKIE['shopera_customizer']) ) {
			$customizations = array($path => '{'.$color_type.$color.';}');
			$date_of_expiry = time() + (10 * 365 * 24 * 60 * 60);
			ob_start();
			setcookie( 'shopera_customizer', json_encode($customizations), $date_of_expiry, "/");
			ob_flush();	
		} else {
			$customizer = (array)json_decode(str_replace('\\', '', $_COOKIE['shopera_customizer']));
			$customizer[$path] = '{'.$color_type.$color.';}';
			$date_of_expiry = time() + (10 * 365 * 24 * 60 * 60);
			ob_start();
			setcookie( 'shopera_customizer', json_encode($customizer), $date_of_expiry, "/");
			ob_flush();
		}
	}

	echo shopera_prepare_customizations();

	die(0);
}

function shopera_prepare_customizations() {
	$customizer = get_option( 'shopera_customizer', array() );
	if ( isset($_COOKIE['shopera_customizer']) ) { $cookies = (array)json_decode(str_replace('\\', '', $_COOKIE['shopera_customizer'])); } else { $cookies = array(); };
	$custom_css = '.test123 {color:red}';

	$css_array = array_merge($customizer, $cookies);

	if ( !empty($css_array) ) {
		foreach ($css_array as $class => $color) {
			$custom_css .= str_replace('\\', '', $class.$color);
		}
	}

	return $custom_css;
}

add_action( 'wp_ajax_nopriv_delete_shopera_customizer', 'shopera_delete_customizer' );
add_action( 'wp_ajax_delete_shopera_customizer', 'shopera_delete_customizer' );
function shopera_delete_customizer() {
	if ( is_user_logged_in() && current_user_can( 'manage_options' ) ) {
		delete_option( 'shopera_customizer' );
	} else if ( isset($_COOKIE['shopera_customizer']) ) {
		$date_of_expiry = time() - 100;
		setcookie( 'shopera_customizer', '', $date_of_expiry, "/");
	}

	die(1);
}

add_filter( "the_excerpt", "shopera_add_class_to_excerpt" );
function shopera_add_class_to_excerpt( $excerpt ) {
	return str_replace('<p', '<p class="excerpt paint-area paint-area--text"', $excerpt);
}

add_filter( "comment_text", "shopera_comment_text" );
function shopera_comment_text( $comment ) {
	return '<p class="comment-text paint-area paint-area--text">'.$comment.'</p>';
}

add_filter('comment_reply_link', 'shopera_replace_reply_link_class');
function shopera_replace_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='comment-reply-link paint-area paint-area--text", $class);
    return $class;
}

add_filter( 'woocommerce_sale_flash', 'shopera_sale_flash');
function shopera_sale_flash( $sale ) {
	global $product;

	if ( $product->is_on_sale() ) {

		return '<span class="onsale paint-area">' . __( 'Sale!', 'woocommerce' ) . '</span>';

	}
}

add_action( 'woocommerce_before_shop_loop_item_title', 'shopera_template_loop_price', 10 );
function shopera_template_loop_price() {
	global $product;

	if ( $price_html = $product->get_price_html() ) {
		$price_html = str_replace('class="amount"', 'class="amount paint-area paint-area--text"', $price_html);
		echo '<span class="price paint-area">'.$price_html.'</span>';
	}
}

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'shopera_template_loop_rating', 5 );
function shopera_template_loop_rating() {
	global $product;

	if ( get_option( 'woocommerce_enable_review_rating', '' ) === 'no' ) {
		return;
	}

	if ( $rating_html = $product->get_rating_html() ) {
		echo str_replace("span ", "span class='single-product-rating paint-area paint-area--text'", $rating_html);
	}
}

add_action( 'woocommerce_after_shop_loop_item_title', 'shopera_template_loop_add_to_cart', 11 );
function shopera_template_loop_add_to_cart() {
	global $product;

	echo apply_filters( 'woocommerce_loop_add_to_cart_link',
		sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button %s product_type_%s paint-area paint-area--text">%s</a>',
			esc_url( $product->add_to_cart_url() ),
			esc_attr( $product->id ),
			esc_attr( $product->get_sku() ),
			esc_attr( isset( $quantity ) ? $quantity : 1 ),
			$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
			esc_attr( $product->product_type ),
			esc_html( $product->add_to_cart_text() )
		),
	$product );
}

function shopera_content_paint_class( $content ) {
	$content = str_replace('<p>', '<p class="content paint-area paint-area--text">', $content);
	return $content;
}
add_filter( 'the_content', 'shopera_content_paint_class' );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary', 'shopera_template_single_title', 5 );
function shopera_template_single_title() {
	echo '<h1 itemprop="name" class="product_title entry-title paint-area paint-area--text">'.get_the_title().'</h1>';
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'shopera_template_single_price', 10 );
function shopera_template_single_price() {
	global $product;

	echo '
	<div class="single-product-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

		<p class="price paint-area paint-area--text">'.$product->get_price_html().'</p>

		<meta itemprop="price" content="'.$product->get_price().'" />
		<meta itemprop="priceCurrency" content="'.get_woocommerce_currency().'" />
		<link itemprop="availability" href="http://schema.org/'.($product->is_in_stock() ? 'InStock' : 'OutOfStock').'" />

	</div>';
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'shopera_template_single_excerpt', 20 );
function shopera_template_single_excerpt() {
	global $post;

	if ( ! $post->post_excerpt ) {
		return;
	}

	echo '<div class="single-product-description-wrapper" itemprop="description">';
		echo str_replace('<p>', '<p class="single-product-excerpt paint-area paint-area--text">', apply_filters( 'woocommerce_short_description', $post->post_excerpt ));
	echo '</div>';
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'shopera_template_single_meta', 40 );
function shopera_template_single_meta() {
	global $post, $product;

	$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
	$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

	echo '<div class="product_meta paint-area paint-area--text">';

		do_action( 'woocommerce_product_meta_start' );

		if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) {

			echo '<span class="sku_wrapper">'.__( 'SKU:', 'woocommerce' ).' <span class="sku" itemprop="sku">'. ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ).'</span>.</span>';

		}

		echo str_replace('<a', '<a class="single-product-category paint-area paint-area--text"', $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '.</span>' ));

		echo str_replace('<a', '<a class="single-product-tag paint-area paint-area--text"', $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '.</span>' ));

		do_action( 'woocommerce_product_meta_end' );

	echo '</div>';
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_after_single_product_summary', 'shopera_output_product_data_tabs', 10 );
function shopera_output_product_data_tabs() {
	$tabs = apply_filters( 'woocommerce_product_tabs', array() );

	if ( ! empty( $tabs ) ) {

		echo '
		<div class="woocommerce-tabs">
			<ul class="tabs">';
				foreach ( $tabs as $key => $tab ) {

					echo '<li class="'.esc_attr( $key ).'_tab">
						<a class="single-product-tab paint-area paint-area--text" href="#tab-'.esc_attr( $key ).'">'. apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ).'</a>
					</li>';

				}
			echo '</ul>';

			foreach ( $tabs as $key => $tab ) {

			
				echo '<div class="panel entry-content" id="tab-'.esc_attr( $key ).'">';
				call_user_func( $tab['callback'], $key, $tab );
				echo '</div>';

			}

		echo '</div>';
	}
}

add_action( 'wp_ajax_nopriv_reload_gfonts_file', 'shopera_reload_gfonts' );
add_action( 'wp_ajax_reload_gfonts_file', 'shopera_reload_gfonts' );
function shopera_reload_gfonts() {
	if ( isset( $_POST['font_family'] ) ? $font_family = sanitize_text_field( str_replace('\\', '', $_POST['font_family'] ) ) : $font_family = '' );

	$fonts = array_unique( json_decode($font_family) );
	$weights = '100,300,400,700';
	$result = '';

	foreach ($fonts as $font_value) {
		$result[] .= $font_value.':'.$weights;
	}

	shopera_save_settings( implode( '|', $result) );

	echo 'Fonts regenerated';

	die(1);
}

add_action( 'wp_ajax_nopriv_reset_customizer_fonts', 'shopera_reset_customizer_fonts' );
add_action( 'wp_ajax_reset_customizer_fonts', 'shopera_reset_customizer_fonts' );
function shopera_reset_customizer_fonts() {
	$customizer = get_option('theme_mods_shopera-pro', array());
	$font_settings = shopera_get_font_settings();

	foreach ($font_settings as $font_key => $font_value) {
		if ( isset($customizer['shopera_'.$font_key.'_fontsize']) ) {
			unset($customizer['shopera_'.$font_key.'_fontsize']);
		}
		if ( isset($customizer['shopera_'.$font_key.'_lineheight']) ) {
			unset($customizer['shopera_'.$font_key.'_lineheight']);
		}
		if ( isset($customizer['shopera_'.$font_key.'_fontfamily']) ) {
			unset($customizer['shopera_'.$font_key.'_fontfamily']);
		}
		if ( isset($customizer['shopera_'.$font_key.'_fontweight']) ) {
			unset($customizer['shopera_'.$font_key.'_fontweight']);
		}
	}

	update_option('theme_mods_shopera-pro', $customizer);

	die(1);
}

function shopera_get_font_settings() {
	return array(
		'primary' =>
			array(
				'class' => 'body.single .entry-content p, .summary.entry-summary div[itemprop=description] p, .woocommerce div.product .woocommerce-tabs .panel p:not(.stars), .woocommerce #reviews #comments ol.commentlist li .comment-text .comment-text, .comment-content > p, .myaccount_address, .col-1.address address, .col-2.address address, .woocommerce .myaccount_user, .woocommerce-info, .entry-summary p, .add-content .cd-item-info p',
				'size' => '14',
				'lineheight' => '24',
				'fontfamily' => 'Raleway',
				'fontweight' => '300',
				'title' => __('Primary font', 'shopera'),
				'description' => __('This is your sites primary font', 'shopera')
			),
		'headingone' =>
			array(
				'class' => '#main h1',
				'size' => '26',
				'lineheight' => '36',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Heading H1', 'shopera'),
				'description' => __('All heading H1 fonts', 'shopera')
			),
		'headingtwo' =>
			array(
				'class' => '#main h2',
				'size' => '24',
				'lineheight' => '24',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Heading H2', 'shopera'),
				'description' => __('All heading H2 fonts', 'shopera')
			),
		'headingthree' =>
			array(
				'class' => '#main h3',
				'size' => '22',
				'lineheight' => '24',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Heading H3', 'shopera'),
				'description' => __('All heading H3 fonts', 'shopera')
			),
		'headingfour' =>
			array(
				'class' => '#main h4',
				'size' => '20',
				'lineheight' => '24',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Heading H4', 'shopera'),
				'description' => __('All heading H4 fonts', 'shopera')
			),
		'headingfive' =>
			array(
				'class' => '#main h5',
				'size' => '18',
				'lineheight' => '24',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Heading H5', 'shopera'),
				'description' => __('All heading H5 fonts', 'shopera')
			),
		'headingsix' =>
			array(
				'class' => '#main h6',
				'size' => '16',
				'lineheight' => '24',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Heading H6', 'shopera'),
				'description' => __('All heading H6 fonts', 'shopera')
			),
		'links' =>
			array(
				'class' => '.site-footer-wrapper .site-info a, .entry-meta-full a, .reply-edit-container a, .widget a, .widget_recent_entries ul li a, .product_meta a, .yith-wcwl-add-button a, .addresses .title a, .woocommerce .myaccount_user a, .woocommerce-info a, .entry-meta-right .post-by a',
				'size' => '16',
				'lineheight' => '24',
				'fontfamily' => 'Raleway',
				'fontweight' => '300',
				'title' => __('Normal links', 'shopera'),
				'description' => __('This is your sites links font', 'shopera')
			),
		'widgettitle' =>
			array(
				'class' => '#main h3.widget-title, #footer-sidebar h3.widget-title, #main .single-product-sidebar h3',
				'size' => '21',
				'lineheight' => '23',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Widget title', 'shopera'),
				'description' => __('Your sites widget title font', 'shopera')
			),
		'sidebar' =>
			array(
				'class' => '#main .widget p, .site-footer-container .widget .textwidget, .recentcomments',
				'size' => '16',
				'lineheight' => '24',
				'fontfamily' => 'Raleway',
				'fontweight' => '300',
				'title' => __('Widget font', 'shopera'),
				'description' => __('This is your sites widgets font', 'shopera')
			),
		'pagetitle' =>
			array(
				'class' => 'body.single #main .entry-header h1',
				'size' => '36',
				'lineheight' => '50',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Page title', 'shopera'),
				'description' => __('Your sites page title font', 'shopera')
			),
		'slidertitlecat' =>
			array(
				'class' => '.swiper-slide .entry-header .category-title-top',
				'size' => '29',
				'lineheight' => '53',
				'fontfamily' => 'Satisfy',
				'fontweight' => '300',
				'title' => __('Frontpage slider category', 'shopera'),
				'description' => __('Your frontpage sliders category font', 'shopera')
			),
		'slidertitle' =>
			array(
				'class' => '.swiper-slide .entry-header .entry-title, .swiper-slide .entry-header .entry-title a',
				'size' => '46',
				'lineheight' => '65',
				'fontfamily' => 'Roboto',
				'fontweight' => '700',
				'title' => __('Frontpage slider title', 'shopera'),
				'description' => __('Your frontpage slider title font', 'shopera')
			),
		'slidertext' =>
			array(
				'class' => '.swiper-slide .slider-content p',
				'size' => '14',
				'lineheight' => '18',
				'fontfamily' => 'Raleway',
				'fontweight' => '400',
				'title' => __('Frontpage slider text', 'shopera'),
				'description' => __('Frontpage slider text font', 'shopera')
			),
		'slidersidetitle' =>
			array(
				'class' => '.swiper-slide.side .entry-header .entry-title a',
				'size' => '20',
				'lineheight' => '30',
				'fontfamily' => 'Satisfy',
				'fontweight' => '700',
				'title' => __('Frontpage slider side title', 'shopera'),
				'description' => __('Frontpage slider side title font', 'shopera')
			),
		'slidersidetext' =>
			array(
				'class' => 'body .swiper-slide.side .slider-content p',
				'size' => '14',
				'lineheight' => '18',
				'fontfamily' => 'Roboto+Condensed',
				'fontweight' => '400',
				'title' => __('Frontpage slider side text', 'shopera'),
				'description' => __('Frontpage slider side text font', 'shopera')
			),
		'singleitemtitle' =>
			array(
				'class' => '#main.container .woocommerce ul.products li.product h3, body.woocommerce-page #main ul.products li.product h3',
				'size' => '14',
				'lineheight' => '24',
				'fontfamily' => 'Roboto+Condensed',
				'fontweight' => '600',
				'title' => __('Shop single item', 'shopera'),
				'description' => __('Your shops single item title', 'shopera')
			),
		'testimonialtext' =>
			array(
				'class' => '.container .in-content-testimonial .testimonial-data .testimonial-content',
				'size' => '15',
				'lineheight' => '27',
				'fontfamily' => 'Raleway',
				'fontweight' => '400',
				'title' => __('Testimonial text', 'shopera'),
				'description' => __('Testimonial slider text font', 'shopera')
			),
		'testimonialauthor' =>
			array(
				'class' => '.container .in-content-testimonial .testimonial-data .testimonial-author',
				'size' => '16',
				'lineheight' => '29',
				'fontfamily' => 'Raleway',
				'fontweight' => '700',
				'title' => __('Testimonial author text', 'shopera'),
				'description' => __('Testimonial slider author text font', 'shopera')
			),
		'testimonialcompany' =>
			array(
				'class' => '.container .in-content-testimonial .testimonial-data .testimonial-company',
				'size' => '13',
				'lineheight' => '24',
				'fontfamily' => 'Raleway',
				'fontweight' => '700',
				'title' => __('Testimonial company text', 'shopera'),
				'description' => __('Testimonial slidet company text font', 'shopera')
			),
		'categorysctitle' =>
			array(
				'class' => '.woo-category-inner .woo-category-title',
				'size' => '36',
				'lineheight' => '30',
				'fontfamily' => 'Roboto',
				'fontweight' => '700',
				'title' => __('Category shortcode title', 'shopera'),
				'description' => __('Category shortcode title font', 'shopera')
			),
		'categorysctext' =>
			array(
				'class' => '.woo-category-inner .woo-category-excerpt',
				'size' => '14',
				'lineheight' => '18',
				'fontfamily' => 'Raleway',
				'fontweight' => '300',
				'title' => __('Category shortcode text', 'shopera'),
				'description' => __('Category shortcode text font', 'shopera')
			),
		'menufont' =>
			array(
				'class' => '#menu-primary li a span',
				'size' => '16',
				'lineheight' => '30',
				'fontfamily' => 'Roboto+Condensed',
				'fontweight' => '300',
				'title' => __('Menu font', 'shopera'),
				'description' => __('Your sites main menu font', 'shopera')
			),
		'footertext' =>
			array(
				'class' => '.site-info-content .copyright, .site-info-content .copyright a',
				'size' => '13',
				'lineheight' => '24',
				'fontfamily' => 'Raleway',
				'fontweight' => '300',
				'title' => __('Footer text', 'shopera'),
				'description' => __('Your sites footer text font', 'shopera')
			),
		'openpmeta' =>
			array(
				'class' => '.entry-meta-full > span, .entry-meta-full > span a',
				'size' => '14',
				'lineheight' => '42',
				'fontfamily' => 'Raleway',
				'fontweight' => '300',
				'title' => __('Open post meta', 'shopera'),
				'description' => __('Open posts meta font (below post image)', 'shopera')
			),
		'openproducttitle' =>
			array(
				'class' => '#main .woocommerce div.product .product_title',
				'size' => '26',
				'lineheight' => '36',
				'fontfamily' => 'Roboto',
				'fontweight' => '300',
				'title' => __('Open product title', 'shopera'),
				'description' => __('Open product title font', 'shopera')
			)
	);
}

add_action( 'load-post.php', 'shopera_metabox_setup' );
add_action( 'load-post-new.php', 'shopera_metabox_setup' );
function shopera_metabox_setup() {

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'shopera_add_metabox' );

	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'shopera_save_metabox', 10, 2 );
}

function shopera_save_metabox( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['shopera_post_metabox_nonce'] ) || !wp_verify_nonce( $_POST['shopera_post_metabox_nonce'], basename( __FILE__ ) ) )
	return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
	return $post_id;

	$meta_values = array(
		'shopera_external_url',
		'shopera_top_title',
		'shopera_text_below_title',
		'shopera_button_text',
		'shopera_button_link'
		);

	foreach ($meta_values as $a_meta_value) {
		/* Get the posted data and sanitize it for use as an HTML class. */
		$new_meta_value   = ( isset( $_POST[$a_meta_value] ) ? sanitize_text_field( $_POST[$a_meta_value] ) : '' );

		/* Get the meta key. */
		$meta_key   = $a_meta_value;

		/* Get the meta value of the custom field key. */
		$meta_value   = get_post_meta( $post_id, $meta_key, true );

		/* If a new meta value was added and there was no previous value, add it. */
		if ( $new_meta_value && '' == $meta_value )
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );

		/* If the new meta value does not match the old value, update it. */
		elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );

		/* If there is no new meta value but an old value exists, delete it. */
		elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
	}

}

function shopera_add_metabox() {

	add_meta_box(
		'shopera_metabox',                                   // Unique ID
		__('Featured content advanced fields, <a href="http://documentation.cohhe.com/shopera/knowledgebase/featured-content-external-url/" style="font-size: 14px;">documentation</a>', 'shopera'),  // Title
		'shopera_metabox_function',                          // Callback function
		'post',                                           // Admin page (or post type)
		'normal',                                           // Context
		'high'                                              // Priority
	);
}

function shopera_metabox_function( $object, $box ) { ?>

	<?php wp_nonce_field( basename( __FILE__ ), 'shopera_post_metabox_nonce' ); ?>

	<p>
		<label for="shopera_external_url"><?php _e( "External URL:", 'shopera' ); ?></label>
		<br />
		<input class="widefat" type="text" name="shopera_external_url" id="shopera_external_url" value="<?php echo esc_attr( get_post_meta( $object->ID, 'shopera_external_url', true ) ); ?>" size="30" />
		<?php _e('Enter your URL here if you want to link this post to external URL at the featured content.', 'shopera'); ?>
	</p>

	<p>
		<label for="shopera_top_title"><?php _e( "Top title:", 'shopera' ); ?></label>
		<br />
		<input class="widefat" type="text" name="shopera_top_title" id="shopera_top_title" value="<?php echo esc_attr( get_post_meta( $object->ID, 'shopera_top_title', true ) ); ?>" size="30" />
		<?php _e('Enter any text here and it\'ll be displayed above the main title at the featured content.', 'shopera'); ?>
	</p>

	<p>
		<label for="shopera_text_below_title"><?php _e( "Text below main title:", 'shopera' ); ?></label>
		<br />
		<input class="widefat" type="text" name="shopera_text_below_title" id="shopera_text_below_title" value="<?php echo esc_attr( get_post_meta( $object->ID, 'shopera_text_below_title', true ) ); ?>" size="30" />
		<?php _e('For two and three column content styles there\'s a text below the main title, enter it here.', 'shopera'); ?>
	</p>

	<p>
		<label for="shopera_button_text"><?php _e( "Button text:", 'shopera' ); ?></label>
		<br />
		<input class="widefat" type="text" name="shopera_button_text" id="shopera_button_text" value="<?php echo esc_attr( get_post_meta( $object->ID, 'shopera_button_text', true ) ); ?>" size="30" />
		<?php _e('Enter the text that\'s going to be shown for the button at the featured content.', 'shopera'); ?>
	</p>

	<p>
		<label for="shopera_button_link"><?php _e( "Button URL:", 'shopera' ); ?></label>
		<br />
		<input class="widefat" type="text" name="shopera_button_link" id="shopera_button_link" value="<?php echo esc_attr( get_post_meta( $object->ID, 'shopera_button_link', true ) ); ?>" size="30" />
		<?php _e('Enter the URL that you want your button to be linked with.', 'shopera'); ?>
	</p>

<?php }

function shopera_category_module( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'categories' => ''
	), $atts ) );

	global $wpdb;
	$output = '';
	$formatted_images = array();

	if ( $categories ) {
		$category = explode(',', $categories);

		$output .= '<div class="shopera-category-wrapper">';
		foreach ($category as $cat_value) {
			$category_data = get_term( $cat_value, 'product_cat' );
			$category_image_id = get_woocommerce_term_meta( $cat_value, 'thumbnail_id', true );
			$category_image = wp_get_attachment_image_src( $category_image_id, 'shopera-post-picture' );

			$output .= '<a href="' . esc_url(site_url()) . '/?product_cat=' . esc_attr($category_data->slug) . '" class="shopera-category-item" style="background-image:url(' . esc_url( $category_image['0'] ) . ');">';
				$output .= '<div class="category-inner-wrapper"><div class="shopera-category-info">';
					$output .= '<span class="shopera-category-name">' . $category_data->name . '</span>';
					$output .= '<span class="shopera-category-products">' . $category_data->count . ' ' . __('products', 'shopera') . '</span>';
				$output .= '</div></div>';
			$output .= '</a>';
		}
		$output .= '</div>';
	}

	

	return $output;
}
add_shortcode('woo_product_categories','shopera_category_module');

function shopera_wishlist_icon() {
	$curr_product = get_product(get_the_ID());
	$default_wishlists = is_user_logged_in() ? YITH_WCWL()->get_wishlists( array( 'is_default' => true ) ) : false;

	if( ! empty( $default_wishlists ) ){
		$default_wishlist = $default_wishlists[0]['ID'];
	}
	else{
		$default_wishlist = false;
	}

	$exists = YITH_WCWL()->is_product_in_wishlist( get_the_ID(), $default_wishlist );

	$exists_class = ' glyphicon-heart-empty';
	if ( $exists ) {
		$exists_class = ' added-to-wishlist glyphicon-heart';
	}

	echo '<span class="glyphicon add_to_wishlist'.$exists_class.'" data-product-id="'.get_the_ID().'" data-product-type="'.$curr_product->product_type.'"></span>';
}
if ( defined( 'YITH_WCWL' ) ) {
	add_action( 'woocommerce_before_shop_loop_item_title', 'shopera_wishlist_icon', 15 );
}
