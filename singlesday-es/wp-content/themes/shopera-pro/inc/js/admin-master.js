// Admin Javascript
jQuery( document ).ready( function( $ ) {

	// Choose layout
	$("#vh_layouts img").click(function() {
		$(this).parent().parent().find(".selected").removeClass("selected");
		$(this).addClass("selected");
	});


	/**
	 * ----------------------------------------------------------------------
	 * Theme installation wizard action:
	 * 2. Import demo content
	 */

	$("#do_demo-import").click(function(event) {
		event.preventDefault();

		// Do not run multiply times
		if ( ! $("#theme-setup-step-2").hasClass('step-completed') ) {
			// Do not run before step 1
			if ( $("#theme-setup-step-1").hasClass('step-completed') ) {
				// Do not run before step 2
				// if ( $("#theme-setup-step-2").hasClass('step-completed') ) {

					$("#theme-setup-step-2").addClass('loading');
					$.ajax({
						// type: "POST",
						cache: false,
						url: location.protocol + '//' + location.host + location.pathname + "?importcontent=alldemocontent",
						success: function(response){
							$("#theme-setup-step-2").removeClass('loading');

							// config process failed
							if ( $(response).find(".ajax-request-error").length > 0 ) {
								$(".vhman-message.quick-setup .step-demoimport .error").css('display', 'inline-block');
								$(".vhman-message.quick-setup").after('<div class="error-log-window" style="display:none"></div>');
								$(".error-log-window").append( $(response).find(".ajax-log") );

								$('.vhman-message.quick-setup .step-demoimport .error-log-window').css('display','inline-block');

							// config process succeeded
							} else {
								$(".vhman-message.quick-setup .step-demoimport").addClass("step-completed");

								// update option "THEMENAME . '_democontent_imported'"
								// with 'true' value
								$.ajax({
									cache: false,
									url: location.protocol + '//' + location.host + location.pathname + "?demoimport=completed",
								});
							}
						},

					}); //ajax

				// } else {
				// 	$( "#theme-setup-step-2" ).effect( "bounce", 1000);
				// }
			} else {
				$( "#theme-setup-step-1" ).effect( "bounce", 1000);
			}
		}
	});
});