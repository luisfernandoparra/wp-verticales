<?php
/**
 * Shopera 1.0 Theme Customizer support
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */

/**
 * Implement Theme Customizer additions and adjustments.
 *
 * @since Shopera 1.0
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function shopera_customize_register( $wp_customize ) {
	// Add custom description to Colors and Background sections.
	$wp_customize->get_section( 'colors' )->description           = __( 'Background may only be visible on wide screens.', 'shopera' );
	$wp_customize->get_section( 'background_image' )->description = __( 'Background may only be visible on wide screens.', 'shopera' );

	// Add postMessage support for site title and description.
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	// Rename the label to "Site Title Color" because this only affects the site title in this theme.
	$wp_customize->get_control( 'header_textcolor' )->label = __( 'Site Title Color', 'shopera' );

	// Rename the label to "Display Site Title & Tagline" in order to make this option extra clear.
	$wp_customize->get_control( 'display_header_text' )->label = __( 'Display Site Title &amp; Tagline', 'shopera' );

	// Add the featured content section in case it's not already there.
	$wp_customize->add_section( 'featured_content', array(
		'title'       => __( 'Featured Content', 'shopera' ),
		'description' => sprintf( __( 'To feature your posts use <a href="%1$s">featured</a> tag or provide your own tag below. If no posts match the tag, <a href="%2$s">sticky posts</a> will be displayed instead.', 'shopera' ), admin_url( '/edit.php?tag=featured' ), admin_url( '/edit.php?show_sticky=1' ) ),
		'priority'    => 130,
	) );

	$wp_customize->add_section( 'featured_content_side', array(
		'title'       => __( 'Featured Side Content', 'shopera' ),
		'description' => sprintf( __( 'To feature your posts use <a href="%1$s">featured-side</a> tag or provide your own tag below. If no posts match the tag, <a href="%2$s">sticky posts</a> will be displayed instead.', 'shopera' ), admin_url( '/edit.php?tag=featured-side' ), admin_url( '/edit.php?show_sticky=1' ) ),
		'priority'    => 140,
	) );

	// Add General setting panel and configure settings inside it
	$wp_customize->add_panel( 'shopera_general_panel', array(
		'priority'       => 150,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'General settings' , 'shopera'),
		'description'    => __( 'You can configure your general theme settings here' , 'shopera')
	) );

	// Website logo
	$wp_customize->add_section( 'shopera_general_logo', array(
		'priority'       => 10,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Website logo' , 'shopera'),
		'description'    => __( 'Please upload your logo, recommended logo size should be between 262x80' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_logo', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'shopera_logo', array(
		'label'    => __( 'Website logo', 'shopera' ),
		'section'  => 'shopera_general_logo',
		'settings' => 'shopera_logo',
	) ) );

	// woocommerce shop breadcrumb
	$wp_customize->add_section( 'shopera_general_shop_breadcrumb', array(
		'priority'       => 10,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'WooCommerce shop breadcrumb' , 'shopera'),
		'description'    => __( 'Please upload your image for WooCommerce shop breadcrumbs.' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_shop_breadcrumbs', array( 'default' => get_template_directory_uri().'/images/breadcrumbs.jpg', 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'shopera_shop_breadcrumbs', array(
		'label'    => __( 'WooCommerce shop breadcrumb', 'shopera' ),
		'section'  => 'shopera_general_shop_breadcrumb',
		'settings' => 'shopera_shop_breadcrumbs',
	) ) );

	// Copyright
	$wp_customize->add_section( 'shopera_general_copyright', array(
		'priority'       => 20,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Copyright' , 'shopera'),
		'description'    => __( 'Please provide short copyright text which will be shown in footer.' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_copyright', array( 'sanitize_callback' => 'esc_html', 'default' => __( 'Created by', 'shopera' ). ' ' . '<a class="created-cohhe paint-area paint-area--text" href="https://cohhe.com/">' . __( 'Cohhe', 'shopera' ) . '</a>' . ' ' . __( 'Proudly powered by', 'shopera' ) . ' ' . '<a class="created-by paint-area paint-area--text" href="' . esc_url( 'http://wordpress.org/' ) . '">' . __( 'WordPress', 'shopera' ) . '</a>' ) );

	$wp_customize->add_control(
		'shopera_copyright',
		array(
			'label'      => 'Copyright',
			'section'    => 'shopera_general_copyright',
			'type'       => 'text',
		)
	);

	// Scroll to top
	$wp_customize->add_section( 'shopera_general_scrolltotop', array(
		'priority'       => 30,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Scroll to top' , 'shopera'),
		'description'    => __( 'Do you want to enable "Scroll to Top" button?' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_scrolltotop', array( 'sanitize_callback' => 'shopera_sanitize_checkbox' ) );

	$wp_customize->add_control(
		'shopera_scrolltotop',
		array(
			'label'      => 'Scroll to top',
			'section'    => 'shopera_general_scrolltotop',
			'type'       => 'checkbox',
		)
	);

	// Favicon
	$wp_customize->add_section( 'shopera_general_favicon', array(
		'priority'       => 40,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Favicon' , 'shopera'),
		'description'    => __( 'Do you have favicon? You can upload it here.' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_favicon', array( 'sanitize_callback' => 'esc_url_raw' ) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'shopera_favicon', array(
		'label'    => __( 'Favicon', 'shopera' ),
		'section'  => 'shopera_general_favicon',
		'settings' => 'shopera_favicon',
	) ) );

	// Comment form URL field
	$wp_customize->add_section( 'shopera_general_commenturl', array(
		'priority'       => 50,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Comment form URL field' , 'shopera'),
		'description'    => __( 'Do you want to disable comment URL field?' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_commenturl', array( 'sanitize_callback' => 'shopera_sanitize_checkbox' ) );

	$wp_customize->add_control(
		'shopera_commenturl',
		array(
			'label'      => 'Comment form URL field',
			'section'    => 'shopera_general_commenturl',
			'type'       => 'checkbox',
		)
	);

	// Slider settings
	$wp_customize->add_section( 'shopera_general_slider', array(
		'priority'       => 51,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Slider status' , 'shopera'),
		'description'    => __( 'Enable or disable the front page slider.' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_main_slider', array( 'default' => '1', 'sanitize_callback' => 'shopera_sanitize_checkbox' ) );

	$wp_customize->add_control(
		'shopera_main_slider',
		array(
			'label'      => 'Slider status',
			'section'    => 'shopera_general_slider',
			'type'       => 'checkbox',
		)
	);

	// Slider style
	$wp_customize->add_section( 'shopera_general_slider_style', array(
		'priority'       => 51,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Slider style' , 'shopera'),
		'description'    => __( 'Choose the style for your slider.' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_main_slider_style', array( 'default' => 'default-style-slider', 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'shopera_main_slider_style',
		array(
			'label'      => 'Slider style',
			'section'    => 'shopera_general_slider_style',
			'type'       => 'select',
			'choices'    => array('default-style-slider' => 'Default style', 'three-column-slider' => 'Three column', 'two-column-slider' => 'Two column')
		)
	);

	// Post grid settings
	$wp_customize->add_section( 'shopera_general_grid', array(
		'priority'       => 52,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Post grid status' , 'shopera'),
		'description'    => __( 'Do you want to display posts in a grid?' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_post_grid', array( 'default' => '1', 'sanitize_callback' => 'shopera_sanitize_checkbox' ) );

	$wp_customize->add_control(
		'shopera_post_grid',
		array(
			'label'      => 'Post grid status',
			'section'    => 'shopera_general_grid',
			'type'       => 'checkbox',
		)
	);

	// Page layout
	$wp_customize->add_section( 'shopera_general_layout', array(
		'priority'       => 60,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Layout' , 'shopera'),
		'description'    => __( 'Choose a layout for your theme pages. Note that a widget has to be inside widget are, or the layout won\'t change.' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting(
		'shopera_layout',
		array(
			'default'           => 'full',
			'sanitize_callback' => 'sanitize_text_field'
		)
	);

	$wp_customize->add_control(
		'shopera_layout',
		array(
			'type' => 'radio',
			'label' => 'Layout',
			'section' => 'shopera_general_layout',
			'choices' => array(
				'left' => 'Left',
				'full' => 'Full',
				'right' => 'Right'
			)
		)
	);

	// Disable color customizer
	$wp_customize->add_section( 'shopera_general_colorcustomizer', array(
		'priority'       => 61,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Color customizer' , 'shopera'),
		'description'    => __( 'Do you want to display frontend color customizer for users?' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_color_customizer', array( 'default' => '1', 'sanitize_callback' => 'shopera_sanitize_checkbox' ) );

	$wp_customize->add_control(
		'shopera_color_customizer',
		array(
			'label'      => 'Color customizer status',
			'section'    => 'shopera_general_colorcustomizer',
			'type'       => 'checkbox',
		)
	);

	// Add Font setting panel and configure settings inside it
	$wp_customize->add_panel( 'shopera_font_panel', array(
		'priority'       => 160,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Font settings' , 'shopera'),
		'description'    => __( 'You can configure your themes font settings here, if there are characters in your language that are not supported by a particular font, disable it..' , 'shopera')
	) );

	// Font size
	$wp_customize->add_section( 'shopera_font_size', array(
		'priority'       => 20,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Logo font size' , 'shopera'),
		'description'    => __( 'You can change your websites main title size.' , 'shopera'),
		'panel'          => 'shopera_font_panel'
	) );

	$wp_customize->add_setting( 'shopera_logo_font_size', array( 'default' => '50', 'sanitize_callback' => 'absint' ) );

	$wp_customize->add_control(
		'shopera_logo_font_size',
		array(
			'label'      => 'Satisfy font',
			'section'    => 'shopera_font_size',
			'type'       => 'range',
			'input_attrs' => array(
				'min' => 10,
				'max' => 80,
				'step' => 2,
			)
		)
	);

	// Social links
	$wp_customize->add_section( new Shopera_Customized_Section( $wp_customize, 'shopera_social_links', array(
		'priority'       => 210,
		'capability'     => 'edit_theme_options'
		) )
	);

	$wp_customize->add_setting( 'shopera_fake_field', array( 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'shopera_fake_field',
		array(
			'label'      => '',
			'section'    => 'shopera_social_links',
			'type'       => 'text'
		)
	);

	$font_settings = shopera_get_font_settings();

	foreach ($font_settings as $font_key => $font_value) {
		$wp_customize->add_section( 'shopera_'.$font_key.'_fonts', array(
			'priority'       => 20,
			'capability'     => 'edit_theme_options',
			'title'          => $font_value['title'],
			'description'    => $font_value['description'],
			'panel'          => 'shopera_font_panel'
		) );

		$wp_customize->add_setting( 'shopera_'.$font_key.'_fontsize', array( 'default' => $font_value['size'], 'sanitize_callback' => 'absint' ) );
		$wp_customize->add_control(
			'shopera_'.$font_key.'_fontsize',
			array(
				'label'      => 'Font size',
				'section'    => 'shopera_'.$font_key.'_fonts',
				'type'       => 'select',
				'choices'    => shopera_get_select( 8, 100 )
			)
		);

		$wp_customize->add_setting( 'shopera_'.$font_key.'_lineheight', array( 'default' => $font_value['lineheight'], 'sanitize_callback' => 'absint' ) );
		$wp_customize->add_control(
			'shopera_'.$font_key.'_lineheight',
			array(
				'label'      => 'Lineheight',
				'section'    => 'shopera_'.$font_key.'_fonts',
				'type'       => 'select',
				'choices'    => shopera_get_select( 8, 150 )
			)
		);

		$wp_customize->add_setting( 'shopera_'.$font_key.'_fontfamily', array( 'default' => $font_value['fontfamily'], 'sanitize_callback' => 'sanitize_text_field' ) );
		$wp_customize->add_control(
			'shopera_'.$font_key.'_fontfamily',
			array(
				'label'      => 'Font family',
				'section'    => 'shopera_'.$font_key.'_fonts',
				'type'       => 'select',
				'choices'    => shopera_return_fonts()
			)
		);

		$wp_customize->add_setting( 'shopera_'.$font_key.'_fontweight', array( 'default' => $font_value['fontweight'], 'sanitize_callback' => 'sanitize_text_field' ) );
		$wp_customize->add_control(
			'shopera_'.$font_key.'_fontweight',
			array(
				'label'      => 'Font weight',
				'section'    => 'shopera_'.$font_key.'_fonts',
				'type'       => 'select',
				'choices'    => array(
					'100' => '100',
					'300' => '300',
					'400' => 'Normal',
					'700' => 'Bold',
					'400italic' => 'Italic',
					'700italic' => 'Bold Italic'
					)
			)
		);
	}

	// Reset
	$wp_customize->add_section( new Shopera_Customized_Reset_Section( $wp_customize, 'shopera_reset_fonts', array(
		'priority'       => 210,
		'capability'     => 'edit_theme_options',
		'panel'          => 'shopera_font_panel'
		) )
	);

	$wp_customize->add_setting( 'shopera_fake_field2', array( 'sanitize_callback' => 'sanitize_text_field' ) );
	$wp_customize->add_control(
		'shopera_fake_field2',
		array(
			'label'      => '',
			'section'    => 'shopera_reset_fonts',
			'type'       => 'text'
		)
	);

	// Custom CSS
	$wp_customize->add_section( 'shopera_general_css', array(
		'priority'       => 62,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Custom CSS' , 'shopera'),
		'description'    => __( 'Copy your custom CSS code here. Use plain CSS without any html tags.' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_custom_css', array( 'default' => '', 'sanitize_callback' => 'sanitize_text_field' ) );

	$wp_customize->add_control(
		'shopera_custom_css',
		array(
			'label'      => 'Custom CSS',
			'section'    => 'shopera_general_css',
			'type'       => 'textarea',
		)
	);

	// Custom JavaScript
	$wp_customize->add_section( 'shopera_general_js', array(
		'priority'       => 63,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Custom JavaScript' , 'shopera'),
		'description'    => __( 'Copy your custom JavaScript code here. You have to add any needed html tags yourself.' , 'shopera'),
		'panel'          => 'shopera_general_panel'
	) );

	$wp_customize->add_setting( 'shopera_custom_js', array( 'default' => '', 'sanitize_callback' => 'shopera_sanitize_textarea_field' ) );

	$wp_customize->add_control(
		'shopera_custom_js',
		array(
			'label'      => 'Custom JavaScript',
			'section'    => 'shopera_general_js',
			'type'       => 'textarea',
		)
	);
}
add_action( 'customize_register', 'shopera_customize_register' );

function shopera_get_select( $min, $max ) {
	$select = array();
	for ($i=$min; $i <= $max; $i++) { 
		$select[$i] = $i.'px';
	}
	return $select;
}

function shopera_return_fonts() {
	return array("Arial" => 'Arial', "Verdana" => 'Verdana', "Georgia" => 'Georgia', "Tahoma" => 'Tahoma', "Trebuchet+MS" => 'Trebuchet MS', "Calibri" => 'Calibri', "Geneva" => 'Geneva', "Abel" => 'Abel', "Abril+Fatface" => 'Abril Fatface', "Aclonica" => 'Aclonica', "Actor" => 'Actor', "Adamina" => 'Adamina', "Aguafina+Script" => 'Aguafina Script', "Aladin" => 'Aladin', "Aldrich" => 'Aldrich', "Alice" => 'Alice', "Alike+Angular" => 'Alike Angular', "Alike" => 'Alike', "Allan" => 'Allan', "Allerta+Stencil" => 'Allerta Stencil', "Allerta" => 'Allerta', "Amaranth" => 'Amaranth', "Amatic+SC" => 'Amatic SC', "Andada" => 'Andada', "Andika" => 'Andika', "Annie+Use+Your+Telescope" => 'Annie Use Your Telescope', "Anonymous+Pro" => 'Anonymous Pro', "Antic" => 'Antic', "Anton" => 'Anton', "Arapey" => 'Arapey', "Architects+Daughter" => 'Architects Daughter', "Arimo" => 'Arimo', "Artifika" => 'Artifika', "Arvo" => 'Arvo', "Asset" => 'Asset', "Astloch" => 'Astloch', "Atomic+Age" => 'Atomic Age', "Aubrey" => 'Aubrey', "Bangers" => 'Bangers', "Bentham" => 'Bentham', "Bevan" => 'Bevan', "Bigshot+One" => 'Bigshot One', "Bitter" => 'Bitter', "Black+Ops+One" => 'Black Ops One', "Bowlby+One+SC" => 'Bowlby One SC', "Bowlby+One" => 'Bowlby One', "Brawler" => 'Brawler', "Bubblegum+Sans" => 'Bubblegum Sans', "Buda" => 'Buda', "Butcherman+Caps" => 'Butcherman Caps', "Cabin+Condensed" => 'Cabin Condensed', "Cabin+Sketch" => 'Cabin Sketch', "Cabin" => 'Cabin', "Cagliostro" => 'Cagliostro', "Calligraffitti" => 'Calligraffitti', "Candal" => 'Candal', "Cantarell" => 'Cantarell', "Cardo" => 'Cardo', "Carme" => 'Carme', "Carter+One" => 'Carter One', "Caudex" => 'Caudex', "Cedarville+Cursive" => 'Cedarville Cursive', "Changa+One" => 'Changa One', "Cherry+Cream+Soda" => 'Cherry Cream Soda', "Chewy" => 'Chewy', "Chicle" => 'Chicle', "Chivo" => 'Chivo', "Coda+Caption" => 'Coda Caption', "Coda" => 'Coda', "Comfortaa" => 'Comfortaa', "Coming+Soon" => 'Coming Soon', "Contrail+One" => 'Contrail One', "Convergence" => 'Convergence', "Cookie" => 'Cookie', "Copse" => 'Copse', "Corben" => 'Corben', "Cousine" => 'Cousine', "Coustard" => 'Coustard', "Covered+By+Your+Grace" => 'Covered By Your Grace', "Crafty+Girls" => 'Crafty Girls', "Creepster+Caps" => 'Creepster Caps', "Crimson+Text" => 'Crimson Text', "Crushed" => 'Crushed', "Cuprum" => 'Cuprum', "Damion" => 'Damion', "Dancing+Script" => 'Dancing Script', "Dawning+of+a+New+Day" => 'Dawning of a New Day', "Days+One" => 'Days One', "Delius+Swash+Caps" => 'Delius Swash Caps', "Delius+Unicase" => 'Delius Unicase', "Delius" => 'Delius', "Devonshire" => 'Devonshire', "Didact+Gothic" => 'Didact Gothic', "Dorsa" => 'Dorsa', "Dr+Sugiyama" => 'Dr Sugiyama', "Droid+Sans+Mono" => 'Droid Sans Mono', "Droid+Sans" => 'Droid Sans', "Droid+Serif" => 'Droid Serif', "EB+Garamond" => 'EB Garamond', "Eater+Caps" => 'Eater Caps', "Expletus+Sans" => 'Expletus Sans', "Fanwood+Text" => 'Fanwood Text', "Federant" => 'Federant', "Federo" => 'Federo', "Fjord+One" => 'Fjord One', "Fondamento" => 'Fondamento', "Fontdiner+Swanky" => 'Fontdiner Swanky', "Forum" => 'Forum', "Francois+One" => 'Francois One', "Gentium+Basic" => 'Gentium Basic', "Gentium+Book+Basic" => 'Gentium Book Basic', "Geo" => 'Geo', "Geostar+Fill" => 'Geostar Fill', "Geostar" => 'Geostar', "Give+You+Glory" => 'Give You Glory', "Gloria+Hallelujah" => 'Gloria Hallelujah', "Goblin+One" => 'Goblin One', "Gochi+Hand" => 'Gochi Hand', "Goudy+Bookletter+1911" => 'Goudy Bookletter 1911', "Gravitas+One" => 'Gravitas One', "Gruppo" => 'Gruppo', "Hammersmith+One" => 'Hammersmith One', "Herr+Von+Muellerhoff" => 'Herr Von Muellerhoff', "Holtwood+One+SC" => 'Holtwood One SC', "Homemade+Apple" => 'Homemade Apple', "IM+Fell+DW+Pica+SC" => 'IM Fell DW Pica SC', "IM+Fell+DW+Pica" => 'IM Fell DW Pica', "IM+Fell+Double+Pica+SC" => 'IM Fell Double Pica SC', "IM+Fell+Double+Pica" => 'IM Fell Double Pica', "IM+Fell+English+SC" => 'IM Fell English SC', "IM+Fell+English" => 'IM Fell English', "IM+Fell+French+Canon+SC" => 'IM Fell French Canon SC', "IM+Fell+French+Canon" => 'IM Fell French Canon', "IM+Fell+Great+Primer+SC" => 'IM Fell Great Primer SC', "IM+Fell+Great+Primer" => 'IM Fell Great Primer', "Iceland" => 'Iceland', "Inconsolata" => 'Inconsolata', "Indie+Flower" => 'Indie Flower', "Irish+Grover" => 'Irish Grover', "Istok+Web" => 'Istok Web', "Jockey+One" => 'Jockey One', "Josefin+Sans" => 'Josefin Sans', "Josefin+Slab" => 'Josefin Slab', "Judson" => 'Judson', "Julee" => 'Julee', "Jura" => 'Jura', "Just+Another+Hand" => 'Just Another Hand', "Just+Me+Again+Down+Here" => 'Just Me Again Down Here', "Kameron" => 'Kameron', "Kelly+Slab" => 'Kelly Slab', "Kenia" => 'Kenia', "Knewave" => 'Knewave', "Kranky" => 'Kranky', "Kreon" => 'Kreon', "Kristi" => 'Kristi', "La+Belle+Aurore" => 'La Belle Aurore', "Lancelot" => 'Lancelot', "Lato" => 'Lato', "League+Script" => 'League Script', "Leckerli+One" => 'Leckerli One', "Lekton" => 'Lekton', "Lemon" => 'Lemon', "Limelight" => 'Limelight', "Linden+Hill" => 'Linden Hill', "Lobster+Two" => 'Lobster Two', "Lobster" => 'Lobster', "Lora" => 'Lora', "Love+Ya+Like+A+Sister" => 'Love Ya Like A Sister', "Loved+by+the+King" => 'Loved by the King', "Luckiest+Guy" => 'Luckiest Guy', "Maiden+Orange" => 'Maiden Orange', "Mako" => 'Mako', "Marck+Script" => 'Marck Script', "Marvel" => 'Marvel', "Mate+SC" => 'Mate SC', "Mate" => 'Mate', "Maven+Pro" => 'Maven Pro', "Meddon" => 'Meddon', "MedievalSharp" => 'MedievalSharp', "Megrim" => 'Megrim', "Merienda+One" => 'Merienda One', "Merriweather" => 'Merriweather', "Metrophobic" => 'Metrophobic', "Michroma" => 'Michroma', "Miltonian+Tattoo" => 'Miltonian Tattoo', "Miltonian" => 'Miltonian', "Miss+Fajardose" => 'Miss Fajardose', "Miss+Saint+Delafield" => 'Miss Saint Delafield', "Modern+Antiqua" => 'Modern Antiqua', "Molengo" => 'Molengo', "Monofett" => 'Monofett', "Monoton" => 'Monoton', "Monsieur+La+Doulaise" => 'Monsieur La Doulaise', "Montez" => 'Montez', "Mountains+of+Christmas" => 'Mountains of Christmas', "Mr+Bedford" => 'Mr Bedford', "Mr+Dafoe" => 'Mr Dafoe', "Mr+De+Haviland" => 'Mr De Haviland', "Mrs+Sheppards" => 'Mrs Sheppards', "Muli" => 'Muli', "Neucha" => 'Neucha', "Neuton" => 'Neuton', "News+Cycle" => 'News Cycle', "Niconne" => 'Niconne', "Nixie+One" => 'Nixie One', "Nobile" => 'Nobile', "Nosifer+Caps" => 'Nosifer Caps', "Nothing+You+Could+Do" => 'Nothing You Could Do', "Nova+Cut" => 'Nova Cut', "Nova+Flat" => 'Nova Flat', "Nova+Mono" => 'Nova Mono', "Nova+Oval" => 'Nova Oval', "Nova+Round" => 'Nova Round', "Nova+Script" => 'Nova Script', "Nova+Slim" => 'Nova Slim', "Nova+Square" => 'Nova Square', "Numans" => 'Numans', "Nunito" => 'Nunito', "Old+Standard+TT" => 'Old Standard TT', "Open+Sans+Condensed" => 'Open Sans Condensed', "Open+Sans" => 'Open Sans', "Orbitron" => 'Orbitron', "Oswald" => 'Oswald', "Over+the+Rainbow" => 'Over the Rainbow', "Ovo" => 'Ovo', "PT+Sans+Caption" => 'PT Sans Caption', "PT+Sans+Narrow" => 'PT Sans Narrow', "PT+Sans" => 'PT Sans', "PT+Serif+Caption" => 'PT Serif Caption', "PT+Serif" => 'PT Serif', "Pacifico" => 'Pacifico', "Passero+One" => 'Passero One', "Patrick+Hand" => 'Patrick Hand', "Paytone+One" => 'Paytone One', "Permanent+Marker" => 'Permanent Marker', "Petrona" => 'Petrona', "Philosopher" => 'Philosopher', "Piedra" => 'Piedra', "Pinyon+Script" => 'Pinyon Script', "Play" => 'Play', "Playfair+Display" => 'Playfair Display', "Podkova" => 'Podkova', "Poller+One" => 'Poller One', "Poly" => 'Poly', "Pompiere" => 'Pompiere', "Prata" => 'Prata', "Prociono" => 'Prociono', "Puritan" => 'Puritan', "Quattrocento+Sans" => 'Quattrocento Sans', "Quattrocento" => 'Quattrocento', "Questrial" => 'Questrial', "Quicksand" => 'Quicksand', "Radley" => 'Radley', "Raleway" => 'Raleway', "Rammetto+One" => 'Rammetto One', "Rancho" => 'Rancho', "Rationale" => 'Rationale', "Redressed" => 'Redressed', "Reenie+Beanie" => 'Reenie Beanie', "Ribeye+Marrow" => 'Ribeye Marrow', "Ribeye" => 'Ribeye', "Righteous" => 'Righteous', "Rochester" => 'Rochester', "Rock+Salt" => 'Rock Salt', "Rokkitt" => 'Rokkitt', "Rosario" => 'Rosario', "Ruslan+Display" => 'Ruslan Display', "Salsa" => 'Salsa', "Sancreek" => 'Sancreek', "Sansita+One" => 'Sansita One', "Satisfy" => 'Satisfy', "Schoolbell" => 'Schoolbell', "Shadows+Into+Light" => 'Shadows Into Light', "Shanti" => 'Shanti', "Short+Stack" => 'Short Stack', "Sigmar+One" => 'Sigmar One', "Signika+Negative" => 'Signika Negative', "Signika" => 'Signika', "Six+Caps" => 'Six Caps', "Slackey" => 'Slackey', "Smokum" => 'Smokum', "Smythe" => 'Smythe', "Sniglet" => 'Sniglet', "Snippet" => 'Snippet', "Sorts+Mill+Goudy" => 'Sorts Mill Goudy', "Special+Elite" => 'Special Elite', "Spinnaker" => 'Spinnaker', "Spirax" => 'Spirax', "Stardos+Stencil" => 'Stardos Stencil', "Sue+Ellen+Francisco" => 'Sue Ellen Francisco', "Sunshiney" => 'Sunshiney', "Supermercado+One" => 'Supermercado One', "Swanky+and+Moo+Moo" => 'Swanky and Moo Moo', "Syncopate" => 'Syncopate', "Tangerine" => 'Tangerine', "Tenor+Sans" => 'Tenor Sans', "Terminal+Dosis" => 'Terminal Dosis', "The+Girl+Next+Door" => 'The Girl Next Door', "Tienne" => 'Tienne', "Tinos" => 'Tinos', "Tulpen+One" => 'Tulpen One', "Ubuntu+Condensed" => 'Ubuntu Condensed', "Ubuntu+Mono" => 'Ubuntu Mono', "Ubuntu" => 'Ubuntu', "Ultra" => 'Ultra', "UnifrakturCook" => 'UnifrakturCook', "UnifrakturMaguntia" => 'UnifrakturMaguntia', "Unkempt" => 'Unkempt', "Unlock" => 'Unlock', "Unna" => 'Unna', "VT323" => 'VT323', "Varela+Round" => 'Varela Round', "Varela" => 'Varela', "Vast+Shadow" => 'Vast Shadow', "Vibur" => 'Vibur', "Vidaloka" => 'Vidaloka', "Volkhov" => 'Volkhov', "Vollkorn" => 'Vollkorn', "Voltaire" => 'Voltaire', "Waiting+for+the+Sunrise" => 'Waiting for the Sunrise', "Wallpoet" => 'Wallpoet', "Walter+Turncoat" => 'Walter Turncoat', "Wire+One" => 'Wire One', "Yanone+Kaffeesatz" => 'Yanone Kaffeesatz', "Yellowtail" => 'Yellowtail', "Yeseva+One" => 'Yeseva One', "Zeyada" => 'Zeyada', "Headland+One" => 'Headland One', "Fjalla+One" => 'Fjalla One', "Roboto" => 'Roboto', "Roboto+Condensed" => 'Roboto Condensed');
}

if ( class_exists( 'WP_Customize_Section' ) && !class_exists( 'Shopera_Customized_Section' ) ) {
	class Shopera_Customized_Section extends WP_Customize_Section {
		public function render() {
			$classes = 'accordion-section control-section control-section-' . $this->type;
			?>
			<li id="accordion-section-<?php echo esc_attr( $this->id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
				<style type="text/css">
					.cohhe-social-profiles {
						padding: 14px;
					}
					.cohhe-social-profiles li:last-child {
						display: none !important;
					}
					.cohhe-social-profiles li i {
						width: 20px;
						height: 20px;
						display: inline-block;
						background-size: cover !important;
						margin-right: 5px;
						float: left;
					}
					.cohhe-social-profiles li i.twitter {
						background: url(<?php echo get_template_directory_uri().'/images/icons/twitter.png'; ?>);
					}
					.cohhe-social-profiles li i.facebook {
						background: url(<?php echo get_template_directory_uri().'/images/icons/facebook.png'; ?>);
					}
					.cohhe-social-profiles li i.googleplus {
						background: url(<?php echo get_template_directory_uri().'/images/icons/googleplus.png'; ?>);
					}
					.cohhe-social-profiles li i.cohhe_logo {
						background: url(<?php echo get_template_directory_uri().'/images/icons/cohhe.png'; ?>);
					}
					.cohhe-social-profiles li a {
						height: 20px;
						line-height: 20px;
					}
					#customize-theme-controls>ul>#accordion-section-shopera_social_links {
						margin-top: 10px;
					}
					.cohhe-social-profiles li.documentation {
						text-align: right;
						margin-bottom: 60px;
					}
				</style>
				<ul class="cohhe-social-profiles">
					<li class="documentation"><a href="http://documentation.cohhe.com/shopera" class="button button-primary button-hero" target="_blank"><?php _e( 'Documentation', 'shopera' ); ?></a></li>
					<li class="social-twitter"><i class="twitter"></i><a href="https://twitter.com/Cohhe_Themes" target="_blank"><?php _e( 'Follow us on Twitter', 'shopera' ); ?></a></li>
					<li class="social-facebook"><i class="facebook"></i><a href="https://www.facebook.com/cohhethemes" target="_blank"><?php _e( 'Join us on Facebook', 'shopera' ); ?></a></li>
					<li class="social-googleplus"><i class="googleplus"></i><a href="https://plus.google.com/+Cohhe_Themes/posts" target="_blank"><?php _e( 'Join us on Google+', 'shopera' ); ?></a></li>
					<li class="social-cohhe"><i class="cohhe_logo"></i><a href="https://cohhe.com/" target="_blank"><?php _e( 'Cohhe.com', 'shopera' ); ?></a></li>
				</ul>
			</li>
			<?php
		}
	}
}

if ( class_exists( 'WP_Customize_Section' ) && !class_exists( 'Shopera_Customized_Reset_Section' ) ) {
	class Shopera_Customized_Reset_Section extends WP_Customize_Section {
		public function render() {
			$classes = 'accordion-section control-section control-section-' . $this->type;
			?>
			<li id="accordion-section-<?php echo esc_attr( $this->id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
				<style type="text/css">
					.cohhe-reset-fonts {
						padding: 14px;
						text-align: right;
					}
					.cohhe-reset-fonts li:last-child {
						display: none !important;
					}
				</style>
				<ul class="cohhe-reset-fonts">
					<li class="reset-fonts"><a href="javascript:void(0)" class="button button-primary" target="_blank"><?php _e( 'Reset fonts', 'shopera' ); ?></a></li>
				</ul>
			</li>
			<?php
		}
	}
}

function shopera_sanitize_checkbox( $input ) {
	// Boolean check 
	return ( ( isset( $input ) && true == $input ) ? true : false );
}

function shopera_sanitize_textarea_field( $input ) {
	return esc_js( $input );
}

/**
 * Sanitize the Featured Content layout value.
 *
 * @since Shopera 1.0
 *
 * @param string $layout Layout type.
 * @return string Filtered layout type (grid|slider).
 */
function shopera_sanitize_layout( $layout ) {
	if ( ! in_array( $layout, array( 'slider' ) ) ) {
		$layout = 'slider';
	}

	return $layout;
}

function delete_transient_side() {
	$side_posts = new Shopera_Featured_Content_Side();
	$side_posts->get_featured_posts();

	$slider_style = get_theme_mod('shopera_main_slider_style', 'default-style-slider');
	if ( $slider_style == 'default-style-slider' ) {
		$side_post_count = 5;
	} elseif ( $slider_style == 'three-column-slider' ) {
		$side_post_count = 3;
	} else {
		$side_post_count = 2;
	}
	var_dump($side_post_count);
}

/**
 * Bind JS handlers to make Theme Customizer preview reload changes asynchronously.
 *
 * @since Shopera 1.0
 */
function shopera_customize_preview_js() {
	wp_enqueue_script( 'shopera_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20131205', true );
}
add_action( 'customize_preview_init', 'shopera_customize_preview_js' );

/**
 * Add contextual help to the Themes and Post edit screens.
 *
 * @since Shopera 1.0
 *
 * @return void
 */
function shopera_contextual_help() {
	if ( 'admin_head-edit.php' === current_filter() && 'post' !== $GLOBALS['typenow'] ) {
		return;
	}

	get_current_screen()->add_help_tab( array(
		'id'      => 'shopera',
		'title'   => __( 'Shopera 1.0', 'shopera' ),
		'content' =>
			'<ul>' .
				'<li>' . sprintf( __( 'The home page features your choice of up to 6 posts prominently displayed in a grid or slider, controlled by the <a href="%1$s">featured</a> tag; you can change the tag and layout in <a href="%2$s">Appearance &rarr; Customize</a>. If no posts match the tag, <a href="%3$s">sticky posts</a> will be displayed instead.', 'shopera' ), admin_url( '/edit.php?tag=featured' ), admin_url( 'customize.php' ), admin_url( '/edit.php?show_sticky=1' ) ) . '</li>' .
				'<li>' . sprintf( __( 'Enhance your site design by using <a href="%s">Featured Images</a> for posts you&rsquo;d like to stand out (also known as post thumbnails). This allows you to associate an image with your post without inserting it. Shopera 1.0 uses featured images for posts and pages&mdash;above the title&mdash;and in the Featured Content area on the home page.', 'shopera' ), 'http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail' ) . '</li>' .
				'<li>' . sprintf( __( 'For an in-depth tutorial, and more tips and tricks, visit the <a href="%s">Shopera 1.0 documentation</a>.', 'shopera' ), 'http://codex.wordpress.org/Shopera' ) . '</li>' .
			'</ul>',
	) );
}
add_action( 'admin_head-themes.php', 'shopera_contextual_help' );
add_action( 'admin_head-edit.php',   'shopera_contextual_help' );
