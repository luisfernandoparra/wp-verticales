<?php
/**
 * The template for displaying featured posts on the front page
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('swiper-slide'); ?>>
	<div class="slide-inner">
		<?php
			// Output the featured image.
			if ( has_post_thumbnail() ) :
				if ( 'grid' == get_theme_mod( 'featured_content_layout' ) ) { ?>
					<a class="post-thumbnail" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
				<?php
				} else {
					$guid = get_the_guid($post->ID);
					$slide_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'shopera-huge-width' ); ?>
					<a href="<?php echo $guid; ?>" style="z-index: -1;" target="_blank">
					<div class="slide-image" style="background-image:url('<?php echo esc_url( $slide_image_url[0] ); ?>');"></div>
					</a>
					<?php
				}
			endif;
		?>
		<header class="entry-header">
			<?php
				$top_title = get_post_meta($post->ID, 'shopera_top_title', true);
				$button_text = get_post_meta($post->ID, 'shopera_button_text', true);
				$button_link = get_post_meta($post->ID, 'shopera_button_link', true);
			?>
			<span class="category-title-top"><?php echo $top_title; ?></span>
			<?php
				$external_url = get_post_meta(get_the_ID(), 'shopera_external_url', true);
				if ( !$external_url ) {
					$post_link = get_permalink();
				} else {
					$post_link = $external_url;
				}
			?>
			<?php the_title( '<h1 class="entry-title"><a href="' . esc_url( $post_link ) . '" rel="bookmark">','</a></h1>' ); ?>
			<div class="clearfix"></div>
		</header><!-- .entry-header -->
		<div class="slider-content">
			<?php the_excerpt(); ?>
		</div>
		<?php if ( $button_text && $button_link ) { ?>
			<a href="<?php echo $button_link; ?>" class="slider-link"><?php echo $button_text; ?></a>
		<?php } ?>
	</div>
</article><!-- #post-## -->
