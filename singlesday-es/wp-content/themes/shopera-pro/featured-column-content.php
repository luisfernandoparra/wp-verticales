<?php
/**
 * The template for displaying featured content
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */
?>

<div class="featured-slider">
	<div id="featured-content" class="featured-content">
	<?php
		$output = '';
		if ( shopera_get_featured_posts() ) {
			$featured_post = shopera_get_featured_posts();
			$featured_post = $featured_post['0'];

			$slide_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_post->ID ), 'shopera-huge-width' );

			$output .= '<div class="featured-content-inner" style="background-image:url(' . esc_url( $slide_image_url['0'] ) . ');">';
				$external_url = get_post_meta($featured_post->ID, 'shopera_external_url', true);
				$top_title = get_post_meta($featured_post->ID, 'shopera_top_title', true);
				$bottom_title = get_post_meta($featured_post->ID, 'shopera_text_below_title', true);
				$button_text = get_post_meta($featured_post->ID, 'shopera_button_text', true);
				$button_link = get_post_meta($featured_post->ID, 'shopera_button_link', true);
				if ( !$external_url ) {
					$post_link = get_permalink( $featured_post->ID );
				} else {
					$post_link = $external_url;
				}
				$output .= '<div class="featured-content-wrapper">';
				if ( $top_title ) {
					$output .= '<span class="category-title-top">' . $top_title . '</span>';
				}
				$output .= '<h1 class="entry-title"><a href="' . esc_url( $post_link ) . '" rel="bookmark">' . get_the_title( $featured_post->ID ) . '</a></h1>';
				if ( $bottom_title ) {
					$output .= '<span class="category-title-bottom">' . $bottom_title . '</span>';
				}
				if ( $button_text && $button_link ) {
					$output .= '<div class="clearfix"></div><span class="slider-title-line"></span><a href="' . $button_link . '" class="slider-link">' . $button_text . '</a>';
				}
				$output .= '</div>';
			$output .= '</div>';
		}
		echo $output;
	?>
	</div><!-- #featured-content .featured-content -->
	<div class="featured-content-side">
		<?php
			$output = '';
			if ( shopera_get_featured_side_posts() ) {
				$featured_side_posts = shopera_get_featured_side_posts();
				$loops = 1;
				$slider_style = get_theme_mod( 'shopera_main_slider_style', 'default-style-slider' );

				$output .= '<div class="featured-side-content-row">';

				if ( is_page_template('template-two.php') ) {
					$slider_style = 'two-column-slider';
				} else if ( is_page_template('template-three.php') ) {
					$slider_style = 'three-column-slider';
				}
				foreach ($featured_side_posts as $side_post_value) {

					if ( $slider_style == 'three-column-slider' && $loops == 2 ) {
						$output .= '<div class="featured-side-content-row">';
					} elseif ( $loops > 3 ) {
						break;
					}
					$external_url = get_post_meta($side_post_value->ID, 'shopera_external_url', true);
					if ( !$external_url ) {
						$post_link = get_permalink( $side_post_value->ID );
					} else {
						$post_link = $external_url;
					}
					$slide_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $side_post_value->ID ), 'shopera-huge-width' );
					$output .= '<div class="featured-side-content-item" style="background-image:url(' . esc_url( $slide_image_url['0'] ) . ');">';
						$output .= '<h1 class="entry-title"><a href="' . esc_url( $post_link ) . '" rel="bookmark">' . get_the_title( $side_post_value->ID ) . '</a></h1>';
					$output .= '</div>';

					if ( ( $slider_style == 'three-column-slider' && $loops == 1 || $loops == 3 ) || ( $slider_style == 'two-column-slider' && $loops == 1 ) ) {
						$output .= '</div>';
					}
					if ( $slider_style == 'two-column-slider' && $loops == 2 ) {
						break;
					}
					$loops++;
				}
			}
			echo $output;
		?>
	</div>
</div><!-- .featured-slider -->