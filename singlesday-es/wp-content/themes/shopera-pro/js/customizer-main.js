/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2015, Codrops
 * http://www.codrops.com
 */
(function() {

	var docElem = window.document.documentElement,
		// transition end event name
		transEndEventNames = { 'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd', 'msTransition': 'MSTransitionEnd', 'transition': 'transitionend' },
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		element_text_classes = '.product-name,.product-name a,.product-price,.product-quantity,.product-subtotal,.cart_totals h2,.cart-subtotal,.shipping,.order-total,.woocommerce-info, .woocommerce-info .showcoupon, .woocommerce-checkout h3, .product-total, .shop_table .cart_item, .payment_methods.methods .payment_method_bacs, .payment_methods.methods .payment_method_cheque, .payment_methods.methods .payment_method_paypal, .payment_methods.methods p, #reviews h2, .comment-reply-title, .stars a, .widget-title, .widget_archive a, #wp-calendar thead, #wp-calendar tbody, #wp-calendar caption, .widget li a, .widget_newsletterwidget, .recentcomments, .tagcloud a, .textwidget, .textwidget p, .related.products h2, .woo-breadcrumb-inner a, .comment-text .star-rating > span, .comment-text .meta strong, .comment-text .meta time, #tab-description h2, #tab-description p, .shop_attributes th, .shop_attributes p, #tab-additional_information h2';

	function scrollX() { return window.pageXOffset || docElem.scrollLeft; }
	function scrollY() { return window.pageYOffset || docElem.scrollTop; }
	function getOffset(el) {
		var offset = el.getBoundingClientRect();
		return { top : offset.top + scrollY(), left : offset.left + scrollX() };
	}

	function dragMoveListener(event) {
		var target = event.target,
			// keep the dragged position in the data-x/data-y attributes
			x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
			y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

		// translate the element
		target.style.transform = target.style.webkitTransform = 'translate(' + x + 'px, ' + y + 'px)';
		target.style.zIndex = 10000;

		// update the posiion attributes
		target.setAttribute('data-x', x);
		target.setAttribute('data-y', y);
	}

	function revertDraggable(el) {
		el.style.transform = el.style.webkitTransform = 'none';
		el.style.zIndex = 1;
		el.setAttribute('data-x', 0);
		el.setAttribute('data-y', 0);
	}

	function init() {
		// target elements with the "drag-element" class
		interact('.drag-element').draggable({
			// enable inertial throwing
			inertia: true,
			// call this function on every dragmove event
			onmove: dragMoveListener,
			onend: function (event) {
				if(!classie.has(event.target, 'drag-element--dropped') && !classie.has(event.target, 'drag-element--dropped-text')) {
					revertDraggable(event.target);
				}
			}
		});

		// enable draggables to be dropped into this
		interact('.paint-area, .product-name a, .product-name, .product-price, .product-quantity, .product-subtotal, .cart_totals h2, .cart-subtotal, .shipping, .order-total, .woocommerce-info, .woocommerce-info .showcoupon, .woocommerce-checkout h3, .product-total, .shop_table .cart_item, .payment_methods.methods .payment_method_bacs, .payment_methods.methods .payment_method_cheque, .payment_methods.methods .payment_method_paypal, .payment_methods.methods p, .woocommerce-checkout-payment, #reviews h2, .comment-reply-title, .stars a, body, .widget-title, .widget_archive a, #wp-calendar thead, #wp-calendar tbody, #wp-calendar caption, .widget li a, .widget_newsletterwidget, .recentcomments, .tagcloud a, .textwidget, .textwidget p, .related.products h2, .woo-breadcrumb-inner a, .comment-text .star-rating > span, .comment-text .meta strong, .comment-text .meta time, #tab-description h2, #tab-description p, .shop_attributes th, .shop_attributes p, #tab-additional_information h2').dropzone({
			// only accept elements matching this CSS selector
			accept: '.drag-element',
			// Require a 60% element overlap for a drop to be possible
			overlap: 0.60,
			ondragenter: function (event) {
				classie.add(event.target, 'paint-area--highlight');
			},
			ondragleave: function (event) {
				classie.remove(event.target, 'paint-area--highlight');
			},
			ondrop: function (event) {
				var type = 'area';
				if(classie.has(event.target, 'paint-area--text') || jQuery(event.target).is(element_text_classes)) {
					type = 'text';
				}

				var draggableElement = event.relatedTarget;

				// call ajax to update choice
				var customizerElementID = jQuery(event.target).attr('id');
				var customizerElementClass = jQuery(event.target).removeClass('paint-area--highlight paint--active active').attr('class');
				var customizerParentElementID = jQuery(event.target).parent().attr('id');
				var customizerParentElementClass = '.'+jQuery(event.target).parent().removeClass('paint-area--highlight paint--active active').attr('class');
				var elementType = type;
				var customizerColor = draggableElement.getAttribute('data-color');
				var elementTag = jQuery(event.target).prop('tagName').toLowerCase();
				var current_element = jQuery(event.target);
				var valid_path = '';

				if (( typeof customizerElementID === 'undefined' || customizerElementID == 'undefined' || customizerElementID == '' )
					&& ( typeof customizerElementClass === 'undefined' || customizerElementClass == 'undefined' || customizerElementClass == '' ) ) {
					// no class and id
					valid_path = elementTag;
					var has_attribute = false;
					var loop_element = '';
					valid_path = checkParentElements(current_element, valid_path);
				} else if ( typeof customizerElementID === 'undefined' || customizerElementID == 'undefined' || customizerElementID == '' ) {
					// has class
					valid_path = '.'+jQuery(event.target).attr('class').split(' ').join('.');
					valid_path = checkParentElements(jQuery(event.target), valid_path);
				} else {
					// has id
					valid_path = '#'+jQuery(event.target).attr('id');
					valid_path = checkParentElements(jQuery(event.target), valid_path);
				}

				jQuery('.customizer').addClass('disabled');
				console.log(valid_path.replace('. ',' '));

				classie.add(draggableElement, type === 'text' ? 'drag-element--dropped-text' : 'drag-element--dropped');

				var onEndTransCallbackFn = function(ev) {
					this.removeEventListener( transEndEventName, onEndTransCallbackFn );
					if( type === 'area' ) {
						paintArea(event.dragEvent, event.target, draggableElement.getAttribute('data-color'), valid_path.replace('. ',' '));
					}
					setTimeout(function() {
						revertDraggable(draggableElement);
						classie.remove(draggableElement, type === 'text' ? 'drag-element--dropped-text' : 'drag-element--dropped');
					}, type === 'text' ? 0 : 250);
				};
				if( type === 'text' ) {
					paintArea(event.dragEvent, event.target, draggableElement.getAttribute('data-color'), valid_path.replace('. ',' '));
				}
				draggableElement.querySelector('.drop').addEventListener(transEndEventName, onEndTransCallbackFn);

				

				jQuery.ajax({
					type: 'POST',
					url: my_ajax.ajaxurl,
					data: {"action": "update_shopera_customizer", path: '#page '+valid_path.replace('. ',' '), element: customizerElementID, parent: customizerParentElementID, type: elementType, color: customizerColor, tag: elementTag },
					success: function(response) {
						// jQuery('#shopera-style-inline-css').html(response);
						jQuery('.customizer').removeClass('disabled');

						// if ( elementType == 'text' ) {
						// 	current_element.css('color', '');
						// } else if ( elementType == 'area' ) {
						// 	current_element.css('background-color', '');
						// }

						return false;
					}
				});
			},
			ondropdeactivate: function (event) {
				// remove active dropzone feedback
				classie.remove(event.target, 'paint-area--highlight');
			}
		});

		// reset colors
		document.querySelector('button.reset-button').addEventListener('click', resetColors);
	}

	function checkParentElements(element, path) {
		var loop_element = '';
		var valid_path = path;
		loop_element = element.parent();
		while ( true ) {
			if (( typeof loop_element.attr('id') === 'undefined' || loop_element.attr('id') == 'undefined' || loop_element.attr('id') == '' )
				&& ( typeof loop_element.attr('class') === 'undefined' || loop_element.attr('class') == 'undefined' || loop_element.attr('class') == '' ) ) {
				// only tag, set tag at path and move forward
				valid_path = loop_element.prop('tagName').toLowerCase()+' '+valid_path;
				loop_element = loop_element.parent();
				continue;
			} else if ( typeof loop_element.attr('id') == 'undefined' ) {
				// has class
				valid_path = '.'+loop_element.attr('class').split(' ').join('.')+' '+valid_path;
				break;
			} else {
				// has id
				if ( loop_element.attr('id').indexOf("menu-item-") >= 0 ) {
					valid_path = "[id^='menu-item-']"+' '+valid_path;
				} else {
					customizerParentElementID = '#'+jQuery(event.target).parent().attr('id');
					valid_path = '#'+loop_element.attr('id')+' '+valid_path;
				}
				break;
			}
		}

		return valid_path;
	}

	function paintArea(ev, el, color, eclass) {
		var type = 'area';
		if(classie.has(el, 'paint-area--text') || jQuery(el).is(element_text_classes)) {
			type = 'text';
		}

		if( type === 'area' ) {
			// create SVG element
			var dummy = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
			dummy.setAttributeNS(null, 'version', '1.1');
			dummy.setAttributeNS(null, 'width', '100%');
			dummy.setAttributeNS(null, 'height', '100%');
			dummy.setAttributeNS(null, 'class', 'paint');

			var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
			g.setAttributeNS(null, 'transform', 'translate(' + Number(ev.pageX - getOffset(el).left) + ', ' + Number(ev.pageY - getOffset(el).top) + ')');

			var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
			circle.setAttributeNS(null, 'cx', 0);
			circle.setAttributeNS(null, 'cy', 0);
			circle.setAttributeNS(null, 'r', Math.sqrt(Math.pow(el.offsetWidth,2) + Math.pow(el.offsetHeight,2)));
			circle.setAttributeNS(null, 'fill', color);

			dummy.appendChild(g);
			g.appendChild(circle);
			el.appendChild(dummy);
		}

		setTimeout(function() {
			classie.add(el, 'paint--active');

			if( type === 'text' ) {
				el.style.color = color;
				jQuery(eclass).css('color', color);
				var onEndTransCallbackFn = function(ev) {
					if( ev.target != this ) return;
					this.removeEventListener( transEndEventName, onEndTransCallbackFn );
					classie.remove(el, 'paint--active');
				};

				el.addEventListener(transEndEventName, onEndTransCallbackFn);
			}
			else {
				var onEndTransCallbackFn = function(ev) {
					if( ev.target != this || ev.propertyName === 'fill-opacity' ) return;
					this.removeEventListener(transEndEventName, onEndTransCallbackFn);
					// set the color
					el.style.backgroundColor = color;
					jQuery(eclass).css('background-color', color);
					// remove SVG element
					el.removeChild(dummy);

					setTimeout(function() { classie.remove(el, 'paint--active'); }, 25);
				};

				circle.addEventListener(transEndEventName, onEndTransCallbackFn);
			}
		}, 25);
	}

	function resetColors() {
		if ( confirm('All your customizations are going to be deleted permanently') ) {
			jQuery.ajax({
				type: 'POST',
				url: my_ajax.ajaxurl,
				data: {"action": "delete_shopera_customizer" },
				success: function(response) {
					location.reload();
					return false;
				}
			});
		};
	}

	init();

})();