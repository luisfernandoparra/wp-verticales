( function( $ ) {
	jQuery(document).on('change', '#customize-theme-controls select[data-customize-setting-link*="_fontfamily"]', function() {
		var fontfamilies = [];
		jQuery('#customize-theme-controls select[data-customize-setting-link*="_fontfamily"]').each(function() {
			fontfamilies.push(jQuery(this).find(":selected").val());
		});

		jQuery.ajax({
			type: 'POST',
			url: my_ajax.ajaxurl,
			data: {"action": "reload_gfonts_file", font_family: JSON.stringify(fontfamilies) },
			success: function(response) {
				// console.log(response);

				return false;
			}
		});
	});

	jQuery(document).on('click', '.reset-fonts a', function() {
		jQuery(this).attr('disabled', true);
		jQuery.ajax({
			type: 'POST',
			url: my_ajax.ajaxurl,
			data: {"action": "reset_customizer_fonts" },
			success: function(response) {
				jQuery('.reset-fonts a').attr('disabled', false);
				location.reload();

				return false;
			}
		});
	});
} )( jQuery );