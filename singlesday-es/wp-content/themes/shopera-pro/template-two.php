<?php
/**
 * Template Name: Two column slider
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */

get_header();

global $shopera_site_width;
?>

<div id="main-content" class="main-content row">
	<?php if ( SHOPERA_LAYOUT == 'sidebar-left' ) {
		generated_dynamic_sidebar();
	} ?>
	<div id="primary" class="content-area <?php echo esc_attr( $shopera_site_width ); ?>">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	<?php if ( SHOPERA_LAYOUT == 'sidebar-right' ) {
		generated_dynamic_sidebar();
	} ?>
	</div><!-- #main-content -->

<?php
get_footer();
