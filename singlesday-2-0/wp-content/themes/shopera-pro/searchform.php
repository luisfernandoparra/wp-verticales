<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" class="search-field" placeholder="<?php echo esc_attr_x(  __( 'Buscar...', 'shopera' ), 'placeholder', 'shopera' ); ?>" value="<?php echo get_search_query( __( 'Buscar...', 'shopera' ) ); ?>" name="s" title="<?php echo esc_attr_x( 'Buscar', 'label', 'shopera' ); ?>" />
	<?php
	if ( !defined('ICL_LANGUAGE_CODE') ) {
		define('ICL_LANGUAGE_CODE', '');
	}
	?>
	<input type="hidden" name="lang" value="<?php echo esc_attr(ICL_LANGUAGE_CODE); ?>"/>
	<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Buscar', 'submit button', 'shopera' ); ?>" />
</form>
<span class="search-form-submit paint-area paint-area--text glyphicon glyphicon-search"></span>