<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Page thumbnail and title.
		shopera_post_thumbnail();

		if ( !is_front_page() && !is_page_template('template-two.php') && !is_page_template('template-three.php') ) {
			the_title( '<header class="entry-header"><h1 class="entry-title paint-area paint-area--text">', '</h1></header><!-- .entry-header -->' );
		}
		
	?>

	<div class="entry-content paint-area paint-area--text">
		<?php
			the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'shopera' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
