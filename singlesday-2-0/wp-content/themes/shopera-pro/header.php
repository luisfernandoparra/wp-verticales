<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */
//Permitimos cualquier lugar 
header('Access-Control-Allow-Origin: *'); 
//Le decimos los métodos (POST, GET, PUT, DELETE...) 
header('Access-Control-Allow-Methods: GET, POST, HEAD');    
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<?php
global $shopera_site_width, $shopera_post_id;

$form_class    = '';
$class         = '';
$shopera_site_width    = 'col-sm-12 col-md-12 col-lg-12';

if ( isset($GLOBALS['wp_query']->queried_object->ID) ) {
	$shopera_post_id = $GLOBALS['wp_query']->queried_object->ID;
} elseif ( defined('WC_VERSION') && is_shop() ) {
	$shopera_post_id = woocommerce_get_page_id( 'shop' );
} else {
	$shopera_post_id = get_the_ID();
}

$layout_type   = get_post_meta($shopera_post_id, 'layouts', true);

if ( !isset($search_string) ) {
	$search_string = '';
}

if ( is_archive() || is_search() || is_404() ) {
	$layout_type = 'full';
} elseif ( empty($layout_type) ) {
	$layout_type = get_theme_mod('shopera_layout', 'full');
}

switch ($layout_type) {
	case 'right':
		define('SHOPERA_LAYOUT', 'sidebar-right');
		break;
	case 'full':
		define('SHOPERA_LAYOUT', 'sidebar-no');
		break;
	case 'left':
		define('SHOPERA_LAYOUT', 'sidebar-left');
		break;
}

if ( ( SHOPERA_LAYOUT == 'sidebar-left' && is_active_sidebar( 'sidebar-1' ) ) || ( SHOPERA_LAYOUT == 'sidebar-right' && is_active_sidebar( 'sidebar-2' ) ) ) {
	$shopera_site_width = 'col-sm-8 col-md-8 col-lg-8';
}

?>
<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php if ( !is_customize_preview() && ( get_theme_mod('shopera_color_customizer', true) || current_user_can( 'manage_options' ) ) ) { ?>
		<div class="customizer">
			<ul class="color-tool">
				<li class="color-1"><div class="drag-element" data-color="#333"><div class="drop color-1"></div><i class="drop-helper-1 color-1"></i><i class="drop-helper-2 color-1"></i><i class="drop-helper-3 color-1"></i><i class="drop-helper-4 color-1"></i></div></li>
				<li class="color-2"><div class="drag-element" data-color="#f0f0f0"><div class="drop color-2"></div><i class="drop-helper-1 color-2"></i><i class="drop-helper-2 color-2"></i><i class="drop-helper-3 color-2"></i><i class="drop-helper-4 color-2"></i></div></li>
				<li class="color-3"><div class="drag-element" data-color="#c0c3d5"><div class="drop color-3"></div><i class="drop-helper-1 color-3"></i><i class="drop-helper-2 color-3"></i><i class="drop-helper-3 color-3"></i><i class="drop-helper-4 color-3"></i></div></li>
				<li class="color-4"><div class="drag-element" data-color="#5FA1E0"><div class="drop color-4"></div><i class="drop-helper-1 color-4"></i><i class="drop-helper-2 color-4"></i><i class="drop-helper-3 color-4"></i><i class="drop-helper-4 color-4"></i></div></li>
				<li class="color-5"><div class="drag-element" data-color="#C1D5C0"><div class="drop color-5"></div><i class="drop-helper-1 color-5"></i><i class="drop-helper-2 color-5"></i><i class="drop-helper-3 color-5"></i><i class="drop-helper-4 color-5"></i></div></li>
				<li class="color-6"><div class="drag-element" data-color="#47AE73"><div class="drop color-6"></div><i class="drop-helper-1 color-6"></i><i class="drop-helper-2 color-6"></i><i class="drop-helper-3 color-6"></i><i class="drop-helper-4 color-6"></i></div></li>
				<li class="color-7"><div class="drag-element" data-color="#EAE7C4"><div class="drop color-7"></div><i class="drop-helper-1 color-7"></i><i class="drop-helper-2 color-7"></i><i class="drop-helper-3 color-7"></i><i class="drop-helper-4 color-7"></i></div></li>
				<li class="color-8"><div class="drag-element" data-color="#FB6964"><div class="drop color-8"></div><i class="drop-helper-1 color-8"></i><i class="drop-helper-2 color-8"></i><i class="drop-helper-3 color-8"></i><i class="drop-helper-4 color-8"></i></div></li>
				<li class="color-9"><div class="drag-element" data-color="#FFFFFF"><div class="drop color-9"></div><i class="drop-helper-1 color-9"></i><i class="drop-helper-2 color-9"></i><i class="drop-helper-3 color-9"></i><i class="drop-helper-4 color-9"></i></div></li>
				<li><button class="reset-button" title="Reser colors">Reset colors</button></li>
			</ul>
			<span class="toggle-colorcustomizer glyphicon glyphicon-cog"></span>
		</div>
		<div class="info-wrap">
			<div class="info">
				<h3><?php _e('Interactive Coloring', 'shopera'); ?></h3>
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/drag.svg" alt="drag icon"/><?php _e('Drag any color from the left toolbar to an area or text in the page. A blue outline will indicate a droppable element.', 'shopera'); ?></p>
				<p><img src="<?php echo get_template_directory_uri(); ?>/images/time.svg" alt="drag icon"/><?php _e('On mobile, wait a tiny bit until you drag the color drop.', 'shopera'); ?></p>
				<button class="info-close"><?php _e('Got it!', 'shopera'); ?></button>
			</div>
		</div>
	<?php } ?>
	<header id="masthead" class="site-header" role="banner">
		<div class="header-content container paint-area">
			<div class="header-main row">
				<div class="site-title col-xs-3 col-sm-3 col-md-3">
					<?php
					$logo = get_theme_mod( 'shopera_logo' );
					if ( $logo ) { ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url( $logo ); ?>"></a>
					<?php
					} else { ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="site-title title-effect paint-area paint-area--text"><span data-letters="<?php bloginfo( 'name' ); ?>"><?php bloginfo( 'name' ); ?></span></a>
					<?php
					}
					?>
				</div>
				<?php if ( class_exists( 'WooCommerce' ) ) { ?>
					<div class="cart-contents"></div>
				<?php } ?>
				<div class="header_search">
					<?php
						if ( !function_exists('AWS') ) {
							get_search_form();
						} else {
							echo do_shortcode('[aws_search_form]');
							echo '<span class="search-form-submit aws glyphicon glyphicon-search"></span>';
						}
					?>
				</div>
				<button type="button" class="navbar-toggle visible-xs visible-sm" data-toggle="collapse" data-target=".site-navigation">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<nav id="primary-navigation" class="col-xs-12 col-sm-10 col-md-10 site-navigation primary-navigation navbar-collapse collapse" role="navigation">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'menu_class'     => 'nav-menu',
								'depth'          => 3,
								'fallback_cb'    => 'shopera_fallback_page_menu',
								'walker'         => new Shopera_Header_Menu_Walker
							)
						);
					?>
					<div class="mobile header_search"><?php get_search_form(); ?></div>
				</nav>
			</div>
		</div>
		<div class="clearfix"></div>
	</header><!-- #masthead -->
	<?php
		$slider_state = get_theme_mod( 'shopera_main_slider', true );
		$slider_style = get_theme_mod( 'shopera_main_slider_style', 'default-style-slider' );
		if ( is_front_page() && shopera_has_featured_posts() && $slider_state && $slider_style == 'default-style-slider' ) {
			// Include the featured content template.
			get_template_part( 'featured-content' );
		} elseif ( is_front_page() && shopera_get_featured_posts() && $slider_state && $slider_style != 'default-style-slider' ) {
			get_template_part( 'featured-column-content' );
		} elseif ( is_page_template('template-three.php') ) {
			get_template_part( 'featured-column-content' );
		} elseif ( is_page_template('template-two.php') ) {
			get_template_part( 'featured-content' );
		}
	?>
	<div id="main" class="site-main container <?php echo SHOPERA_LAYOUT; ?>">
