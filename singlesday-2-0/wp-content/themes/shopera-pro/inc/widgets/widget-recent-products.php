<?php
// Creating the widget 
class wpb_widget_followers extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'shopera-recent-products', 

			// Widget name will appear in UI
			__('Shopera - Recent products', 'vh'), 

			// Widget description
			array( 'description' => __( 'Just a simple widget that displays social icons with follower count.', 'vh' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		global $post, $wpdb, $wp_query;
		$maintitle      = $instance['maintitle'];
		$per_page = $instance['per_page'];

		// before and after widget arguments are defined by themes

		echo $args['before_widget'];
		
		if ( !empty($maintitle) ) {
			echo '<h3 class="widget-title">' . esc_html( $maintitle ) . '</h3>';
		}

		echo do_shortcode('[recent_products per_page="' . $per_page . '" columns="1"]');

		echo $args['after_widget'];
	}
		
	// Widget Backend 
	public function form( $instance ) {

		$maintitle = isset($instance[ 'maintitle' ]) ? $instance[ 'maintitle' ] : '';
		$per_page = isset($instance[ 'per_page' ]) ? $instance[ 'per_page' ] : '';

		// Widget admin form
		?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'maintitle' ) ); ?>"><?php _e( 'Widget title:', 'vh' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'maintitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'maintitle' ) ); ?>" type="text" value="<?php echo esc_attr( $maintitle ); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'per_page' ) ); ?>"><?php _e( 'Per page:', 'vh' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'per_page' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'per_page' ) ); ?>" type="text" value="<?php echo esc_attr( $per_page ); ?>" />
		</p>

		<?php 
	}
	
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['maintitle'] = ( ! empty( $new_instance['maintitle'] ) ) ? strip_tags( $new_instance['maintitle'] ) : '';
		$instance['per_page'] = ( ! empty( $new_instance['per_page'] ) ) ? strip_tags( $new_instance['per_page'] ) : '';

		return $instance;
	}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget_followers() {
	register_widget( 'wpb_widget_followers' );
}
add_action( 'widgets_init', 'wpb_load_widget_followers' );
?>