<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */
?>

		</div><!-- #main -->

		<div class="site-footer-wrapper">
			<div class="site-footer-container container paint-area">
				<footer id="colophon" class="site-footer row" role="contentinfo">
					<?php
						get_sidebar( 'footer' );
					?>
				</footer><!-- #colophon -->
				<div class="clearfix"></div>
			</div>
			<div class="site-info paint-area col-sm-12 col-md-12 col-lg-12">
				<div class="site-info-content container">
					<div class="copyright paint-area paint-area--text">
						<?php
							$copyright = get_theme_mod( 'shopera_copyright', __( 'Created by', 'shopera' ). ' ' . '<a class="created-cohhe paint-area paint-area--text" href="https://cohhe.com/">' . __( 'Cohhe', 'shopera' ) . '</a>' . ' ' , __( 'Proudly powered by', 'shopera' ) . ' ' . '<a class="created-by paint-area paint-area--text" href="' . esc_url( 'http://wordpress.org/' ) . '">' . __( 'WordPress', 'shopera' ) );
							if ( $copyright != '' ) {
								echo html_entity_decode( $copyright );
							}
						?>
					</div>
					<?php 
						$show_scroll_to_top = get_theme_mod( 'shopera_scrolltotop' );

						if ( $show_scroll_to_top ) { ?>
							<a class="scroll-to-top" href="#"><?php _e( 'Up', 'shopera' ); ?></a>
						<?php
						}
					?>
				</div>
			</div><!-- .site-info -->
		</div>
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>