<?php if( is_active_sidebar( 'sidebar-primary' ) ): ?>
<aside id="secondary">
	<div id="sidebar" class="clearfix <?php echo esc_attr( themedsgn_get_theme_mod( 'sidebar_style', 'sidebar-1' ) ); ?>">
		<?php dynamic_sidebar( 'sidebar-primary' ); ?>
	</div>
</aside>
<?php endif; ?>