<?php
if ( post_password_required() ) {
	return;
}
?>
<div id="comments" class="comments-section">
	<?php if ( have_comments() ) : ?>
		<h3 class="comments-title">
			<?php
				printf(
					esc_html( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments', 'elvira' ) ),
					number_format_i18n( get_comments_number() )
				);
			?>
		</h3>

		<ol class="comments-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 50,
				) );
			?>
		</ol>

		<?php the_comments_navigation(); ?>

	<?php endif; ?> 

	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'elvira' ); ?></p>
	<?php endif; ?>

	<?php comment_form(); ?>
</div>