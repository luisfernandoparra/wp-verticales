<?php
if ( ! class_exists( 'WP_Customize_Control' ) ) {
	return NULL;
}

class Themedsgn_Customize_Typography_Control extends WP_Customize_Control {

	public $type = 'typography';

	public function render_content() {
		$choices = themedsgn_get_fonts();
		?>
		<label>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
			<?php endif; ?>

			<select <?php $this->link(); ?>>
				<?php foreach( $choices as $key => $value ): ?>
					<?php if( in_array( $key , array( '__label_01', '__label_02' ) ) ): ?>
					<optgroup label="<?php echo esc_attr( strtoupper( $value ) ); ?>"></optgroup>
					<?php else: ?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $this->value(), $value ); ?>><?php echo esc_html( $value ); ?></option>
					<?php endif; ?>
				<?php endforeach; ?>
			</select>
		</label>
		<?php
	}

}