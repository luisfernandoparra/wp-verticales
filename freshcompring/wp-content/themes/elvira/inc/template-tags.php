<?php
/**
 * Topbar
 */
if( !function_exists( 'themedsgn_elvira_topbar' ) ):
function themedsgn_elvira_topbar() {
	// disabled topbar, bail.
	if( ! themedsgn_get_theme_mod( 'enable_topbar', true ) ) {
		return;
	}

	$topbar_left  = themedsgn_get_theme_mod( 'topbar_left', 'menu' );
	$topbar_right = themedsgn_get_theme_mod( 'topbar_right', 'html' );
	?>
	<div id="topbar">
		<div class="container">
			<div class="topbar-left">					
				<?php 
				switch( $topbar_left ) {
					case 'menu':
							if( has_nav_menu( 'top' ) ) {
								wp_nav_menu( array(
									'menu_class'	=> 'top-menu',
									'container'		=> '',
									'theme_location'=> 'top',
								) );
							}
						break;
					case 'social':
							get_search_form();
							themedsgn_elvira_social_icons( 'topbar-socials' );
						break;
					case 'html':
							themedsgn_elvira_topbar_user_content();
						break;
				}				
				?>
			</div>
			<div class="topbar-right">
				<?php
				switch( $topbar_right ) {
					case 'menu':
							if( has_nav_menu( 'top' ) ) {
								wp_nav_menu( array(
									'menu_class'	=> 'top-menu',
									'container'		=> '',
									'theme_location'=> 'top',
								) );
							}
						break;
					case 'social':
							get_search_form();
							themedsgn_elvira_social_icons( 'topbar-socials' );
						break;
					case 'html':
							themedsgn_elvira_topbar_user_content();
						break;
				}
				?>
			</div>
		</div>
	</div>
	<?php	
}
endif;


/**
 * Topbar user content.
 */
if( !function_exists( 'themedsgn_elvira_topbar_user_content' ) ):
function themedsgn_elvira_topbar_user_content() {	
	echo themedsgn_get_theme_mod( 'topbar_html', "" );	
}
endif;


/**
 * Header.
 */
if( !function_exists( 'themedsgn_elvira_header' ) ):
function themedsgn_elvira_header() {
	?>
	<header id="header">
		<div id="main-header" class="<?php echo esc_attr( themedsgn_get_theme_mod( 'header_layout', 'header-1' ) ); ?> <?php echo themedsgn_get_theme_mod( 'logo_border', true ) ? '' : 'no-border'; ?>">
			<div class="container">	
				<div class="header-wrapper">

					<?php 
					$header_layout = themedsgn_get_theme_mod( 'header_layout', 'header-1' );
					switch( $header_layout ) {
						case 'header-1':
							themedsgn_elvira_header_socials();
							themedsgn_elvira_logo();
							themedsgn_elvira_header_search();
						break;
						case 'header-2':
							themedsgn_elvira_header_search();
							themedsgn_elvira_logo();
							themedsgn_elvira_header_socials();
						break;
						case 'header-3':
							themedsgn_elvira_logo();
							themedsgn_elvira_header_socials();
						break;
						case 'header-4':
							themedsgn_elvira_logo();
							themedsgn_elvira_header_search();
						break;
						case 'header-5':
						case 'header-6':
							themedsgn_elvira_logo();
						break;
					}
					?>

				</div>
			</div>
		</div><!-- #main-header -->

		<nav id="nav" class="<?php echo esc_attr( themedsgn_get_theme_mod( 'nav_style', 'boxed' ) ); ?> <?php echo esc_attr( themedsgn_get_theme_mod( 'nav_alignment', 'center' ) ); ?>">
			<div class="container">
				<div class="menu-wrapper clearfix">
					<?php 
					if( has_nav_menu( 'main' ) ) {
						wp_nav_menu( array(
							'menu_class'	=> 'main-menu',
							'container'		=> '',
							'theme_location'=> 'main',
						) );
					}
					?>

					<div class="mobile-nav">
						<a class="mobile-nav-anchor" href="#"><i class="fa fa-bars"></i></a>
						<?php 
						if( has_nav_menu( 'mobile' ) ) {
							wp_nav_menu( array(
								'menu_class'	=> 'mobile-menu',
								'container'		=> '',
								'theme_location'=> 'mobile',
							) );
						} else {
							wp_nav_menu( array(
								'menu_class'	=> 'mobile-menu',
								'container'		=> '',
								'theme_location'=> 'main',
							) );
						}

						themedsgn_elvira_social_icons( 'mobile-socials' );
						?>
					</div><!-- .mobile-nav -->
				</div>
			</div>
		</nav><!-- #nav -->
	</header>

	<?php
}
endif;


/**
 * Header logo.
 */
if( !function_exists( 'themedsgn_elvira_logo' ) ):
function themedsgn_elvira_logo() {
	$logo_image  = themedsgn_get_theme_mod( 'logo', get_template_directory_uri() . '/assets/images/logo.png' );
	$logo_width  = themedsgn_get_theme_mod( 'logo_width', 0 );
	$logo_height = themedsgn_get_theme_mod( 'logo_height', 0 );

	if( !empty( $logo_width ) && $logo_width > 0 ) {
		$logo_width = 'width="' . $logo_width . '"';
	} else {
		$logo_width = '';
	}

	if( !empty( $logo_height ) && $logo_height > 0 ) {
		$logo_height = 'height="' . $logo_height . '"';
	} else {
		$logo_height = '';
	}

	?>
	<div class="site-branding">
		<div class="<?php echo empty( $logo_image ) ? 'logo-text' : 'logo-image'; ?>">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php
				if( empty( $logo_image ) ):
					bloginfo( 'name' );
				else : ?>
					<img src="<?php echo esc_url( $logo_image ) ?>" alt="<?php bloginfo( 'name' ); ?>" <?php echo !empty( $logo_width ) ? $logo_width : ''; ?> <?php echo !empty( $logo_height ) ? $logo_height : ''; ?>>
				<?php
				endif; ?>
			</a>
		</div>
		<?php if( themedsgn_get_theme_mod( 'tagline', false ) ): ?>
		<div class="tagline"><?php bloginfo( 'description' ); ?></div>
		<?php endif; ?>
	</div>
	<?php
}
endif;


/**
 * Header search.
 */
if( !function_exists( 'themedsgn_elvira_header_search' ) ):
function themedsgn_elvira_header_search() {
	$search_text = themedsgn_get_theme_mod( 'search_placeholder', 'Search&hellip;' );
	?>
	<div class="header-search">
		<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<input type="search" placeholder="<?php echo esc_attr( $search_text ); ?>" value="<?php the_search_query(); ?>" name="s" class="search-input">
			<i class="fa fa-search"></i>
		</form>
	</div>
	<?php
}
endif;


/**
 * Header socials.
 */
if( !function_exists( 'themedsgn_elvira_header_socials' ) ):
function themedsgn_elvira_header_socials() {
	themedsgn_elvira_social_icons( 'header-socials' );
}
endif;


/**
 * Social icons.
 */
if( !function_exists( 'themedsgn_elvira_social_icons' ) ):
function themedsgn_elvira_social_icons( $class = '' ) {
	$socials = array(
		'bloglovin'   => array( 'Bloglovin', 'fa fa-heart' ),
		'dribbble'    => array( 'Dribbble', 'fa fa-dribbble' ),
		'facebook'    => array( 'Facebook', 'fa fa-facebook' ),
		'flickr'      => array( 'Flickr', 'fa fa-flickr' ),
		'google_plus' => array( 'Google+', 'fa fa-google-plus' ),
		'instagram'   => array( 'Instagram', 'fa fa-instagram' ),
		'lastfm'      => array( 'Last.fm', 'fa fa-lastfm' ),
		'linkedin'    => array( 'LinkedIn', 'fa fa-linkedin' ),
		'medium'      => array( 'Medium', 'fa fa-medium' ),
		'pinterest'   => array( 'Pinterest', 'fa fa-pinterest' ),
		'skype'       => array( 'Skype', 'fa fa-skype' ),
		'soundcloud'  => array( 'Soundcloud', 'fa fa-soundcloud' ),
		'spotify'     => array( 'Spotify', 'fa fa-spotify' ),
		'tumblr'      => array( 'Tumblr', 'fa fa-tumblr' ),
		'twitter'     => array( 'Twitter', 'fa fa-twitter' ),
		'vimeo'       => array( 'Vimeo', 'fa fa-vimeo' ),
		'vine'        => array( 'Vine', 'fa fa-vine' ),
		'vk'          => array( 'Vk', 'fa fa-vk' ),
		'youtube'     => array( 'YouTube', 'fa fa-youtube-play' ),
	);
	?>
	<div class="<?php echo esc_attr( $class ); ?>">
		<?php 
		foreach( $socials as $key => $value ):
			$url = themedsgn_get_theme_mod( $key, '' );
			if( !empty( $url ) ): ?>
			<a href="<?php echo esc_url( $url ); ?>" title="<?php echo esc_attr( $value[0] ) ?>" target="_blank">
				<i class="<?php echo esc_attr( $value[1] ) ?>"></i>
				<?php echo $class == 'footer-socials' ? esc_html( $value[0] ) : ''; ?>
			</a>
			<?php 
			endif;
		endforeach; ?>
	</div>
	<?php
}
endif;


/**
 * Footer logo.
 */
if( !function_exists( 'themedsgn_elvira_footer_logo' ) ):
function themedsgn_elvira_footer_logo() {
	if( !themedsgn_get_theme_mod( 'show_footer_logo', true ) ) {
		return;
	}
	$logo_image  = themedsgn_get_theme_mod( 'footer_logo', get_template_directory_uri() . '/assets/images/footer-logo.png' );
	$logo_width  = themedsgn_get_theme_mod( 'footer_logo_width', 0 );
	$logo_height = themedsgn_get_theme_mod( 'footer_logo_height', 0 );

	if( !empty( $logo_width ) && $logo_width > 0 ) {
		$logo_width = 'width="' . $logo_width . '"';
	} else {
		$logo_width = '';
	}

	if( !empty( $logo_height ) && $logo_height > 0 ) {
		$logo_height = 'height="' . $logo_height . '"';
	} else {
		$logo_height = '';
	}
	?>
	<div class="container">
		<div class="footer-logo">
			<div class="<?php echo empty( $logo_image ) ? 'logo-text' : 'logo-image'; ?>">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
					if( empty( $logo_image ) ) {
						bloginfo( 'name' );
					} else {
						?>
						<img src="<?php echo esc_url( $logo_image ) ?>" alt="<?php bloginfo( 'name' ); ?>"  <?php echo !empty( $logo_width ) ? $logo_width : ''; ?> <?php echo !empty( $logo_height ) ? $logo_height : ''; ?>>
						<?php
					}
					?>
				</a>
			</div>
		</div>
	</div>
	<?php
}
endif;


/**
 * Before and after blogroll.
 */
function themedsgn_elvira_before_blogroll() {
	$layouts = array(
		'classic'     => 'classic',
		'classic-alt' => 'classic',
		'grid'        => 'grid',
		'list'        => 'list',
	);
	if( is_home() ) {
		$layout = $layouts[ themedsgn_get_theme_mod( 'homepage_blogroll', 'classic-alt' ) ];
	} elseif( is_archive() ) {
		$layout = $layouts[ themedsgn_get_theme_mod( 'archive_blogroll', 'list' ) ];
	} elseif( is_search() ) {
		$layout = $layouts[ themedsgn_get_theme_mod( 'search_blogroll', 'list' ) ];
	} else {
		$layout = 'classic';
	}

	?>
	<div id="blogroll" class="blogroll-<?php echo esc_attr( $layout ); ?> clearfix">
	<?php
	if( $layout == 'grid' ): ?>
	<div class="grid-sizer"></div>
	<?php
	endif;
}
function themedsgn_elvira_after_blogroll() {
	?>
	</div>
	<?php
}


/**
 * Entry category.
 */
if( !function_exists( 'themedsgn_elvira_category_meta' ) ):
function themedsgn_elvira_category_meta() {
	// hide category meta true, then bail.
	if( ! themedsgn_get_theme_mod( 'show_meta_category', true ) ) {
		return;
	}
	?>
	<div class="entry-cat">
		<?php the_category( ' ' ); ?>
	</div>
	<?php
}
endif;


/**
 * Entry meta.
 */
if( !function_exists( 'themedsgn_elvira_entry_meta' ) ):
function themedsgn_elvira_entry_meta( $author = true, $date = true, $comment = true ) {
	$formats     = array(
		'date_1' => 'M d, Y',
		'date_2' => 'F d, Y',
		'date_3' => 'm/d/Y',
		'date_4' => 'd M Y',
		'date_5' => 'd F Y',
		'date_6' => 'd/m/y',
	);
	$date_format = $formats[ themedsgn_get_theme_mod( 'date_format', 'date_1' ) ];
	$posted_by	 = themedsgn_get_theme_mod( 'author_by', 'By' );
	?>
	<div class="entry-meta">

		<?php 
		/*
		 * Author meta. 
		 */
		if( $author ): ?>
		<span><?php echo esc_html( $posted_by ); ?> <?php the_author_posts_link(); ?></span>
		<?php endif; ?>

		<?php 
		/*
		 * Entry date meta.
		 */
		if( $date ): ?>
		<span><a href="<?php the_permalink(); ?>"><time class="updated" datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php echo esc_html( get_the_date( $date_format ) ); ?></time></a></span>
		<?php endif; ?>

		<?php 
		/*
		 * Comment meta.
		 */
		if( $comment ): ?>
		<span>
			<?php 
			comments_popup_link(
				themedsgn_get_theme_mod( 'comment_no_comment', esc_html__( 'No Comments', 'elvira' ) ),
				themedsgn_get_theme_mod( 'comment_one_comment', esc_html__( '1 Comment', 'elvira' ) ),
				sprintf( '%1$s %2$s', get_comments_number(), themedsgn_get_theme_mod( 'comment_many_comments', esc_html__( 'Comments', 'elvira' ) ) )
			); 
			?>
		</span>
		<?php endif; ?>

	</div>
	<?php
}
endif;


/**
 * Entry tags.
 */
if( !function_exists( 'themedsgn_elvira_entry_tags' ) ):
function themedsgn_elvira_entry_tags() {
	the_tags( '<div class="entry-tags">', ' ', '</div>' );
}
endif;


/**
 * Readmore.
 */
if( !function_exists( 'themedsgn_elvira_readmore' ) ):
function themedsgn_elvira_readmore() {
	?>
	<a href="<?php the_permalink(); ?>" class="button more-link"><?php echo esc_html( themedsgn_get_theme_mod( 'readmore', 'Continue Reading' ) ); ?></a>
	<?php
}
endif;


/**
 * Entry share.
 */
if( !function_exists( 'themedsgn_elvira_entry_share' ) ):
function themedsgn_elvira_entry_share() {
	$share_facebook    = 'https://www.facebook.com/sharer/sharer.php?u=' . get_the_permalink();
	$share_twitter     = 'https://twitter.com/home?status=' . get_the_permalink();
	$share_google_plus = 'https://plus.google.com/share?url=' . get_the_permalink();
	$share_pinterest   = 'https://pinterest.com/pin/create/button/?url=' . get_the_permalink() . '&media=' . wp_get_attachment_url( get_post_thumbnail_id(), 'full' );
	$share_linkedin    = 'https://www.linkedin.com/shareArticle?mini=true&url=' . get_the_permalink() . '&title=' . the_title_attribute( 'echo=0' );
	?>
	<div class="entry-share">
		<?php if( themedsgn_get_theme_mod( 'share_facebook', true ) ): ?><a href="<?php echo esc_url( $share_facebook ); ?>"><i class="fa fa-facebook"></i></a><?php endif; ?>
		<?php if( themedsgn_get_theme_mod( 'share_twitter', true ) ): ?><a href="<?php echo esc_url( $share_twitter ); ?>"><i class="fa fa-twitter"></i></a><?php endif; ?>
		<?php if( themedsgn_get_theme_mod( 'share_googleplus', true ) ): ?><a href="<?php echo esc_url( $share_google_plus ); ?>"><i class="fa fa-google-plus"></i></a><?php endif; ?>
		<?php if( themedsgn_get_theme_mod( 'share_pinterest', true ) ): ?><a href="<?php echo esc_url( $share_pinterest ); ?>"><i class="fa fa-pinterest"></i></a><?php endif; ?>
		<?php if( themedsgn_get_theme_mod( 'share_linkedin', true ) ): ?><a href="<?php echo esc_url( $share_linkedin ); ?>"><i class="fa fa-linkedin"></i></a><?php endif; ?>
	</div>
	<?php
}
endif;


/**
 * Navigation.
 */
if( !function_exists( 'themedsgn_elvira_navigation' ) ):
function themedsgn_elvira_navigation() {
	the_posts_pagination( array(
		'prev_text'          => '<i class="fa fa-chevron-left"></i>',
		'next_text'          => '<i class="fa fa-chevron-right"></i>',
	) );
}
endif;


/**
 * Archive box.
 */
if( !function_exists( 'themedsgn_elvira_archive' ) ):
function themedsgn_elvira_archive() {
	?>
	<div class="archive-box">
		<?php if( is_search() ): ?>
			<h2><span><?php printf( __( 'Search Results for: %s', 'elvira' ), '&ldquo;<strong>' . esc_html( get_search_query() ) . '</strong>&rdquo;' ); ?></span></h2>
			<?php get_search_form(); ?>
		<?php 
		else:
			the_archive_title( '<h2><span>', '</span></h2>' );
			if( is_author() ): 
			?>
				<div class="archive-description">
					<?php the_author_meta( 'description' ); ?>
				</div>
			<?php
			else:
				the_archive_description( '<div class="archive-description">', '</div>' ); 
			endif;
		endif;
		?>
	</div>
	<?php
}
endif;


/**
 * Related posts.
 */
if( !function_exists( 'themedsgn_elvira_related_posts' ) ):
function themedsgn_elvira_related_posts() {
	if( !themedsgn_get_theme_mod( 'related_posts', true ) ) {
		return;
	}
	global $post;
	$heading 	= themedsgn_get_theme_mod( 'related_posts_title', 'Related Posts' );
	$orig_post 	= $post;

	$tags = wp_get_post_tags( $post->ID );
	if( $tags ) {

		$tag_ids = array();		
		foreach( $tags as $tag ) {
			$tag_ids[] = $tag->term_id;			
		}

		$args = array(
			'tag__in'             => $tag_ids,
			'post__not_in'        => array( $post->ID ),
			'posts_per_page'      => 3,
			'ignore_sticky_posts' => true,
			// 'meta_key'            => '_thumbnail_id',
		);

		$query = new WP_Query( $args );
		if( $query->have_posts() ): ?>
			<div class="related-posts-section clearfix">

				<?php if( !empty( $heading ) ): ?>
				<h3 class="related-posts-title"><span><?php echo esc_html( $heading ); ?></span></h3>
				<?php endif; ?>

				<div class="related-posts">
					<?php
					while( $query->have_posts() ) {
						$query->the_post(); ?>
						<div class="related-item">
							<a href="<?php the_permalink(); ?>">								
								<div class="entry-image">
									<?php 
									if( has_post_thumbnail() ):
										the_post_thumbnail( 'related-thumbnail' ); 
									else:?>
										<img src="<?php echo esc_url( get_template_directory_uri() . '/assets/images/related-thumbnail.jpg' ); ?>">
									<?php
									endif; ?>
								</div>
								<?php the_title( '<h3>', '</h3>' ); ?>
							</a>
						</div>
						<?php
					} ?>
				</div>

			</div>
		<?php 
		endif;
	}
	$post = $orig_post;
	wp_reset_postdata();
}
endif;


/**
 * Author info.
 */
if( !function_exists( 'themedsgn_elvira_author_info' ) ):
function themedsgn_elvira_author_info() {
	$title 	= themedsgn_get_theme_mod( 'author_info_title', 'Written By' );
	if( !themedsgn_get_theme_mod( 'author_info', true ) ) {
		return;
	}
	?>
	<div class="author-info clearfix">
		<?php echo get_avatar( get_the_author_meta( 'ID' ), 120 ); ?>
		<div class="author-detail">
			<?php if( !empty( $title ) ): ?><h4 class="author-title"><?php echo esc_html( $title ); ?></h4><?php endif; ?>
			<h3 class="author-name"><?php the_author_posts_link(); ?></h3>			
			<div class="author-description"><?php the_author_meta( 'description' ); ?></div>
			<div class="author-socials social-icons">
			<?php
			$social_fields = array(
				'facebook' => array( esc_html__( 'Facebook', 'elvira' ), 'fa fa-facebook' ),
				'twitter' => array( esc_html__( 'Twitter', 'elvira' ), 'fa fa-twitter' ),
				'google_plus' => array( esc_html__( 'Google+', 'elvira' ), 'fa fa-google-plus' ),
				'instagram' => array( esc_html__( 'Instagram', 'elvira' ), 'fa fa-instagram' ),				
				'pinterest' => array( esc_html__( 'Pinterest', 'elvira' ), 'fa fa-pinterest' ),
				'linkedin' => array( esc_html__( 'LinkedIn', 'elvira' ), 'fa fa-linkedin' ),
				'youtube' => array( esc_html__( 'YouTube', 'elvira' ), 'fa fa-youtube-play' ),
			);
			foreach ( $social_fields as $k => $v ): 
				$url = get_the_author_meta( $k, get_current_user_id() );
				if( !empty( $url ) ): 
				?>
					<a href="<?php echo esc_url( $url ); ?>" title="<?php echo esc_attr( $v[0] ); ?>" target="_blank"><i class="<?php echo esc_attr( $v[1] ); ?>"></i></a>
				<?php
				endif;
			endforeach; 
			?>
			</div>
		</div>
	</div><!-- .author-info -->	
	<?php
}
endif;


/**
 * Excerpt length.
 */
if( !function_exists( 'themedsgn_elvira_excerpt_length' ) ):
function themedsgn_elvira_excerpt_length( $length ) {
	return 50;
}
endif;
add_filter( 'excerpt_length', 'themedsgn_elvira_excerpt_length', 999 );


/**
 * Excerpt more.
 */
if( !function_exists( 'themedsgn_elvira_excerpt_more' ) ):
function themedsgn_elvira_excerpt_more( $more ) {
	return '&hellip;';
}
endif;
add_filter( 'excerpt_more', 'themedsgn_elvira_excerpt_more' );


/**
 * Custom user contacts.
 */
if( !function_exists( 'themedsgn_elvira_user_contacts' ) ):
function themedsgn_elvira_user_contacts( $fields ) {
	$fields[ 'facebook' ] = esc_html__( 'Facebook URL', 'elvira' );
	$fields[ 'twitter' ] = esc_html__( 'Twitter URL', 'elvira' );
	$fields[ 'google_plus' ] = esc_html__( 'Google+ URL', 'elvira' );
	$fields[ 'instagram' ] = esc_html__( 'Instagram URL', 'elvira' );
	$fields[ 'pinterest' ] = esc_html__( 'Pinterest URL', 'elvira' );
	$fields[ 'linkedin' ] = esc_html__( 'LinkedIn URL', 'elvira' );
	$fields[ 'youtube' ] = esc_html__( 'YouTube URL', 'elvira' );	
	return $fields;
}
endif;
add_filter( 'user_contactmethods', 'themedsgn_elvira_user_contacts' );


/**
 * Exclude search
 */
if( !function_exists( 'themedsgn_elvira_exclude_search' ) ):
function themedsgn_elvira_exclude_search( $query ) {
	if( $query->is_search() && themedsgn_get_theme_mod( 'exclude_pages', true ) ) {
		$query->set( 'post_type', 'post' );
	}
	return $query;
}
endif;
add_filter( 'pre_get_posts', 'themedsgn_elvira_exclude_search' );


/**
 * Custom inline stylesheet
 */
if( !function_exists( 'themedsgn_elvira_inline_stylesheet' ) ):
function themedsgn_elvira_inline_stylesheet() {
	// header wrapper padding
	$inline_css = '.header-wrapper { padding-top: '. themedsgn_get_theme_mod( 'header_padding_top', 30 ) .'px; padding-bottom: '. themedsgn_get_theme_mod( 'header_padding_bottom', 30 ) .'px; }';

	// widget title
	if( ! themedsgn_get_theme_mod( 'sidebar_title_uppercase', true ) ) {
		$inline_css .= '.widget .widget-title { text-transform: none; letter-spacing: normal; font-weight: bold; }';
	}

	// typography
	$body_font     = themedsgn_get_fonts( themedsgn_get_theme_mod( 'body_font_family', 'rubik' ) );
	$menu_font     = themedsgn_get_fonts( themedsgn_get_theme_mod( 'nav_font_family', 'rubik' ) );
	$headings_font = themedsgn_get_fonts( themedsgn_get_theme_mod( 'heading_font_family', 'domine' ) );

	$inline_css .= 'body { font-family: "' . $body_font['css'] . '"; font-size: ' . themedsgn_get_theme_mod( 'body_font_size', '14' ) . 'px; }';
	$inline_css .= 'h1, h2, h3, h4, h5, h6, .colophon, .widget_recent_entries li::before { font-family: "' . $headings_font['css'] . '" }';
	$inline_css .= '.top-menu, .main-menu { font-family: "' . $menu_font['css'] . '" }';
	$inline_css .= '.main-menu { font-size: ' . themedsgn_get_theme_mod( 'nav_font_size', '14' ) . 'px }';
	if( !themedsgn_get_theme_mod( 'nav_font_uppercase', true ) ) {
		$inline_css .= '.main-menu > li > a { text-transform: none; letter-spacing: normal; }';
	}

	// styling
	$accent_color    = themedsgn_get_theme_mod( 'accent_color', '#89B399' );
	$text_color      = themedsgn_get_theme_mod( 'text_color', '#4a4a4a' );
	$border_color    = themedsgn_get_theme_mod( 'border_color', '#eeeeee' );
	$more_background = themedsgn_get_theme_mod( 'readmore_background', '#89b399' );
	$menu_background = themedsgn_get_theme_mod( 'nav_background', '#89b399' );
	$menu_color      = themedsgn_get_theme_mod( 'nav_color', '#ffffff' );
	$menu_hover      = themedsgn_get_theme_mod( 'nav_hover_color', '#ffffff' );
	$menu_hover_bg   = themedsgn_get_theme_mod( 'nav_hover_background', '#7aa088' );
	$menu_child_bg   = themedsgn_get_theme_mod( 'nav_child_background', '#93BFA3' );
	$footer_bg       = themedsgn_get_theme_mod( 'footer_logo_background', '#93BFA3' );
	$bg_color        = themedsgn_get_theme_mod( 'background_color', '#f9f9f9' );
	$bg_image        = themedsgn_get_theme_mod( 'background_image', '' );
	$bg_repeat       = themedsgn_get_theme_mod( 'background_repeat', 'repeat' );
	$bg_size         = themedsgn_get_theme_mod( 'background_size', 'auto' );
	$bg_position     = themedsgn_get_theme_mod( 'background_position', 'left-top' );
	$bg_attachment   = themedsgn_get_theme_mod( 'background_attachment', 'scroll' );

	if( !empty( $bg_image ) ) {
		$background_setting  = 'background-image: url(' . esc_url( $bg_image ) . ');';
		$background_setting .= 'background-repeat: '. $bg_repeat .';';
		$background_setting .= 'background-size: ' . $bg_size . ';';
		$background_setting .= 'background-position: ' . $bg_position . ';';
		$background_setting .= 'background-attachment: ' . $bg_attachment . ';';
	} else {
		$background_setting = '';
	}

	$inline_css  .= 'body{ color:' . $text_color . ' }';
	$inline_css  .= '.entry .entry-wrapper, .author-info, .related-posts .related-item a, .comments-section, .widget, .posts-carousel .post-item .post-item-wrap, .navigation.post-navigation { border-color:'. $border_color .' }';
	$inline_css  .= '.entry .entry-footer .more-link { background-color:'. $more_background .' }';
	$inline_css  .= '#nav.stretched, .sticky-nav-on #nav, .menu-wrapper { background-color:'. $menu_background .' }';
	$inline_css  .= '.main-menu a { color:'. $menu_color .' }';
	$inline_css  .= '.main-menu > li > a .main-menu > li > a:hover, .main-menu > li.current-menu-item > a, .main-menu > li:hover > a { color:'. $menu_hover .'; background-color: '. $menu_hover_bg .' }';
	$inline_css  .= '.main-menu ul { background-color:'. $menu_child_bg .' }';		
	$inline_css  .= 'body { background-color: '. $bg_color .'; '. $background_setting .' }';
	$inline_css  .= '.footer-logo { background-color:'. $footer_bg .' }';

	if( ! ( $accent_color == '#89B399' || $accent_color == '#89b399' ) ) {
		// background color
		$inline_css  .= '::-moz-selection { background: '. $accent_color .'; } ::selection { background: '. $accent_color .'; }';
		$inline_css  .= '.comment-body .reply a, .link-pages a, .link-pages > span, .social-icons a { background-color: ' . $accent_color . ' }';
		// color
		$inline_css  .= 'a, .top-menu li a:hover, #topbar .topbar-socials a:hover, .entry .entry-cat a, .entry .entry-title a:hover, .entry .entry-meta a:hover, .navigation.post-navigation a:hover, .single-entry .entry-footer .entry-tags a:hover, .entry.type-page .entry-footer .entry-comment-meta a:hover, .related-posts .related-item a:hover, .widget li a:hover, .posts-carousel .post-item h4 a:hover, .slider-item .slider-caption .slider-cat a, .slider-item .slider-caption .slider-title a:hover, .slider-item .slider-caption .more:hover { color: ' . $accent_color . ';}';
		// border color
		$inline_css  .= 'input:hover, textarea:hover, input:focus, textarea:focus, .header-search .search-input:hover, .header-search .search-input:focus, #blogroll .entry.sticky .entry-wrapper, .posts-carousel .owl-dot.active, .main-footer .footer-socials a:hover, .entry .entry-cat a { border-color: '. $accent_color .' }';
		// border left
		$inline_css  .= 'blockquote { border-left-color: '. $accent_color .' }';
		// background & border color
		$inline_css  .= 'input[type=submit], button, .button { background-color: '. $accent_color .'; border-color: '. $accent_color .'; }';
		// color & border color
		$inline_css  .= '.header-socials a:hover, .entry .entry-footer .entry-share a:hover, .pagination .nav-links .page-numbers.current, .pagination .nav-links .page-numbers:hover, .comment-navigation a:hover, .posts-carousel .owl-nav .owl-prev:hover, .posts-carousel .owl-nav .owl-next:hover, .tagcloud a:hover, #topbar .button:hover { color: '. $accent_color .'; border-color: ' . $accent_color . '; }';
		// border bottom color
		$inline_css  .= '.slider-item .slider-caption .more { border-bottom-color: '. $accent_color .'; }';
		// border top color
		$inline_css  .= '#blogroll .entry.sticky .sticky-indicator { border-top-color: '. $accent_color .' }';
		// Woocommerce Custom Stylesheets
		$inline_css  .= '.woocommerce ul.products li.product a:hover, .woocommerce-page ul.products li.product a:hover { color: ' . $accent_color . ' }';
		$inline_css  .= '.woocommerce ul.products li.product .button, .woocommerce div.product form.cart .button, .woocommerce #respond input#submit { background: ' . $accent_color . ' }';
		$inline_css  .= '.woocommerce nav.woocommerce-pagination ul li span, .woocommerce nav.woocommerce-pagination ul li a:hover { border-color: ' . $accent_color . '; color: ' . $accent_color . ' }';
		$inline_css  .= '.woocommerce ul.cart_list li, .woocommerce ul.product_list_widget li { color: ' . $accent_color . ' }';
		
	}

	// custom css
	$inline_css  .= themedsgn_get_theme_mod( 'custom_css', '' );

	wp_add_inline_style( 'elvira-style', $inline_css );
}
endif;
add_action( 'wp_enqueue_scripts', 'themedsgn_elvira_inline_stylesheet' );