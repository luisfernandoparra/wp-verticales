<?php if( is_active_sidebar( 'sidebar-woocommerce' ) ): ?>
<aside id="secondary">
	<div id="sidebar">
		<?php dynamic_sidebar( 'sidebar-woocommerce' ); ?>
	</div>
</aside>
<?php endif; ?>