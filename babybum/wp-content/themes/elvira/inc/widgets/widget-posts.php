<?php
add_action( 'widgets_init', create_function('', 'return register_widget("themedsgn_widget_posts");') );

class themedsgn_widget_posts extends WP_Widget {

	public function __construct() {
		$widget_details = array(
			'classname'   => 'themedsgn_widget_posts',
			'description' => esc_html__( 'Display posts with thumbnail (recent/commented/random).', 'elvira' ),
		);

		parent::__construct( 'themedsgn_widget_posts', 'ThemeDsgn Posts Widget', $widget_details );		
	}

	public function form( $instance ) {
		$title      = isset( $instance['title'] ) ? $instance['title'] : '';
		$layout     = isset( $instance['layout'] ) ? $instance['layout'] : 'list-small';
		$number     = isset( $instance['number'] ) ? (int) $instance['number'] : 5;
		$order      = isset( $instance['order'] ) ? $instance['order'] : 'date';
		$cat        = isset( $instance['cat'] ) ? $instance['cat'] : '';
		$show_date  = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;
		$categories = get_categories( array(
			'hide_empty'   => true,
			'hierarchical' => false,
		) );		
		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:', 'elvira' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e( 'Number of posts to show:', 'elvira' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" value="<?php echo esc_attr( $number ); ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php esc_html_e( 'Posts ordered by:', 'elvira' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>">
				<option <?php selected( $order, 'date' ); ?> value="date">Date</option>
				<option <?php selected( $order, 'comment_count' ); ?> value="comment_count">Comment Count</option>
				<option <?php selected( $order, 'rand' ); ?> value="rand">Random</option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'cat' ); ?>"><?php esc_html_e( 'Category:', 'elvira' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'cat' ); ?>" name="<?php echo $this->get_field_name( 'cat' ); ?>">
				<option <?php selected( $cat, '' ); ?> value="">All Categories</option>
				<?php 
				if( $categories ):
					foreach( $categories as $category ): ?>
					<option <?php selected( $cat, $category->term_id ); ?> value="<?php echo esc_attr( $category->term_id ); ?>"><?php echo esc_html( $category->name ); ?></option>
					<?php
					endforeach;
				endif;
				?>
			</select>
		</p>		

		<p>
			<label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php esc_html_e( 'Posts layout style:', 'elvira' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'layout' ); ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>">
				<option <?php selected( $layout, 'list-small' ); ?> value="list-small">Small Thumbnail</option>
				<option <?php selected( $layout, 'list-large' ); ?> value="list-large">Large Thumbnail</option>
			</select>
		</p>

		<p>
			<input class="checkbox" type="checkbox"<?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php esc_html_e( 'Show date?', 'elvira' ); ?></label>
		</p>

		<?php

	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']     = sanitize_text_field( $new_instance['title'] );
		$instance['layout']    = $new_instance['layout'];
		$instance['number']    = (int) $new_instance['number'];
		$instance['order']     = $new_instance['order'];
		$instance['cat']       = $new_instance['cat'];
		$instance['show_date'] = (bool) $new_instance['show_date'];
		return $instance;
	}

	public function widget( $args, $instance ) {
		$title     = !empty( $instance['title'] ) ? $instance['title'] : '';
		$layout    = !empty( $instance['layout'] ) ? $instance['layout'] : 'list-small';
		$number    = !empty( $instance['number'] ) ? (int) $instance['number'] : 5;
		$order     = !empty( $instance['order'] ) ? $instance['order'] : 'date';
		$cat       = !empty( $instance['cat'] ) ? $instance['cat'] : '';
		$show_date = !empty( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;

		echo $args['before_widget'];
		if( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$query_args = array(
			'posts_per_page'      => $number,
			'orderby'             => $order,
			'ignore_sticky_posts' => true,
			'cat'                 => $cat,
		);
		$query = new WP_Query( $query_args );
		if( $query->have_posts() ): ?>

			<ul class="posts-list <?php echo esc_attr( $layout ); ?>">

			<?php while( $query->have_posts() ): $query->the_post(); ?>

				<li class="clearfix">
					<a href="<?php the_permalink(); ?>">
						<?php 
						if( has_post_thumbnail() ):
							if( $layout == 'list-small' ) {
								the_post_thumbnail( 'mini-thumbnail' );	
							} else {
								the_post_thumbnail( 'related-thumbnail' );
							}
						else: ?>
							<?php if( $layout == 'list-small' ): ?>
							<img src="<?php echo esc_url( get_template_directory_uri() . '/assets/images/mini-thumbnail.jpg' ); ?>" alt="<?php the_title_attribute(); ?>">
							<?php else: ?>
							<img src="<?php echo esc_url( get_template_directory_uri() . '/assets/images/related-thumbnail.jpg' ); ?>" alt="<?php the_title_attribute(); ?>">
							<?php endif; ?>
						<?php 
						endif; ?>
					</a>
					<div class="post-list-detail">
						<a href="<?php the_permalink(); ?>"><?php echo strip_tags( get_the_title() ); ?></a>
						<?php if( $show_date ): ?><time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php echo esc_html( get_the_date( 'M j, Y' ) ); ?></time><?php endif; ?>
					</div>
				</li>

			<?php endwhile; ?>

			</ul>

			<?php
		endif;		
		echo $args['after_widget'];
		wp_reset_postdata();
	}

}