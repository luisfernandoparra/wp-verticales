<?php
if ( ! class_exists( 'WP_Customize_Control' ) ) {
	return NULL;
}

class Themedsgn_Customize_Arbitrary_Control extends WP_Customize_Control {

	public $type = 'html';
	public $content = '';

	public function render_content() {
		?>
		<div class="themedsgn-arbitrary-html-control">
			<div class="customize-control-title"><?php echo esc_html( $this->label ); ?></div>
			<?php echo !empty( $this->content  ) ? '<div class="arbitrary-html-content">' . $this->content . '</div>' : ''; ?>
		</div>
		<?php
	}

}